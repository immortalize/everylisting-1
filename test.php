<?php

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        
        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),
        
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,
        
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,
        
        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
}

//function getMapServletResponse($command, $errorreporting) {
	
	$command="http://35.243.205.61/MapMarkerServlet/Process?request=jsonlocation&nocleanup=true%C2%ACimingreport=true";
	$errorreporting=1;
    if ($errorreporting) {
        $keep_display_errors = ini_set('display_errors', 1); // 1-turn on all error reporings 0-turn off all error reporings
        $keep_error_reporting = error_reporting();
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    }
    $rnd = gen_uuid();
    $curlret[$rnd] = curl_init();
    curl_setopt($curlret[$rnd], CURLOPT_URL, $command);
    curl_setopt($curlret[$rnd], CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curlret[$rnd], CURLOPT_HEADER, false);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($curlret[$rnd], CURLOPT_FRESH_CONNECT, true);
    curl_setopt($curlret[$rnd], CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curlret[$rnd], CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curlret[$rnd]);
    $httpcode = curl_getinfo($curlret[$rnd], CURLINFO_HTTP_CODE);
	
	echo "Servlet response:	" . $result . "Http code:	" . $httpcode;
    curl_close($curlret[$rnd]);
    if ($errorreporting) {
        if ($keep_display_errors != false) {
            ini_set('display_errors', $keep_display_errors);
        }
        error_reporting($keep_error_reporting);
    }
	
	
    unset($curlret[$rnd]);
    return trim($result);
//}

?>