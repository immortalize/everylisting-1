package core;

public enum ERenderingState {
	inqueue,
	gathering,
	resizing,
	rendering,
	saving,
	successfullycompleted,
	failed
}
