package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;

import core.AnimationBuilder.Sequence;

public class TextSuperposer extends FrameSuperposer {
	
	private String text;
	private ELocation location;
	private int size;
	private int padding;
	private String color;

	public TextSuperposer(String text, ELocation location, int size, int padding, String color) {
		this.text = text;
		this.location = location;
		this.size = size;
		this.padding = padding;
		this.color = color;
	}
	
	private static Font scaleFontToFit(String text, int width, Graphics g, Font pFont) {
		float fontSize = pFont.getSize();
	    float fWidth = g.getFontMetrics(pFont).stringWidth(text);
	    fontSize = ((float)width / (float)fWidth) * fontSize;
	    return pFont.deriveFont((float)fontSize);
	}

	@Override
	public Rectangle render(BufferedImage image, Dimension dimension, Rectangle notSuperposedRect, AnimationBuilder caller, Sequence sequence) throws Exception {
		Graphics2D graphics2D = (Graphics2D) image.getGraphics().create();
		Rectangle destRect = new Rectangle();
		destRect.width = (dimension.width * size)/100;
		graphics2D.setFont(scaleFontToFit(text, destRect.width, graphics2D, graphics2D.getFont()));
		destRect.width = graphics2D.getFontMetrics().stringWidth(text);
		int height = graphics2D.getFontMetrics().getHeight();
		destRect.height = height;
		switch (location) {
			case eTopLeft:
				destRect.x = padding;
				destRect.y = padding;
				break;
			case eTopRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = padding;
				break;
			case eBottomLeft:
				destRect.x = padding;
				destRect.y = dimension.height - padding - height;
				break;
			case eBottomRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = dimension.height - padding - height;
				break;
			case eCenterOnLastSequence:
				if (!caller.isLastSequence(sequence)) {
					return null;
				} else {
					destRect.x = (dimension.width/2) - ((destRect.width + padding)/2);
					destRect.y = (dimension.height/2) - ((height + padding)/2);
				}
				break;
			default:
				throw new Exception("Illegal location.");
		}
	    graphics2D.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
	    Rectangle roundedRect = new Rectangle(destRect.x - (padding/2), destRect.y - (padding/2), destRect.width + padding, destRect.height + padding);
	    graphics2D.fillRoundRect(roundedRect.x, roundedRect.y, roundedRect.width, roundedRect.height, 15, 15);
	    Color vcolor;
	    try {
	        Field field = Class.forName("java.awt.Color").getField(color);
	        vcolor = (Color)field.get(null);
	    } catch (Exception e) {
	    	vcolor = Color.BLACK;
	    }
		graphics2D.setColor(vcolor);
		graphics2D.drawString(text, destRect.x, destRect.y + graphics2D.getFontMetrics().getMaxAscent());
		graphics2D.dispose();
		return roundedRect;
	}

	@Override
	public ELocation getLocation() {
		return location;
	}
}
