package core;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.xml.bind.JAXBException;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;

import com.defano.jsegue.AnimatedSegue;
import com.defano.jsegue.SegueAnimationObserver;
import com.defano.jsegue.SegueBuilder;
import com.defano.jsegue.SegueCompletionObserver;

import core.FrameSuperposer.ELocation;
import core.RenderingJobProgress.JobsDirectoryAbsentException;
import util.GifSequenceWriter;
import util.StringUtils;

public class AnimationBuilder implements SegueCompletionObserver {
	
	@SuppressWarnings("unused")
	private String className = this.getClass().getSimpleName();
	private transient List<Sequence> effectiveSequence;
	private transient int framesCount = 0;
	private transient long gatheringTime = 0;
	private transient long renderingTime = 0;
	private transient List<FrameSuperposer> frameSuperposers = null;
	private Dimension dimension = new Dimension();
	private Dimension dimensionInEffect = null;
	private transient Rectangle notSuperposedRect;
	private String fileformat = "gif";
	private transient boolean isVideo = false;
	private transient File calculatedFile = null;
	private transient Exception caughtException = null;
	private transient RenderingJobProgress jobProgress = null;

	public class Sequence implements SegueCompletionObserver, SegueAnimationObserver {
		@SuppressWarnings("unused")
		private String className = this.getClass().getSimpleName();
		@SuppressWarnings("unused")
		private String animationEndClass;
		private String url;
		private int sequenceIndex;
		private int fps;
		private boolean superposedElements = false;
		private int fixedTime;
		private int msTransitionTime;
		private transient Class<? extends AnimatedSegue> animationEnd;
		private transient BufferedImage image;
		private transient List<BufferedImage> transitionFrames = null;
		private transient Sequence queue = null;
		private transient AnimatedSegue segue = null;
		private transient SegueCompletionObserver notifyOnDone = null;

		public Sequence(String url, Class<? extends AnimatedSegue> animationEnd, int fixedTime, int msTransitionTime) {
			this.url = url;
			this.animationEnd = animationEnd;
			this.msTransitionTime = msTransitionTime;
			this.fps = 30;
			this.fixedTime = fixedTime;
			this.animationEndClass = (this.animationEnd != null)?this.animationEnd.getSimpleName():null;
		}

		private boolean isValid() {
			return (image != null);
		}

		private void gatherBufferedImage() throws MalformedURLException, IOException, BufferedImageCreationException {
			image = ImageIO.read(new URL(url));
			if (image == null) {
				throw new BufferedImageCreationException("Can't create the image from " + url + ". Potentially caused by a file fomat not handled.");
			}
		}

		private void resizeImage(Dimension targetDimension) {
			image = Scalr.resize(image, Mode.AUTOMATIC, targetDimension.width);
		}

		@Override
		public void onSegueAnimationCompleted(AnimatedSegue segue) {
			System.out.println(url + " done.");
			if (jobProgress != null) {
				jobProgress.increaseCurrentTotal();
				try {
					jobProgress.store();
				} catch (JAXBException | JobsDirectoryAbsentException e) {
					if (jobProgress != null) {
						jobProgress.setExceptionString(StringUtils.getExceptionString(e));
					}
					e.printStackTrace();
				}
			}
			if (queue != null) {
				if (queue.segue != null) {
					queue.segue.start();
				} else {
					queue.onSegueAnimationCompleted(segue);
				}
			} else if (notifyOnDone != null) {
				notifyOnDone.onSegueAnimationCompleted(segue);
			}
		}

		public String getUrl() {
			return url;
		}

		@Override
		public void onFrameRendered(AnimatedSegue segue, BufferedImage image) {
			transitionFrames.add(image);
			framesCount++;
		}

		public boolean isSuperposedElements() {
			return superposedElements;
		}

		public void setSuperposedElements(boolean superposedElements) {
			this.superposedElements = superposedElements;
		}

		public int getSequenceIndex() {
			return sequenceIndex;
		}
	}

	protected static class BufferedImageCreationException extends Exception {

		private static final long serialVersionUID = -8495482988430393696L;

		public BufferedImageCreationException(String arg0, Throwable arg1) {
			super(arg0, arg1);
		}

		public BufferedImageCreationException(String arg0) {
			super(arg0);
		}

		public BufferedImageCreationException(Throwable arg0) {
			super(arg0);
		}
	}

	public AnimationBuilder(List<FrameSuperposer> frameSuperposers, Dimension imageDimension, String fileformat) {
		this.frameSuperposers = frameSuperposers;
		this.dimension = imageDimension;
		this.fileformat = fileformat;
	}
	
	public Sequence addSequence(String url, Class<? extends AnimatedSegue> animationEnd, int fixedTime, int msTransitionTime) {
		return new Sequence(url, animationEnd, fixedTime, msTransitionTime);
	}

	public File create(List<Sequence> sequences, RenderingJobProgress jobProgress) throws Exception {
		if (jobProgress != null) {
			jobProgress.setExceptionString(null);
			jobProgress.start();
			jobProgress.breakOnException();
		}
		this.jobProgress = jobProgress;
		long time = System.currentTimeMillis();
		calculatedFile = null;
		caughtException = null;
		isVideo = (fileformat.equals("gif"));
		gatheringTime = System.currentTimeMillis();
		if (jobProgress != null) {
			jobProgress.setOneLinerCustomerFacingStatus("Gathering images");
			jobProgress.setState(ERenderingState.gathering);
			jobProgress.setCurrentTotal(0, (!isVideo)?1:sequences.size());
			jobProgress.store();
			jobProgress.breakOnException();
		}
		for (Sequence sequence : sequences) {
			try {
				if (sequence.image == null) {
					sequence.gatherBufferedImage();
					System.out.println(sequence.url + " gathered.");
				}
				if (jobProgress != null) {
					jobProgress.increaseCurrentTotal();
					jobProgress.store();
				}
			} catch (IOException | BufferedImageCreationException e) {
				System.out.println(sequence.url + " failed to gather.");
				throw e;
			}
			if (!isVideo) {
				break;
			}
			if (jobProgress != null) {
				jobProgress.store();
				jobProgress.breakOnException();
			}
		}
		gatheringTime = System.currentTimeMillis() - gatheringTime;
		if (jobProgress != null) {
			jobProgress.setGatheringTime(gatheringTime);
			jobProgress.breakOnException();
		}
		int minwidth = Integer.MAX_VALUE;
		int minheight = Integer.MAX_VALUE;
		for (int i = (sequences.size() - 1); i >= 0; i--) {
			if (!sequences.get(i).isValid()) {
				sequences.remove(i);
			} else {
				minwidth = Math.min(minwidth, sequences.get(i).image.getWidth());
				minheight = Math.min(minheight, sequences.get(i).image.getHeight());
			}
		}
		float proportions = (float)minwidth/(float)minheight;
		dimensionInEffect = new Dimension();
		dimensionInEffect.width = dimension.width;
		dimensionInEffect.height = (int) (dimensionInEffect.width / proportions);
		notSuperposedRect = new Rectangle(0, 0, dimensionInEffect.width, dimensionInEffect.height);
		effectiveSequence = sequences;
		if (sequences.size() > 0) {
			if (jobProgress != null) {
				jobProgress.setOneLinerCustomerFacingStatus("Resizing images");
				jobProgress.setState(ERenderingState.resizing);
				jobProgress.setCurrentTotal(0, sequences.size());
				jobProgress.store();
				jobProgress.breakOnException();
			}
			int index = 0;
			for (Sequence sequence : sequences) {
				sequence.sequenceIndex = index;
				index++;
				sequence.resizeImage(dimensionInEffect);
				if (jobProgress != null) jobProgress.setCurrentTotal(1, (!isVideo)?1:sequences.size());
				if (!isVideo) {
					break;
				}
				if (jobProgress != null) {
					jobProgress.store();
					jobProgress.breakOnException();
				}
			}
			if (jobProgress != null) {
				jobProgress.setOneLinerCustomerFacingStatus("Rendering " + (isVideo?"sequences":"image"));
				jobProgress.setState(ERenderingState.rendering);
				jobProgress.setCurrentTotal(0, sequences.size());
				jobProgress.store();
				jobProgress.breakOnException();
			}			
			for (int i = (sequences.size() - 1); i >= 0; i--) {
				if (i != (sequences.size() - 1)) {
					sequences.get(i).queue = sequences.get(i + 1);
				} else {
					sequences.get(i).notifyOnDone = this;
				}
				sequences.get(i).transitionFrames = new ArrayList<>();
				if ((sequences.get(i).animationEnd != null) && (isVideo)) {
					sequences.get(i).segue = SegueBuilder.of(sequences.get(i).animationEnd)
							.withSource(sequences.get(i).image).withDestination((i != (sequences.size() - 1))?sequences.get(i + 1).image:sequences.get(0).image)
							.withDuration(sequences.get(i).msTransitionTime, TimeUnit.MILLISECONDS)
							.withMaxFramesPerSecond(sequences.get(i).fps).withAnimationObserver(sequences.get(i))
							.withCompletionObserver(sequences.get(i)).build();
				}
				if (!isVideo) {
					break;
				}
			}
			renderingTime = System.currentTimeMillis();
			if (sequences.get(0).segue != null) {
				sequences.get(0).segue.start();
			} else {
				sequences.get(0).onSegueAnimationCompleted(null);
			}
		}
		while ((calculatedFile == null) && (caughtException == null)) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
		System.out.println("Processing required " + (System.currentTimeMillis() - time) + " ms.");
		if (jobProgress != null) {
			jobProgress.setTotalProcessingTime(System.currentTimeMillis() - time);
			jobProgress.breakOnException();
		}
		return calculatedFile;
	}
	
	private BufferedImage processSuperposers(BufferedImage image, Sequence sequence) throws Exception {
		if (frameSuperposers != null) {
			sequence.setSuperposedElements(true);
			for (FrameSuperposer oneSuperposer: frameSuperposers) {
				Rectangle dRect = oneSuperposer.render(image, dimensionInEffect, notSuperposedRect, this, sequence);
				if (!ELocation.eCenterOnLastSequence.equals(oneSuperposer.getLocation())) oneSuperposer.setUsedRectangle(dRect);
				if (dRect != null) {
					if ((dRect.y <= 50) && (notSuperposedRect.y < dRect.height)) {
						notSuperposedRect.height -= ((dRect.x + dRect.height) - notSuperposedRect.y);
						notSuperposedRect.y = (dRect.x + dRect.height);
					} else if ((dRect.y + dRect.height >= (dimensionInEffect.height - 50)) && (dRect.y < (notSuperposedRect.y + notSuperposedRect.height))) {
						notSuperposedRect.height -= ((notSuperposedRect.y + notSuperposedRect.height) - dRect.y);
					}
				}
			}
		}
		return image;
	}
	
	public FrameSuperposer getFrameSuperposerAtLocation(ELocation location) {
		if (frameSuperposers != null) {
			for (FrameSuperposer oneSuperposer: frameSuperposers) {
				if (location.equals(oneSuperposer.getLocation())) {
					return oneSuperposer;
				}
			}
		}
		return null;
	}

	@Override
	public void onSegueAnimationCompleted(AnimatedSegue segue) {
		try {
			if (jobProgress != null) {
				jobProgress.setOneLinerCustomerFacingStatus("Saving");
				jobProgress.setState(ERenderingState.saving);
				jobProgress.setCurrentTotal(0, effectiveSequence.size());
				jobProgress.store();
			}
			renderingTime = System.currentTimeMillis() - renderingTime;
			System.out.println("Creating final " + fileformat + " file "+ ((isVideo)?("with " + framesCount + " frames"):"") + "(gathering time: " + gatheringTime + " ms, rendering time: " + renderingTime + " ms).");
			if (jobProgress != null) {
				jobProgress.setFramesCount(framesCount);
				jobProgress.setRenderingTime(renderingTime);
				jobProgress.setOneLinerCustomerFacingStatus("Saving");
				jobProgress.setState(ERenderingState.saving);
				jobProgress.setCurrentTotal(0, effectiveSequence.size());
				jobProgress.store();
			}
			File dFile = File.createTempFile("AnimationBuilder_", "." + fileformat);
			ImageOutputStream imageos = ImageIO.createImageOutputStream(new FileOutputStream(dFile));
			// 30 fps means 1 frame each 33ms
			GifSequenceWriter writer = new GifSequenceWriter(imageos, BufferedImage.TYPE_INT_ARGB, 33, true);
			for (Sequence sequence: effectiveSequence) {
				writer.writeToSequence(processSuperposers(sequence.image, sequence), sequence.fixedTime);
				if (sequence.transitionFrames != null) for (BufferedImage frame: sequence.transitionFrames) {
					writer.writeToSequence(processSuperposers(frame, sequence));
				}
				if (jobProgress != null) {
					jobProgress.increaseCurrentTotal();
					jobProgress.store();
				}
			}
			writer.close();
			imageos.close();
			System.out.println("All done: " + dFile.getAbsolutePath());
			calculatedFile = dFile;
		} catch (Exception e) {
			if (jobProgress != null) {
				jobProgress.setExceptionString(StringUtils.getExceptionString(e));
			}
			e.printStackTrace();
		}
	}

	public boolean isLastSequence(Sequence sequence) {
		return ((effectiveSequence.get(effectiveSequence.size() - 1)) == sequence);
	}

	public String getFileformat() {
		return fileformat;
	}

	public void setFileformat(String fileformat) {
		this.fileformat = fileformat;
	}
	
	public boolean isVideo() {
		isVideo = (fileformat.equals("gif"));
		return isVideo;
	}
}
