package core;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import core.AnimationBuilder.Sequence;

public abstract class FrameSuperposer {
	
	@SuppressWarnings("unused")
	private String className;
	private transient Rectangle usedRectangle;
	
	public enum ELocation {
		eTopLeft,
		eTopRight,
		eBottomLeft,
		eBottomRight,
		eRight,
		eLeft,
		eCenterOnLastSequence
	}
	
	protected FrameSuperposer() {
		className = this.getClass().getSimpleName();
	}
	
	public abstract ELocation getLocation();
	
	public abstract Rectangle render(BufferedImage image, Dimension dimension, Rectangle notSuperposedRect, AnimationBuilder caller, Sequence sequence) throws Exception;

	public Rectangle getUsedRectangle() {
		return usedRectangle;
	}

	public void setUsedRectangle(Rectangle usedRectangle) {
		this.usedRectangle = usedRectangle;
	}	
}
