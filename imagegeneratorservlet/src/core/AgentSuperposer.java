package core;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import core.AnimationBuilder.Sequence;

public class AgentSuperposer extends FrameSuperposer {

	private String name;
	private ELocation location;
	private int size;
	private int padding;
	private transient BufferedImage avatar = null;
	private String profileimageurl;

	public AgentSuperposer(String profileimageurl, String name, ELocation location, int size, int padding) throws IOException {
		this.name = name;
		this.location = location;
		this.size = size;
		this.padding = padding;
		this.profileimageurl = profileimageurl;
		if (this.profileimageurl != null) {
	        URL url = new URL(profileimageurl);
			avatar = ImageIO.read(url);
		}
	}
	
	private static Font scaleFontToFit(String text, int width, Graphics g, Font pFont) {
		float fontSize = pFont.getSize();
	    float fWidth = g.getFontMetrics(pFont).stringWidth(text);
	    fontSize = ((float)width / (float)fWidth) * fontSize;
	    return pFont.deriveFont(fontSize);
	}
	
	@Override
	public Rectangle render(BufferedImage image, Dimension dimension, Rectangle notSuperposedRect, AnimationBuilder caller, Sequence sequence) throws Exception {
		Graphics2D graphics2D = (Graphics2D) image.getGraphics().create();
		Rectangle destRect = new Rectangle();
		Rectangle avatarRect = new Rectangle();
		destRect.width = (dimension.width * size)/100;
		graphics2D.setFont(scaleFontToFit(name, destRect.width, graphics2D, graphics2D.getFont()));
		if (avatar != null) {
			avatarRect.width = graphics2D.getFontMetrics().getHeight();
			graphics2D.setFont(scaleFontToFit(name, destRect.width - avatarRect.width - padding, graphics2D, graphics2D.getFont()));
			avatarRect.width = graphics2D.getFontMetrics().getHeight() - padding;
			avatarRect.height = avatarRect.width;
		}
		destRect.width = graphics2D.getFontMetrics().stringWidth(name) + avatarRect.width + padding;
		int height = graphics2D.getFontMetrics().getHeight();
		int adjustmenty = 0;
		if (avatar != null) {
			if (height < (dimension.height * 12)/100) {
				adjustmenty = ((dimension.height * 12)/100) - height;
				height = (dimension.height * 12)/100;
				avatarRect.width = height - padding;
				avatarRect.height = avatarRect.width;
				graphics2D.setFont(scaleFontToFit(name, destRect.width - avatarRect.width - padding, graphics2D, graphics2D.getFont()));
			}
		}
		destRect.height = height;
		switch (location) {
			case eTopLeft:
				destRect.x = padding;
				destRect.y = padding;
				break;
			case eTopRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = padding;
				break;
			case eBottomLeft:
				destRect.x = padding;
				destRect.y = dimension.height - padding - height;
				break;
			case eBottomRight:
				destRect.x = dimension.width - destRect.width - padding;
				destRect.y = dimension.height - padding - height;
				break;
			default:
				throw new Exception("Illegal location.");
		}
		if (avatar != null) {
			avatarRect.x = destRect.x + padding/2;
			avatarRect.y = destRect.y + padding/2;
		}
	    graphics2D.setColor(new Color(1.0f, 1.0f, 1.0f, 0.5f));
	    Rectangle roundedRect = new Rectangle(destRect.x - (padding/2), destRect.y - (padding/2), destRect.width + padding, destRect.height + padding);
	    graphics2D.fillRoundRect(roundedRect.x, roundedRect.y, roundedRect.width, roundedRect.height, 15, 15);
		graphics2D.setColor(Color.BLACK);
		graphics2D.drawString(name, destRect.x + avatarRect.width + padding, destRect.y + graphics2D.getFontMetrics().getMaxAscent() + (adjustmenty/2));
	    if (avatar != null) {
	    	Ellipse2D circle = new Ellipse2D.Double(avatarRect.x, avatarRect.y, avatarRect.width, avatarRect.height);
	    	Shape oldClip = graphics2D.getClip();
	    	graphics2D.clip(circle);
	    	graphics2D.drawImage(avatar, avatarRect.x, avatarRect.y, avatarRect.width, avatarRect.height, null);
	    	graphics2D.setClip(oldClip);
	    }
		graphics2D.dispose();
		return roundedRect;
	}
	
	@Override
	public ELocation getLocation() {
		return location;
	}

}
