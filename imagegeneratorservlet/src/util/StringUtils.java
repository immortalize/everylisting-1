package util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.apache.commons.text.StringEscapeUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;

/**
 * Utility class with string related services (not meant to be instantiated, just call the static methods)
 * @author Philippe Roy
 */
public class StringUtils {
	
	private StringUtils() {}
	
	/**
	 * @param dString the string to calculate against
	 * @return the crc32 corresponding value
	 */
	public static long calculateCrc32ofString(String dString) {
		byte bytes[] = dString.getBytes();
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        return checksum.getValue();
	}
	
	/**
	 * @param input the string to process
	 * @param match the regular-expression to count
	 * @param escape if the regular-expression could contain regex sensitive characters, put escape to true so they do not generate an error
	 * @return how many times input matches the regular-expression
	 */
	public static int countMatches(String input, String match, boolean escape) {
		int count = 0;
		if (input != null) {
			Pattern p = Pattern.compile((escape?"\\Q":"") + match + (escape?"\\E":""));
			Matcher m = p.matcher(input);
			while (m.find()) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * @param input the string to analyze
	 * @param match the regular expression to match
	 * @param group the group of interest from the regular expression
	 * @return the first match with the group value, null if not found
	 */
	public static String regexMatch(String input, String match, int group) {
		if (input != null) {
			Pattern p = Pattern.compile(match);
			Matcher m = p.matcher(input);
			if (m.find()) {
				return m.group(group);
			}
		}
		return null;
	}
	
	public static String convertExtendedAsciiToUTF8(String input) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < input.length(); i++) {
			if ((input.charAt(i) > 0x7F) && (input.charAt(i) <= 0xFF)) {
				sb.append(new String(java.nio.charset.StandardCharsets.UTF_8.encode("" + input.charAt(i)).array()).charAt(0));
			} else {
				sb.append(input.charAt(i));
			}
		}
		return sb.toString();
	}
	
	/**
	 * @param dString the string to analyze
	 * @return the MD5 related to the string
	 * @throws NoSuchAlgorithmException
	 */
	public static String calculateMD5ofString(String dString) throws NoSuchAlgorithmException {
		if (dString != null) {
			MessageDigest md = MessageDigest.getInstance("MD5");
		    md.update(dString.getBytes());
		    byte byteData[] = md.digest();
		    StringBuffer sb = new StringBuffer();
		    for (int i = 0; i < byteData.length; i++) {
		    	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		    }
		    return sb.toString();
		} else {
			return null;
		}
	}
	
	/**
	 * @param dString the string to analyze
	 * @return the MD5 related to the string
	 * @throws NoSuchAlgorithmException
	 */
	public static String calculateMD5ofStringHex(String dString) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(dString.getBytes());
	    byte byteData[] = md.digest();
	    StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
	     	if (hex.length() == 1) hexString.append('0');
	     	hexString.append(hex);
		}
		return hexString.toString();
	}
	
	/**
	 * @param name the dot notation json node key
	 * @param value the value corresponding to the node
	 * @return the corresponding json string
	 * @throws JsonProcessingException
	 */
	@SuppressWarnings("unchecked")
	public static String dotNotationToJSON(String name, String value) throws JsonProcessingException {
		Map<String, Object> root = new HashMap<String, Object>();
	    Map<String, Object> m = root;
	    String[] parts = name.split("\\.");
	    for (Integer i = 0; i < parts.length; i++) {
	        String part = parts[i];
	        if (i == parts.length - 1) {
	            // At last part so put value
	            m.put(part, value);
	        } else {
	            // Before last part so put map
	            Map<String, Object> mm = (Map<String, Object>) m.get(part);
	            if (mm == null) {
	                mm = new HashMap<String, Object>();
	                m.put(part, mm);
	            }
	            m = mm;
	        }
	    }
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(root);
	}
	
	/**
	 * @param json the json string
	 * @param key the key to locate and factor its value
	 * @param factor the factor to apply to the node
	 * @return the modified json string
	 * @throws IOException 
	 */
	public static String factorInNode(String json, String key, Float factor) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = defensiveReadTree(json, mapper);
        JsonNode parentNode = null;
        String[] tokens = key.split("\\.");
        JsonNode node = rootNode;
        for (String token: tokens) {
        	parentNode = node;
        	node = node.get(token);
        	if (node == null) {
        		break;
        	}
        }
        if (node != null) {
        	float value = Float.parseFloat(StringUtils.reformatAsNumber(node.asText(), null, false));
        	value *= factor;
        	((ObjectNode)parentNode).put(tokens[tokens.length-1], "" + value);
        }
        return mapper.writeValueAsString(rootNode);
	}
	
	private static void addKeys(String currentPath, JsonNode jsonNode, Map<String, String> map, boolean terminalsOnly) {
	    if (jsonNode.isObject()) {
	        ObjectNode objectNode = (ObjectNode) jsonNode;
	        Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
	        String pathPrefix = currentPath.isEmpty() ? "" : currentPath + ".";
	        if ((!terminalsOnly) && (currentPath != null) && (currentPath.length() > 0)) {
	        	map.put(currentPath, jsonNode.toString());
	        }
	        while (iter.hasNext()) {
	            Map.Entry<String, JsonNode> entry = iter.next();
	            addKeys(pathPrefix + entry.getKey(), entry.getValue(), map, terminalsOnly);
	        }
	    } else if (jsonNode.isArray()) {
	        ArrayNode arrayNode = (ArrayNode) jsonNode;
	        if ((!terminalsOnly) && (currentPath != null) && (currentPath.length() > 0)) {
	        	map.put(currentPath, jsonNode.toString());
	        }
	        for (int i = 0; i < arrayNode.size(); i++) {
	            addKeys(currentPath, arrayNode.get(i), map, terminalsOnly);
	        }
	    } else if (jsonNode.isValueNode()) {
	        ValueNode valueNode = (ValueNode) jsonNode;
	        map.put(currentPath, valueNode.asText());
	    }
	}
	
	public static Map<String, String> getJsonNodesMap(String json, boolean terminalsOnly) throws IOException {
		JsonNode root = defensiveReadTree(json);
		Map<String, String> map = new HashMap<>();
		addKeys("", root, map, terminalsOnly);
		return map;
	}
	
	public static String ensureArrayElementsUnicity(String json) throws IOException {
		if (json == null) return json;
		ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = defensiveReadTree(json, mapper);
        if (rootNode != null) {
	        List<Entry<String, JsonNode>> nodes = new ArrayList<>();
	        Iterator<Entry<String, JsonNode>> it = rootNode.fields();
	        while (it.hasNext()) {
	        	nodes.add(it.next());
	        }
			for (Entry<String, JsonNode> searchNode : nodes) {
	    		if (searchNode.getValue() instanceof ArrayNode) {
	    			ArrayNode dUnicityArray = mapper.createArrayNode();
	    			ArrayNode dOriginalArray = (ArrayNode)searchNode.getValue();
	    			Set<String> jsonentries = new HashSet<>();
	    			for (JsonNode dNode: dOriginalArray) {
	    				if (dNode != null) {
		    				String jsonentry = ensureArrayElementsUnicity(mapper.writeValueAsString(dNode));
		    				if (!jsonentries.contains(jsonentry)) {
		    					jsonentries.add(jsonentry);
		    					if (!"{}".equals(mapper.writeValueAsString(dNode))) {
		    						dUnicityArray.add(defensiveReadTree(jsonentry, mapper));
		    					}
		    				}
	    				}
	    			}
	    			((ObjectNode)rootNode).set(searchNode.getKey(), dUnicityArray);
	    		} else {
	    			searchNode.setValue(defensiveReadTree(ensureArrayElementsUnicity(mapper.writeValueAsString(searchNode.getValue())), mapper));
	    		}
	        }
	        return mapper.writeValueAsString(rootNode);
        } else {
        	return json;
        }
	}
	
	private static JsonNode defensiveReadTree(String json) throws IOException {
		return defensiveReadTree(json, new ObjectMapper());
	}
	
	private static JsonNode defensiveReadTree(String json, ObjectMapper mapper) throws IOException {
		if (json == null) return null;
		JsonNode rootNode = null;
		try {
        	rootNode = mapper.readTree(json);
        } catch (JsonParseException e) {
        	try {
	        	json = StringEscapeUtils.unescapeJava(json);
	        	rootNode = mapper.readTree(json);
			} catch (JsonSyntaxException | JsonParseException e2) {
				File dFile = File.createTempFile("defensiveReadTree_", ".json");
				Files.write(dFile.toPath(), json.getBytes());
				throw new IOException("Json content exception in StringUtils.defensiveReadTree. Full json at " + dFile.getAbsolutePath(), e);
			}
        }
		return rootNode;
	}
	
	/**
	 * @param json the json to locate the node from
	 * @param nodePath the path of the node to locate
	 * @return the node (or null if not found)
	 * @throws IOException
	 */
	public static JsonNode locateNode(String json, String nodePath) throws IOException {
        JsonNode rootNode = defensiveReadTree(json);
        String[] tokens = nodePath.split("\\.");
        if (nodePath.startsWith("literal:")) {
        	tokens = new String[] {nodePath.substring("literal:".length()).trim()};
        }
        JsonNode node = rootNode;
        for (String token: tokens) {
        	JsonNode searchNode = node;
        	if (node != null) {
	        	node = node.get(token);
	        	if (node == null) {
	        		if (searchNode instanceof ArrayNode) {
	        			ArrayNode dArray = (ArrayNode)searchNode;
	        			for (JsonNode dNode: dArray) {
	        				node = dNode.get(token);
	        				if (node != null) {
	        					break;
	        				}
	        			}
	        			if (node == null) {
	    					break;
	    				}
	        		} else {
	        			break;
	        		}
	        	}
        	}
        }
        return node;
	}
	
	/**
	 * @param json the json to extract the value from
	 * @param nodePath the path of the node to get
	 * @return the node value (or null if not found)
	 * @throws IOException 
	 */
	public static String getNodeValue(String json, String nodePath) throws IOException {
		JsonNode node = locateNode(json, nodePath);
        if (node != null) {
        	if (node.textValue() != null) {
        		return node.textValue();
        	} else {
        		return node.toString();
        	}
        }
        return null;
	}
	
	/**
	 * @param json the json to extract the value from
	 * @param nodePath the path of the node to get
	 * @param value the value to set
	 * @return the new json (or null if the node did not exist before)
	 * @throws IOException 
	 */
	@SuppressWarnings("deprecation")
	public static String setNodeToValue(String json, String nodePath, String value) throws IOException {
		if (json == null) return null;
		ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = defensiveReadTree(json, mapper);
        JsonNode parentNode = null;
        String[] tokens = nodePath.split("\\.");
        JsonNode node = rootNode;
        int curTokenNdx = 0;
        for (String token: tokens) {
        	parentNode = node;
        	node = node.get(token);
        	if (node == null) {
        		if (ObjectNode.class.isInstance(parentNode)) {
	        		((ObjectNode)parentNode).set(token, mapper.createObjectNode());
	        		node = parentNode.get(token);
        		} else if (ArrayNode.class.isInstance(parentNode)) {
        			int elemNdx = 0;
        			for (JsonNode elem: ((ArrayNode)parentNode)) {
        				String curNodePath = "";
        				for (int i = curTokenNdx; i < tokens.length; i++) {
        					if (curNodePath.length() > 0) {
        						curNodePath += ".";
        					}
        					curNodePath += tokens[i];
        				}
        				String modJson = setNodeToValue(elem.toString(), curNodePath, value);
        				elem = (modJson != null)?defensiveReadTree(modJson, mapper):null;
        				((ArrayNode)parentNode).set(elemNdx, elem);
        				elemNdx++;
        			}
        			return mapper.writeValueAsString(rootNode);
        		}
        	}
        	curTokenNdx++;
        }
        if (node != null) {
        	if (value != null) {
        		if (((!(value.startsWith("\"")) || (value.startsWith("'"))) || (!((value.endsWith("\"")) || (value.endsWith("'"))))) && (value.contains("{") || value.contains("["))) {
            		try {
	        			JsonNode valueNode = defensiveReadTree(value, mapper);
	            		((ObjectNode)parentNode).put(tokens[tokens.length-1], valueNode);
            		} catch (Exception e) {
            			((ObjectNode)parentNode).put(tokens[tokens.length-1], value);
            		}
        		} else {
        			((ObjectNode)parentNode).put(tokens[tokens.length-1], value);
        		}
        	} else if (((ObjectNode)parentNode).has(tokens[tokens.length-1])) {
        		((ObjectNode)parentNode).remove(tokens[tokens.length-1]);
        	}
        	return mapper.writeValueAsString(rootNode);
        }
        return null;
	}
	
	/**
	 * @param json the json to extract the value from
	 * @param nodePaths the paths of the nodes to get
	 * @return the node value in each array entry (or null if not found)
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static String[] getNodeValues(String json, String[] nodePaths, java.util.List<Integer> origins) throws IOException {
		java.util.List<String> dReturn = (java.util.List<String>) new ArrayList<String>();
		if (json != null) {
			ObjectMapper mapper = new ObjectMapper();
	        JsonNode rootNode = defensiveReadTree(json, mapper);
	        int originNdx = 0;
	        for (String nodePath: nodePaths) {
	        	boolean wasArray = false;
		        String[] tokens = nodePath.split("\\.");
		        JsonNode node = rootNode;
		        JsonNode lastOKNode = node;
		        int tokenIndex = 0;
		        for (String token: tokens) {
		        	if (node != null) {
		        		lastOKNode = node;
		        		node = node.get(token);
		        	}
		        	if (node == null) {
		        		if ((lastOKNode != null) && (lastOKNode.isArray())) {
		        			String dNodePath = nodePath.substring(nodePath.indexOf("." + token) + 1);
		        			for (Iterator<JsonNode> it = lastOKNode.elements(); it.hasNext();) {
		        				node = it.next();
		        				if (node.isObject()) {
		        					String[] res = getNodeValues(mapper.writeValueAsString(node), new String[] {dNodePath}, null);
		        					if ((res != null) && (res.length > 0)) {
		        						for (String oneres: res) {
		        				        	dReturn.add(oneres);
		        				        	if (origins != null) origins.add(originNdx);
		        						}
		        					}
		        				} else if (dNodePath.length() == 0) {
	    				        	dReturn.add(node.asText());
	    				        	if (origins != null) origins.add(originNdx);
		        				}
		        			}
		        			wasArray = true;
		        			break;
		        		} else {
		        			break;
		        		}
		        	} else if ((node.isArray()) && (tokenIndex == (tokens.length - 1))) {
		        		for (Iterator<JsonNode> it = node.elements(); it.hasNext();) {
		        			JsonNode elnode = it.next();
	        				if (elnode.isObject()) {
	        					dReturn.add(mapper.writeValueAsString(elnode));
	        				} else {
	        					dReturn.add(elnode.asText());
	        				}
		        		}
		        		wasArray = true;
		        	}
		        	tokenIndex++;
		        }
		        if (!wasArray) {
			        if (node != null) {
			        	dReturn.add(node.textValue());
			        	if (origins != null) origins.add(originNdx);
			        } else {
			        	dReturn.add(null);
			        	if (origins != null) origins.add(originNdx);
			        }
		        }
		        originNdx++;
	        }
		}
        return (String[]) ((ArrayList) dReturn).toArray(new String[dReturn.size()]);
	}
	
	private static boolean contains(JsonArray array, JsonElement element) {
		Map<String, String> objMap = new Gson().fromJson(element, new TypeToken<HashMap<String, String>>() {}.getType());
		List<String> arrayVals = new Gson().fromJson(array, new TypeToken<List<String>>() {}.getType());
		for (Entry<String, String> entry : objMap.entrySet()) {
			for (String val : arrayVals) {
				if (entry.getKey().equals(val)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Merges two jsons into one
	 * @param json1InPriority the priority json (if both have the same node, this ones has precedence over the other json)
	 * @param json2AsFallback missing nodes from the first json are populated with this node
	 * @return the merged json
	 */
	@SuppressWarnings("deprecation")
	public static String mergeJSONs(String json1InPriority, String json2AsFallback) {
		JsonObject json1Obj = new JsonParser().parse(json1InPriority).getAsJsonObject();
		JsonObject json2Obj = new JsonParser().parse(json2AsFallback).getAsJsonObject();
        Set<Entry<String, JsonElement>> entrySet1 = json1Obj.entrySet();
        for (Entry<String, JsonElement> entry : entrySet1) {
            String key1 = entry.getKey();
            if (json2Obj.get(key1) != null) {
                JsonElement tempEle2 = json2Obj.get(key1);
                JsonElement tempEle1 = entry.getValue();
                if (tempEle2.isJsonArray() && tempEle1.isJsonArray()) {
                	JsonArray tempArray2 = (JsonArray)tempEle2;
                	JsonArray tempArray1 = (JsonArray)tempEle1;
                	for (JsonElement el :tempArray2) {
                		if (!contains(tempArray1, el)) {
                			tempArray1.add(el);
                		}
                	}
                } else if (tempEle2.isJsonObject() && tempEle1.isJsonObject()) {
                    JsonObject mergedObj = new JsonParser().parse(mergeJSONs(tempEle1.getAsJsonObject().toString(), tempEle2.getAsJsonObject().toString())).getAsJsonObject();
                    entry.setValue(mergedObj);
                }
            }
        }
        Set<Entry<String, JsonElement>> entrySet2 = json2Obj.entrySet();
        for (Entry<String, JsonElement> entry : entrySet2) {
            String key2 = entry.getKey();
            if (json1Obj.get(key2) == null) {
                json1Obj.add(key2, entry.getValue());
            }
        }
        JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement el = parser.parse(json1Obj.toString());
        return gson.toJson(el);
    }
	
	public static String formatNumberToPrecision(double value, int precision) {
		double rounded = Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
		String dReturn = "" + rounded;
		while (dReturn.endsWith("0") || (dReturn.endsWith("."))) {
			if (dReturn.endsWith(".")) {
				dReturn = dReturn.substring(0, dReturn.length() - 1);
				break;
			} else {
				dReturn = dReturn.substring(0, dReturn.length() - 1);
			}
		}
		return dReturn;
	}
	
	public static String formatSize(long value) {
		if (value == 0) {
			return "empty";
		}
		StringBuffer dReturn = new StringBuffer();
		if (value > 1024 * 1024 * 1024) {
			dReturn.append(formatNumberToPrecision((double)((double)value/(1024.0 * 1024.0 * 1024.0)), 1) + " GB");
		} else if (value > 1024 * 1024) {
			dReturn.append(formatNumberToPrecision((double)((double)value/(1024.0 * 1024.0)), 1) + " MB");
		} else if (value > 1024) {
			dReturn.append(formatNumberToPrecision((double)((double)value/(1024.0)), 1) + "K");
		} else {
			dReturn.append(value + " bytes");
		}
		return dReturn.toString();
	}
	
	@SuppressWarnings("deprecation")
	public static String prettyPrintJson(String json) {
		if (json == null) return null;
		JsonParser parser = new JsonParser();
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING);
		Gson gson = gsonBuilder.setPrettyPrinting().create();
		JsonElement el = parser.parse(json);
		String dReturn = gson.toJson(el);
		dReturn = dReturn.replaceAll("\"([^\"]*?)\"[ ]?:[ ]?([-]?[0-9]*[.]?[0-9]*),", "\"$1\": \"$2\",");
		return dReturn;
	}
	
	/**
	 * Formats a delay in milliseconds
	 * @param milliseconds the elapsed time
	 * @return the customer-facing string representing the elapsed time
	 */
	public static String formatTime(long milliseconds) {
		if (milliseconds == 0) {
			return "immediate";
		}
		StringBuffer dReturn = new StringBuffer();
		if (milliseconds > (7 * 24 * 60 * 60 * 1000)) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append((long)(milliseconds/(long)(7 * 24 * 60 * 60 * 1000)) + " week");
			milliseconds -= (long)(milliseconds/(long)(7 * 24 * 60 * 60 * 1000)) * (long)(7 * 24 * 60 * 60 * 1000);
		}
		if (milliseconds > (24 * 60 * 60 * 1000)) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append((long)(milliseconds/(long)(24 * 60 * 60 * 1000)) + " day");
			milliseconds -= (long)(milliseconds/(long)(24 * 60 * 60 * 1000)) * (long)(24 * 60 * 60 * 1000);
		}
		if (milliseconds > (60 * 60 * 1000)) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append((long)(milliseconds/(long)(60 * 60 * 1000)) + " hr");
			milliseconds -= (long)(milliseconds/(long)(60 * 60 * 1000)) * (long)(60 * 60 * 1000);
		}
		if (milliseconds > (60 * 1000)) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append((long)(milliseconds/(long)(60 * 1000)) + " min");
			milliseconds -= (long)(milliseconds/(long)(60 * 1000)) * (long)(60 * 1000);
		}
		if (milliseconds > 1000) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append((long)(milliseconds/(long)1000) + " s");
			milliseconds -= (long)(milliseconds/(long)1000) * (long)1000;
		}
		if (milliseconds > 0) {
			if (dReturn.length() > 0) {
				dReturn.append(" ");
			}
			dReturn.append("" + milliseconds + " ms");
		}
		return dReturn.toString();
	}
	
	/**
	 * @param dContent the string to analyze
	 * @return the string with unicode escapes in it
	 */
	public static String convertToUnicodeEscapes(String dContent) {
		StringBuilder b = new StringBuilder();
	    for (char c : dContent.toCharArray()) {
	        if (c >= 128)
	            b.append("\\u").append(String.format("%04X", (int)c));
	        else
	            b.append(c);
	    }
	    return b.toString();
	}
	
	/**
	 * @param input the string to analyze
	 * @return only the digits from the string returned here
	 */
	public static String extractDigits(String input) {
		if (input != null) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < input.length(); i++) {
				if ((input.charAt(i) >= '0') && (input.charAt(i) <= '9')) {
					sb.append(input.charAt(i));
				}
			}
			return sb.toString();
		} else {
			return "";
		}
	}
	
    private static final String NAMES[] = new String[] {
            "Thousand_K",
            "Million_M",
            "Billion_B",
            "Trillion_T",
            "Quadrillion",
            "Quintillion",
            "Sextillion",
            "Septillion",
            "Octillion",
            "Nonillion",
            "Decillion",
            "Undecillion",
            "Duodecillion",
            "Tredecillion",
            "Quattuordecillion",
            "Quindecillion",
            "Sexdecillion",
            "Septendecillion",
            "Octodecillion",
            "Novemdecillion",
            "Vigintillion",
        };
        private static final BigInteger THOUSAND = BigInteger.valueOf(1000);
        private static final NavigableMap<BigInteger, String> MAP;
        static {
            MAP = new TreeMap<BigInteger, String>();
            for (int i = 0; i < NAMES.length; i++)
            {
                MAP.put(THOUSAND.pow(i+1), NAMES[i]);
            }
        }

    private static String createString(BigInteger number) {
        Entry<BigInteger, String> entry = MAP.floorEntry(number);
        if (entry == null) {
            return "Nearly nothing";
        }
        BigInteger key = entry.getKey();
        BigInteger d = key.divide(THOUSAND);
        BigInteger m = number.divide(d);
        float f = m.floatValue() / 1000.0f;
        float rounded = ((int)(f * 10.0))/10.0f;
        String dReturn;
        if ((("" + (int)rounded).length() == 1) && (rounded % 1 != 0)) {
        	dReturn = rounded + ((entry.getValue().indexOf("_") == -1)?(" " + entry.getValue()):(entry.getValue().substring(entry.getValue().indexOf("_") + 1)));
        } else {
            dReturn = ((int)rounded) + ((entry.getValue().indexOf("_") == -1)?(" " + entry.getValue()):(entry.getValue().substring(entry.getValue().indexOf("_") + 1)));
        }
        return dReturn;
    }
	
	public static String formatBigFinancial(BigInteger price, String prefix, String suffix) {
		if (price.compareTo(BigInteger.valueOf(1000)) == 1) {
			return ((prefix != null)?prefix:"") + createString(price) + ((suffix != null)?suffix:"");
		} else {
			return formatFinancial(price.doubleValue(), prefix, suffix);
		}
	}
	
	public static String formatFinancial(Double price, String prefix, String suffix) {
		if (price % 1 == 0) {
			return ((prefix != null)?prefix:"") + priceWithoutDecimal(price) + ((suffix != null)?suffix:"");
		} else {
			return ((prefix != null)?prefix:"") + priceWithDecimal(price) + ((suffix != null)?suffix:"");
		}
	}
	
	public static String priceWithDecimal(Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###,###,###.00");
	    return formatter.format(price);
	}

	public static String priceWithoutDecimal(Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###,###,###.##");
	    return formatter.format(price);
	}
	
	/**
	 * @param dNumberStr the string to format
	 * @return the reformatted as number string
	 */
	public static String reformatAsNumber(String dNumberStr, List<String> allPossibleValues, boolean bestEffortOnly) {
		final String[] KSUFFIXES = {"million:1000000", "k:1000", "milliones:1000000"};
		if ((dNumberStr == null) || (dNumberStr.length() == 0)) {
			if (allPossibleValues != null) {
				allPossibleValues.add(dNumberStr);
			}
			return dNumberStr;
		} else if (dNumberStr.startsWith(".")) {
			dNumberStr = dNumberStr.trim();
			dNumberStr = "0" + dNumberStr;
		} else {
			dNumberStr = dNumberStr.trim();
		}
		if ((!((dNumberStr.charAt(0) >= '0') && (dNumberStr.charAt(0) <= '9'))) && (dNumberStr.charAt(0) != '-')) {
			if (allPossibleValues != null) {
				allPossibleValues.add(dNumberStr);
			}
			try {
				throw new RuntimeException("Unable to convert to number the string '" + dNumberStr + "'.");
			} catch (RuntimeException e) {
				if (!bestEffortOnly) {
					e.printStackTrace();
				}
			}
			return null;
		}
		double nluSuffixFactor = 1;
		String dReturn = "0";
		try {
			DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
			df.setMaximumIntegerDigits(340);
			df.setMaximumFractionDigits(340); 
			int exponentCount = StringUtils.countMatches(dNumberStr, "[Ee]", false);
			if (exponentCount == 1) {
				String[] parts = dNumberStr.split("[Ee]");
				if (parts.length == 2) {
					int exponent = Integer.parseInt(parts[1]);
					while ((parts[0].contains(".")) && (exponent > 0)) {
						int lastpos = parts[0].indexOf('.');
						parts[0] = ((dNumberStr.charAt(0) == '-')?"-":"") + StringUtils.extractDigits(parts[0]);
						parts[0] = parts[0].substring(0, lastpos+1) + "." + parts[0].substring(lastpos+1);
						if (parts[0].endsWith(".")) {
							parts[0] = parts[0].substring(0, parts[0].length() - 1);
						}
						exponent--;
					}
					if (exponent > 0) {
						dNumberStr = df.format(new Double(Double.parseDouble(parts[0]) * (Math.pow(10, exponent))));
					} else {
						dNumberStr = df.format(new Double(Double.parseDouble(parts[0])));
					}
				}
			}
			int commaCounts = StringUtils.countMatches(dNumberStr, ",", true);
			int periodCount = StringUtils.countMatches(dNumberStr, "[\\.]", false);
			char lastSeparator;
			if (dNumberStr.lastIndexOf('.') > dNumberStr.lastIndexOf(',')) {
				lastSeparator = '.';
			} else {
				lastSeparator = ',';
			}
			String[] commaAndPeriodSeparated = dNumberStr.split("[,\\.]");
			if (commaAndPeriodSeparated.length > 0) {
				for (String oneSuffixPair: KSUFFIXES) {
					String suffix = oneSuffixPair.split(":")[0];
					double factor = Double.parseDouble(oneSuffixPair.split(":")[1]);
					if (commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].toLowerCase().endsWith(suffix.toLowerCase())) {
						commaAndPeriodSeparated[commaAndPeriodSeparated.length-1] = commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].substring(0, commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].length() - suffix.length()).trim();
						nluSuffixFactor = factor;
						break;
					}
				}
			}
			Double doubleval = null;
			double divider = 1;
			if ((commaCounts == 0) && (periodCount == 0)) {
				doubleval = Double.parseDouble(StringUtils.extractDigits(dNumberStr)) * ((dNumberStr.charAt(0) == '-')?-1:1);
			} else {
				if ((commaAndPeriodSeparated.length > 1) && (((commaCounts == 1) && (lastSeparator == ',') && (commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].length() == 2)) || ((periodCount == 1) && (lastSeparator == '.')))) {
					if (commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].length() > 0) {
						divider = Math.pow(10, commaAndPeriodSeparated[commaAndPeriodSeparated.length-1].length());
						doubleval = Double.parseDouble(StringUtils.extractDigits(dNumberStr)) * ((dNumberStr.charAt(0) == '-')?-1:1);
					}
					if (((commaCounts + periodCount) == 1) && (allPossibleValues != null)) {
						allPossibleValues.add("" + doubleval);
					}
				}
			}
			if (doubleval == null) {
				doubleval = Double.parseDouble(StringUtils.extractDigits(dNumberStr)) * ((dNumberStr.charAt(0) == '-')?-1:1);
			}
			dReturn = df.format((doubleval/divider) * nluSuffixFactor);
			dReturn = standardizeValue(dReturn);
		} catch (NumberFormatException e) {}
		if (allPossibleValues != null) {
			for (int i = 0; i < allPossibleValues.size(); i++) {
				allPossibleValues.set(i, standardizeValue(allPossibleValues.get(i)));
			}
			allPossibleValues.add(0, dReturn);
		}
		if (dReturn == null) {
			try {
				throw new RuntimeException("Unable to convert to number the string '" + dNumberStr + "'.");
			} catch (RuntimeException e) {
				if (!bestEffortOnly) {
					e.printStackTrace();
				}
			}
		}
		return dReturn;
	}
	
	private static String standardizeValue(String dValue) {
		while (((dValue.indexOf('.') != -1) || (dValue.indexOf(',') != -1)) && ((dValue.endsWith("0")) || (dValue.endsWith(".")) || (dValue.endsWith(",")))) {
			dValue = dValue.substring(0, dValue.length() - 1);
		}
		if (dValue.length() == 0) {
			dValue = "0";
		}
		return dValue;
	}

	/**
	 * @param input the string to analyze
	 * @return all the characters that are not numbers before the first number
	 */
	public static String beforeDigits(String input) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < input.length(); i++) {
			if ((input.charAt(i) >= '0') && (input.charAt(i) <= '9')) {
				return sb.toString();
			} else {
				sb.append(input.charAt(i));
			}
		}
		return null;
	}
	
	// Example implementation of the Levenshtein Edit Distance
	// See http://rosettacode.org/wiki/Levenshtein_distance#Java
	/**
	 * @param s1 string 1 to analyze
	 * @param s2 string 2 to analyze
	 * @return the Levenshtein Edit Distance result
	 */
	private static int editDistance(String s1, String s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		int[] costs = new int[s2.length() + 1];
		for (int i = 0; i <= s1.length(); i++) {
			int lastValue = i;
			for (int j = 0; j <= s2.length(); j++) {
				if (i == 0) {
					costs[j] = j;
				} else {
					if (j > 0) {
						int newValue = costs[j - 1];
						if (s1.charAt(i - 1) != s2.charAt(j - 1)) {
							newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
						}
						costs[j - 1] = lastValue;
						lastValue = newValue;
					}
				}
			}
			if (i > 0) {
				costs[s2.length()] = lastValue;
			}
		}
		return costs[s2.length()];
	}
	
	/**
	 * @param source the string to analyze
	 * @param findStr the string to find (in regular-expression)
	 * @param replaceChar
	 * @param withChar
	 * @param escape
	 * @return the string with replacements made
	 */
	public static String replaceCaseRegEx(String source, String findStr, char replaceChar, char withChar, boolean escape) {
		StringBuilder sb = new StringBuilder();
		int tracer = 0;
		Pattern p = Pattern.compile("\\Q" + findStr.replace("" + replaceChar, "\\E(" + (escape?"\\":"") + replaceChar + ")\\Q") + "\\E", Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(source);
		while ((m.find()) && (tracer < source.length())) {
			if (m.start(0) != m.end(0)) {
				for (int g = 1; g <= m.groupCount(); g++) {
					sb.append(source.substring(tracer, m.start(g)));
					tracer = m.end(g);
					sb.append(withChar);
				}
			} else {
				sb.append(source.substring(tracer, m.start(0)));
			}
			tracer = m.end(0);
		}
		if (sb.length() == 0) {
			return source;
		} else {
			if (tracer < source.length()) sb.append(source.substring(tracer));
			return sb.toString();
		}
	}
	
	/**
	 * Calculates the similarity (a number within 0 and 1) between two strings.
	 * @param s1 the string 1
	 * @param s2 the string 2
	 * @return a value between 0 and 1 corresponding to the similarity
	 */
	public static double similarity(String s1, String s2) {
	  String longer = s1, shorter = s2;
	  if (s1.length() < s2.length()) { // longer should always have greater length
	    longer = s2; shorter = s1;
	  }
	  int longerLength = longer.length();
	  if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
	  return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
	}

	/**
	 * @param string the string to analyze
	 * @return the postgres consumable string
	 */
	public static String escapeStringForPostgres(String string) {
		if (string != null) {
			string = string.replace("'", "''").replace("\\", "\\\\").replace("(", "\\(").replace(")", "\\)").replace("[", "\\[").replace("]", "\\]").replace("{", "\\{").replace("}", "\\}");
		}
		return string;
	}

	public static String slugify(String keyphrase) {
		final Pattern NONLATIN = Pattern.compile("[^\\w-]");
		final Pattern WHITESPACE = Pattern.compile("[\\s]");
	    String nowhitespace = WHITESPACE.matcher(keyphrase).replaceAll("-");
	    String normalized = Normalizer.normalize(nowhitespace, Form.NFD);
	    String slug = NONLATIN.matcher(normalized).replaceAll("");
	    return slug.toLowerCase(Locale.ENGLISH);
	}
	
	public static String[] translateCommandline(String toProcess) {
	    if (toProcess == null || toProcess.length() == 0) {
	        //no command? no string
	        return new String[0];
	    }
	    // parse with a simple finite state machine

	    final int normal = 0;
	    final int inQuote = 1;
	    final int inDoubleQuote = 2;
	    int state = normal;
	    final StringTokenizer tok = new StringTokenizer(toProcess, "\"\' ", true);
	    final ArrayList<String> result = new ArrayList<String>();
	    final StringBuilder current = new StringBuilder();
	    boolean lastTokenHasBeenQuoted = false;

	    while (tok.hasMoreTokens()) {
	        String nextTok = tok.nextToken();
	        switch (state) {
	        case inQuote:
	            if ("\'".equals(nextTok)) {
	                lastTokenHasBeenQuoted = true;
	                state = normal;
	            } else {
	                current.append(nextTok);
	            }
	            break;
	        case inDoubleQuote:
	            if ("\"".equals(nextTok)) {
	                lastTokenHasBeenQuoted = true;
	                state = normal;
	            } else {
	                current.append(nextTok);
	            }
	            break;
	        default:
	            if ("\'".equals(nextTok)) {
	                state = inQuote;
	            } else if ("\"".equals(nextTok)) {
	                state = inDoubleQuote;
	            } else if (" ".equals(nextTok)) {
	                if (lastTokenHasBeenQuoted || current.length() != 0) {
	                    result.add(current.toString());
	                    current.setLength(0);
	                }
	            } else {
	                current.append(nextTok);
	            }
	            lastTokenHasBeenQuoted = false;
	            break;
	        }
	    }
	    if (lastTokenHasBeenQuoted || current.length() != 0) {
	        result.add(current.toString());
	    }
	    if (state == inQuote || state == inDoubleQuote) {
	        throw new RuntimeException("unbalanced quotes in " + toProcess);
	    }
	    return result.toArray(new String[result.size()]);
	}

	public static String getExceptionString(Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}
}
