#!/bin/bash

if [[ $APP_ENV == "prod" ]]
then

./setup.sh << EOF
http://everylisting.com
notmerge
EOF

sed -i 's|http|https|g' import.changed.sql
mysql -u wordpress -peverylisting365247prod -P 3306 -h 10.65.112.5 -e \
     "source import.changed.sql;"

fi


if [[ $APP_ENV == "dev" ]]
then

./setup.sh << EOF
http://dev.everylisting.com
notmerge
EOF

sed -i 's|http|https|g' import.changed.sql
mysql -u wordpress -peverylisting365247 -P 3306 -h 10.65.112.3 -e \
       "drop everylisting_dev; source import.changed.sql;"

fi

#end