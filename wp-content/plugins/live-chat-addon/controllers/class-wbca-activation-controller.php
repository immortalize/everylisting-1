<?php

class wbca_Activation_Controller {

    public function initialize_activation_hooks() {
        register_activation_hook("live-chat-addon/wpbot-chat-addon.php", array($this, 'execute_activation_hooks'));
		register_activation_hook("live-chat-addon/wpbot-chat-addon.php", array($this, 'add_operator_roles'));
		
		add_action('wp_login', array($this, 'update_user_login'), 10, 2);
		add_action('clear_auth_cookie', array($this, 'update_login_status'), 10);
    }

    public function execute_activation_hooks() {
		
        $database_manager = new wbca_Database_Manager();
		
        $database_manager->create_custom_tables();
		        
    }
	
	public function execute_deactivation_hooks() {
		flush_rewrite_rules();
		// Will be executed when the client deactivates the plugin
    }
	public function add_operator_roles() {
        add_role('operator', 'Operator', array('read' => true));
		$role = get_role('operator');

        $operator_capabilities = array(
            "edit_posts",
            "edit_published_posts",
            "publish_posts",
            "read",
            "delete_posts",
        );

        foreach ($operator_capabilities as $capability) {
            $role->add_cap($capability);
        }
		
		// https://wordpress.org/plugins/members/ capability manager

    }
	public function flush_application_rewrite_rules() {
		flush_rewrite_rules();
	}
	
	public function update_user_login( $user_login, $user ) {
		// your code
		
		$user = get_user_by( 'login', $user_login );
		$blogtime = current_time( 'mysql' ); 
		update_user_meta( $user->ID, 'wbca_login_time', $blogtime );
		update_user_meta( $user->ID, 'wbca_login_status', 'online' );
	}
	public function update_login_status() {
		$blogtime = current_time( 'mysql' ); 
		$user = wp_get_current_user();
		update_user_meta( $user->ID, 'wbca_login_time', $blogtime );
		update_user_meta( $user->ID, 'wbca_login_status', 'offline' );
	}
	

}

?>