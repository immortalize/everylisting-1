<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<?php
	$value = get_post_meta($_COOKIE['secret-url-agent-id'], 'IDXGenerator_realEstateBrokerId')[0];
	$args = array('post_type' => 'property', 'meta_query' => array(
	    'relation' => 'OR',
	    'left_clause' => array(
	        'key' => 'IDXGenerator_listing_agents_id',
	        'value'   => $value . '|',
	        'compare' => 'LIKE'
	    ),
	    'center_clause' => array(
	        'key' => 'IDXGenerator_listing_agents_id',
	        'value'   => '|' . $value . '|',
	        'compare' => 'LIKE'
	    ),
	    'right_clause' => array(
	        'key' => 'IDXGenerator_listing_agents_id',
	        'value'   => '|'. $value,
	        'compare' => 'LIKE'
	    ),
	));
	$agent_properties_query = new WP_Query($args);
	if ($agent_properties_query->found_posts > 1) {
	    ?>
	    <script>
	    $('#mypropertiesbadge').text("<?php echo $agent_properties_query->found_posts;?>");
	    $('#propertiescountnotice').css("visibility", "visible");
	    $('#propertiescount').text("<?php echo $agent_properties_query->found_posts;?>");
	    </script> <?php
		$ids = '';
		while ($agent_properties_query->have_posts()) {
			$agent_properties_query->the_post();
			if ($ids != '') {
				$ids .= ',';
			}
			$ids .= get_the_ID();
		}
		wp_reset_postdata();
		$agent_properties_query->rewind_posts();
		echo do_shortcode('[idx_generator_map ids="' . $ids . '"]');
    	?><div id="homeProperties" class="property-listing-two "><?php
    		while ($agent_properties_query->have_posts()) {
    			$agent_properties_query->the_post();
    			?>
    			<div class='col-xs-12 col-sm-12 col-md-6 col-lg-4'>
    				<div class="homepropBoxes thumbnail-size">
    					<?php new_property_list_insert(get_the_ID(), 'phantom-thumbnail-class', 'phantom-description-class') ?>
    				</div>
    			</div>
    			<?php
    			wp_reset_postdata();
    		}
    	?></div><?php
	} else {
	    ?>
	    <script>
		$('#mypropertispill').css("visibility", "none");
		$('#propertiescountnotice').css("visibility", "none");
	    </script>
	    <?php
	}
    ?>
</div>