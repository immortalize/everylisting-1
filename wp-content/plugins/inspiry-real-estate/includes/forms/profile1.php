<?php
/**
 *
 */
function ire_profile_edit_form() {

	if ( is_user_logged_in() ) {

		// get user information
		$current_user      = wp_get_current_user();
		$current_user_meta = get_user_meta( $current_user->ID );


function addInspiryStyle() {
wp_register_style('inspiryStyle','/wp-content/plugins/inspiry-real-estate/public/css/style.css', '', '1.0.0');
wp_enqueue_style('inspiryStyle');
}

add_action( 'wp_print_styles', 'addInspiryStyle' );

		?>
		

	<link rel='stylesheet' id='profile-form' href='../wp-content/plugins/inspiry-real-estate/public/css/style.css' type='text/css' media='all' />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" id="qlcd-wp-chatbot-font-awe-css" href="../wp-content/plugins/inspiry-real-estate/public/css/font-awesome.min.css" type="text/css" media="screen">
		
		<form id="inspiry-edit-user" class="submit-form" enctype="multipart/form-data" method="post">
		
				<!-- new html -->
				
				<div class="container">
        <h4>Profile</h4>

        <!-- Nav pills -->
        <ul class="nav nav-pills tabNav">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#general">General</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#callcenter">Call Center</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#references">References</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#membership">Membership</a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content tabConts">
          <div class="tab-pane container active" id="general">
            <div class="genCont">
              <div class="d-flex">
                <!-- <div class="proImgCont"> -->

                  <div class="proImg">
                                      <?php
                if ( isset( $current_user_meta[ 'profile_image_id' ] ) ) {
                  $profile_image_id = intval( $current_user_meta[ 'profile_image_id' ][ 0 ] );
                  if ( $profile_image_id ) {
                    echo wp_get_attachment_image( $profile_image_id, 'inspiry-agent-thumbnail', false, array( 'class' => 'img-responsive' ) );
                    echo '<input type="hidden" class="profile-image-id" name="profile-image-id" value="' . $profile_image_id . '"/>';
                  }
                }
                ?>

<div class="wpua-edit-container">
  <h3>Avatar</h3>
  <input type="hidden" name="wp-user-avatar" id="wp-user-avatar" value="{attachmentID}" />
  <p id="wpua-add-button">
    <button type="button" class="button" id="wpua-add" name="wpua-add">Edit Image</button>
  </p>
  <p id="wpua-preview">
    <img src="{imageURL}" alt="" />
    Original Size
  </p>
  <p id="wpua-thumbnail">
    <img src="{imageURL}" alt="" />
    Thumbnail
  </p>
  <p id="wpua-remove-button">
    <button type="button" class="button" id="wpua-remove" name="wpua-remove">Default Avatar</button>
  </p>
  <p id="wpua-undo-button">
    <button type="button" class="button" id="wpua-undo" name="wpua-undo">Undo</button>
  </p>
</div>


                    <div class="row">
                      <div class="col">
                        <button id="select-profile-image" class="btn-default" href="javascript:;" class="btnSolid"><?php esc_html_e( 'Change', 'inspiry-real-estate' ); ?></button>
                      </div>
                      <div class="col">
                        <button id="remove-profile-image" class="btn-default btn-orange" href="#remove-profile-image" class="btnBorder"><?php esc_html_e( 'Remove', 'inspiry-real-estate' ); ?></button>
                      </div>
                    </div>

                    <div id="errors-log"></div>
                    <div id="plupload-container"></div>

                    <h6>Note:</h6>
                    <ul>
                      <li>Profile image should have minimum width and height of 220px.</li>
                      <li>Make sure to save changes after uploading the new image.</li>
                    </ul>
                  </div>
               <!-- </div> -->
                <div class="proDetCont">
                  <div class="fieldCont">
                    <label class="lblbold"><?php esc_html_e( 'Biographical Information', 'inspiry-real-estate' ) ?></label>
                    <textarea name="description" id="description" rows="5" cols="30"><?php if ( isset( $current_user_meta[ 'description' ] ) ) {
								echo esc_textarea( $current_user_meta[ 'description' ][ 0 ] );
							} ?></textarea>
                  </div>
                  <div class="proDetForm">
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                        <label for="first-name"><?php esc_html_e( 'First Name', 'inspiry-real-estate' ); ?></label>
						<input class="valid required" name="first-name" type="text" id="first-name"  value="<?php if ( isset( $current_user_meta[ 'first_name' ] ) ) {
							       echo esc_attr( $current_user_meta[ 'first_name' ][ 0 ] );
						       } ?>"
							   title="<?php esc_html_e( '* Provide First Name!', 'inspiry-real-estate' ); ?>"
							   autofocus/>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                        <label for="last-name"><?php esc_html_e( 'Last Name', 'inspiry-real-estate' ); ?></label>
						<input class="required" name="last-name" type="text" id="last-name"
							   value="<?php if ( isset( $current_user_meta[ 'last_name' ] ) ) {
							       echo esc_attr( $current_user_meta[ 'last_name' ][ 0 ] );
						       } ?>"
							   title="<?php esc_html_e( '* Provide Last Name!', 'inspiry-real-estate' ); ?>"/>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                        <label for="display-name"><?php esc_html_e( 'Display Name', 'inspiry-real-estate' ); ?>
							*</label>
						<input class="required" name="display-name" type="text" id="display-name"
							   value="<?php echo esc_attr( $current_user->display_name ); ?>"
							   title="<?php esc_html_e( '* Provide Display Name!', 'inspiry-real-estate' ); ?>"
							   required/>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>Email</label>
                          <input type="text"><label for="email"><?php esc_html_e( 'Email', 'inspiry-real-estate' ); ?>
							*</label>
						<input class="email required" name="email" type="email" id="email"
							   value="<?php echo esc_attr( $current_user->user_email ); ?>"
							   title="<?php esc_html_e( '* Provide Valid Email Address!', 'inspiry-real-estate' ); ?>"
							   required/>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                        <label for="mobile-number"><?php esc_html_e( 'Mobile Number', 'inspiry-real-estate' ); ?></label>
						<input class="digits" name="mobile-number" type="text" id="mobile-number"
							   value="<?php if ( isset( $current_user_meta[ 'mobile_number' ] ) ) {
							       echo esc_attr( $current_user_meta[ 'mobile_number' ][ 0 ] );
						       } ?>"
							   title="<?php esc_html_e( '* Only Digits are allowed!', 'inspiry-real-estate' ); ?>"/>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>Office Number</label>
                          <input type="text">
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>Fax Number</label>
                          <input type="text">
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>Facebook URL</label>
                          <input type="text">
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>Twitter URL</label>
                          <input type="text">
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="fieldCont">
                          <label>LinkedIn URL</label>
                          <input type="text">
                        </div>
                      </div>

                    </div>
                    <div class="d-flex">
                      <div class="ml-auto">
                        <!-- <button class="gradBtn">SUBMIT</button> -->

                        <button class="btn btn-primary btn-lg" type="submit" id="update-user" name="update-user"> <?php esc_html_e( 'Save Changes', 'inspiry-real-estate' ); ?> </button>
				<img src="<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/images/ajax-loader-2.gif" id="ajax-loader" alt="<?php esc_attr_e( 'Loading...', 'inspiry-real-estate' ); ?>">
                      </div>

                      <p id="form-message"></p>
                      <ul id="form-errors"></ul>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div class="tab-pane container fade" id="callcenter">
            <div class="callCenterCont">
              <h5>Call Center</h5>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fieldCont">
                  <label>Country</label>
                  <select>
                    <option>India</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fieldCont">
                  <label>State</label>
                  <select>
                    <option>state</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fieldCont">
                  <label>City</label>
                  <select>
                    <option>city</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fieldCont">
                  <label>Sector</label>
                  <select>
                    <option>Sector</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fieldCont">
                  <label>Shortcut</label>
                  <select>
                    <option>shortcut</option>
                  </select>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="viewBtn">
                  <button>View</button>
                </div>
              </div>
            </div>

            <div class="ccDetails">
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="fieldCont">
                    <label>Name</label>
                    <input type="text">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="fieldCont">
                    <label>Phone</label>
                    <input type="text">
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                  <div class="fieldCont">
                    <label>Email</label>
                    <input type="text">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <a class="cuslinks">Listing Agent Information</a>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8">
                  <div class="fieldCont">
                    <label>Url</label>
                    <input type="text">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <div class="fieldCont">
                    <label>Follow-up</label>
                    <select>
                      <option></option>
                    </select>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="fieldCont">
                    <label>Notes</label>
                    <textarea></textarea>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="viewBtn">
                    <button>Save</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane container fade" id="references">...</div>
          <div class="tab-pane container fade" id="membership">...</div>
        </div>
      </div>

 <script src="../wp-content/plugins/inspiry-real-estate/public/js/jquery.min.js" ></script>
  <script src="../wp-content/plugins/inspiry-real-estate/public/js/popper.js" ></script>
  <script src="../wp-content/plugins/inspiry-real-estate/public/js/bootstrap.min.js" ></script>
		<!-- end new -->

		</form>
		<!-- #inspiry-edit-user -->
		<?php
	} else {
		ire_message( esc_html__( 'Login Required', 'inspiry-real-estate' ), esc_html__( 'You need to login to edit your profile!', 'inspiry-real-estate' ) );
	}
}