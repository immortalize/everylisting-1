
<div class="option-bar form-control-buttons">
    <input type="submit"  value="<?php esc_html_e( 'Search', 'inspiry-real-estate' ); ?>" class="form-submit-btn searchbtn">
    <a class="form-submit-btn expand-adv advsearch"><i class="fa fa-plus" aria-hidden="true"></i></a>
    <div class="tooltiptext_1">Specify the features of interest</div>
</div>