<?php 
function ire_profile_edit_form()
{
	require_once('wp-load.php');
	$isnewregistration = isset($_COOKIE['secret-url-agent-id']);
?>
	<script>
	if (typeof jQuery != 'undefined') {  
		// jQuery is loaded => print the version
		console.log("jquery version: " + jQuery.fn.jquery);
	} else {
		console.log("no jquery");
	}
	</script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="X-UA-Compatible" content="ie=edge"/>

<link rel="stylesheet" href="<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/css/bootstrap4.0.min.css" crossorigin="anonymous"/>



	<style>
	/*
	  .custom-combobox {
		position: relative;
		display: inline-block;
	  }
	  .custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
	  }
	  .custom-combobox-input {
		margin: 0;
		padding: 5px 10px;
	  }
	  
	  #inspiry-edit-user{
		  margin-right: 50px!important;
		  margin-left: -50px!important;
	  }
	  #callcenter{
		  margin-left: -20px!important;
	  }
	  .fieldCont{
		  width: 90%!important;
		  max-width: 90%!important;
	  }
	  */
	</style>
	
	<script>
    <?php $is_secret_url = ((!is_user_logged_in()) && (isset($_COOKIE['secret-url-agent-id']))); ?>
	var isSecretUrl = <?php echo $is_secret_url?'true':'false'; ?>;
	var rolesJsonAtLoadTime = '<?php $post = array();
    $user = wp_get_current_user();
    foreach ($user->roles as $key => $value) {
        array_push($post, $value);
    }
    echo '{"active": ' . str_replace("-", "_", str_replace("'", '"', json_encode($post))) . '}'; ?>';
    var userRoles = JSON.parse(rolesJsonAtLoadTime);
    function pillsShowHideCycle() {
        if (!isSecretUrl && ((typeof userRoles.active['administrator'] != undefined) || (typeof userRoles.active['call_center_agent'] != undefined))) {
            $('#callcenterpill').show();
        } else {
        	$('#callcenterpill').hide();
        }
        if (!isSecretUrl && ((typeof userRoles.active['administrator'] != undefined) || (typeof userRoles.active['referee'] != undefined) || (typeof userRoles.active['referrer'] != undefined))) {
            $('#socialnetworkpill').show();
        } else {
            $('#socialnetworkpill').hide();
        }
        if (isSecretUrl) {
        	$('#mypropertispill').show();
        } else {
        	$('#mypropertispill').hide();
        }
        if ((!isSecretUrl && ((typeof userRoles.active['administrator'] != undefined) || (typeof userRoles['active.agent___active'] != undefined) || (typeof userRoles.active['agent___inactive'] != undefined))) || (isSecretUrl)) {
        	$('#membershippill').show();
        } else {
        	$('#membershippill').hide();
        }
        if ((!isSecretUrl && ((typeof userRoles.active['administrator'] != undefined) || (typeof userRoles.active['agent___active'] != undefined)))) {
        	$('#networkpropertiespill').show();
        } else {
        	$('#networkpropertiespill').hide();
        }
    }
	</script>

	<div class="pageWrapper">
		<div class="profileCont">
			<div class="container">
			<?php
		          if ((is_user_logged_in()) || $is_secret_url) {
					// get user information
					$current_user = wp_get_current_user();
					$current_user_meta = get_user_meta($current_user->ID);

					if ((!$is_secret_url && (in_array('administrator', (array) $current_user->roles) || in_array('agent_-_active', (array) $current_user->roles) || in_array('agent_-_inactive', (array) $current_user->roles) || in_array('call-center_agent', (array) $current_user->roles) || in_array('referee', (array) $current_user->roles) || in_array('referrer', (array) $current_user->roles))) || ($is_secret_url)) {
					?>
					<ul class="nav nav-pills tabNav" >
					    <!-- GENERAL TAB -->
						<li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#general" >General</a></li>  
						
    					<!-- CALL-CENTER TAB -->
    					<li class="nav-item" id="callcenterpill" style="display: none;">
    					<a class="nav-link" data-toggle="pill" href="#callcenter" id="callcenter_tab">Call Center</a></li>   
    					
    					<!-- SOCIAL-NETWORK TAB -->
    					<li class="nav-item" id="socialnetworkpill" style="display: none;">
    					<a class="nav-link" data-toggle="pill" href="#references">Social-Networks</a></li>
    
    					<!-- MY PROPERTIES TAB -->
    					<li id="mypropertispill" class="nav-item" style="display: none;"><a class="nav-link" data-toggle="pill" href="#properties">My Properties&nbsp;<span id="mypropertiesbadge" class="badge badge-pill badge-success"></span></a></li> 
    
    					<!-- MEMBERSHIP TAB -->
    					<li class="nav-item" id="membershippill" style="display: none;"><a class="nav-link" data-toggle="pill" href="#membership">Membership<?php echo (($isnewregistration)?' (optional)':''); ?> </a></li>   
    
    					<!-- PROMOTABLE PROPERTIES TAB -->
    					<li class="nav-item" id="networkpropertiespill" style="display: none;"><a class="nav-link" data-toggle="pill" href="#networkconnect" onclick="ensureMyPropertiesLoaded();">Promotable Properties</a></li>  
					</ul>					
					<?php   
					}
					?>
					<!-- Tab panes -->
					<div class="tab-content tabConts" style="border-left:1px solid #778899; border-right:1px solid #778899; border-bottom:1px solid #778899; border-top:0.5px solid #778899">
						<!-- general tab content ======================================================-->
						<div class="tab-pane container active" id="general">
							<?php require_once 'generaltab.php'; ?>
						</div>  
						<!-- close: general tab content ======================================================-->
						<!-- call-center tab content ======================================================-->
						<div class="tab-pane container fade" id="callcenter">
							<?php require_once 'callcentertab.php'; ?>
						</div>
						<!-- close: call-center tab content ======================================================-->
						<!-- social-network tab content ======================================================-->
						<div class="tab-pane container fade" id="references">
							<?php require_once 'socialnetworktab.php'; ?>    
						</div>
						<!-- close: social-network tab content ======================================================-->
						<!-- membership tab content ======================================================-->
						<div class="tab-pane container fade" id="membership">
							<?php require_once 'membershiptab.php'; ?>    
						</div>
						<!-- close: membership tab content ======================================================-->
						<!-- My properties tab content ======================================================-->
						<div class="tab-pane container fade" id="properties">
							<?php require_once 'mypropertiestab.php'; ?>    
						</div>
						<!-- close: My properties tab content ======================================================-->
						<!-- My properties tab content ======================================================-->
						<?php if (!$is_secret_url) { ?>
						<div class="tab-pane container fade" id="networkconnect">
							<?php require_once 'mynetworkpropertiestab.php'; ?>    
						</div>
						<?php } ?>
						<!-- close: My properties tab content ======================================================-->
						<!-- End Tab Pan -->																														
					</div>
				<?php
				} else {
					ire_message(esc_html__('Login Required', 'inspiry-real-estate'), esc_html__('You need to login to edit your profile!', 'inspiry-real-estate'));

					do_action('wordpress_social_login', array(
						'login'
					));
				}
				?>
			</div>
		</div>
	</div>


	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type='text/javascript'
		src='<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/js/popper.js'></script>
	<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script>
	<script type='text/javascript'
		src='<?php echo esc_url( IRE_PLUGIN_URL ); ?>public/js/profile.js'></script>

	<script>pillsShowHideCycle();</script>


	<?php
}
?>
