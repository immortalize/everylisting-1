<?php
if (isset($_COOKIE['secret-url-agent-id'])) {
    $postmeta = get_post_meta($_COOKIE['secret-url-agent-id']);
} else {
    unset($postmeta);
}
$emptyProfileImage = '$("<img/>", {"src": "/mediacode/images/Generic-Avatar.jpg", "style": "width: 220px; height: 220px;"})';
$issecreturl = isset($_COOKIE['secret-url-agent-id']);
$currentuser = (get_current_user_id() != 0)?wp_get_current_user():null;
if ($currentuser == null) {
    unset($currentuser);
}
// echo "currentuser: " . (isset($currentuser))?print_r($currentuser, true):"not set";
$isnewregistration = !isset($currentuser);
?>
<style>
#avatar_upload #submit, #avatar_upload button {
	width: unset;
	padding: 5px 12px;
	border: none;
	background-color: #434343;
}
#avatar_upload #submit {
    float: right;
    background-color: #f44336;
}
</style>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div id="advanceduseroptions" class="card" style="display: none; margin-bottom: 12px;">
    	<div class="card-header">
        	<strong>Advanced User Options&nbsp;&nbsp;<span id="advanceduseroptionsspinner" style="display: none;"><i class="fa fa-spinner fa-spin fa-lg"></i></span></strong>
        	<a class="pull-right" onclick="$('#advanceduseroptions').slideUp(); $('#advanceduseroptionsbutton').show();" style="cursor: pointer;"><i class="fa fa-window-close"></i></a>
      	</div>
      	<div class="card-body" style="padding: 20px;">
            <?php require_once 'userrolestab.php'; ?>    
      	</div>
    </div>
</div>
<form id="inspiry-edit-user" class="submit-form" onsubmit="return submitForm(this);" method="post">
	<div class="container-fluid">
		<a id="topalertpos" name="topalertpos"/>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
    		<div id="topalertdiv">
        		<div id="topalert" class="alert alert-dismissible alert-primary" style="margin: 20px; display: none;" role="alert">
        			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
              		</button>
        		</div>
    		</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<button id="advanceduseroptionsbutton" class="btn btn-secondary pull-right" style="display: <?php echo $isnewregistration?'none':'visible'; ?>" onclick="$('#advanceduseroptions').slideDown(); $(this).hide(); ensureRolesSettingsLoaded(); return false;">Advanced User Options...</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<div class="form-option user-profile-img-wrapper proImgCont clearfix">
				<div id="user-profile-img">
					<?php if (!$isnewregistration) {?>
					<div class="card">
						<div class="card-body">
							<div id="avatar_upload" class="fieldCont">
                        		<?php echo do_shortcode("[avatar_upload]"); ?>
							</div>
						</div>
					</div>
					<?php } else { ?>
					<div class="form-group">
						<figure>
                            <?php if (isset($postmeta['CallCenter_prefill_profile_image_url'][0])) { ?>
                            	<img src="<?php echo $postmeta['CallCenter_prefill_profile_image_url'][0];?>" style="width: 220px; height: 220px;"/>
                            <?php } else { ?>
                            	<img src="/mediacode/images/Generic-Avatar.jpg" style="width: 220px; height: 220px;"/>
                            <?php } ?>
                        </figure>
						<div class="profile-img-controls proImgCont">
							<ul class="field-description list-unstyled">
								<li><span>*</span><?php esc_html_e( 'Profile image should have minimum width and height of 220px.', 'inspiry-real-estate' ); ?></li>
								<li><span>*</span><?php esc_html_e( 'Make sure to save changes after uploading the new image.', 'inspiry-real-estate' ); ?></li>
							</ul>
							<div id="errors-log"></div>
							<div id="plupload-container"></div>
						</div>
						<button type="button" class="btn btn-primary"
							id="btn-upload-image">Upload Image</button>
						<button type="button" class="btn btn-secondary"
							id="btn-remove-image"
							onclick="imageId = undefined; imageUrl = undefined; setImageToAvatar();">Remove
							Image</button>
						<input type="hidden" name="image" id="input-image" /> <input
							type="hidden" name="image_id" id="input-image-id" />
					</div>
					<?php }?>
					<script>
                    $('#btn-upload-image').on('click', function(e) {
                        e.preventDefault();
                        var button = $(this);
                        var figure = button.siblings("figure");
                        var custom_uploader = wp.media({
                                title: 'Insert image',
                                library: { type : 'image' },
                                button: { text: 'Use this image' },
                                id: 'library-' + (Math.random() * 10),
                                multiple: false
                            }).on('select', function() {
                                var attachment = custom_uploader.state().get('selection').first().toJSON();
                                figure.html($('<img/>', {'src': attachment.url, 'style': 'width: 220px; height: 220px;'}));
                                imageId = attachment.id;
                                imageUrl = attachment.url;
                        }).open();
                    });
                    </script>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<label class="lblbold" for="prefill-bio-text">Biographical Information</label>
			<textarea id="prefill-bio-text" class="form-control" rows="6"><?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_bio'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_bio'][0]); } else if (isset($currentuser)) { echo $currentuser->description; } ?></textarea>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="proDetForm">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-first-name"><?php esc_html_e( 'First Name', 'inspiry-real-estate' ); ?></label>
							<input class="valid required" name="prefill-first-name"
								type="text" id="prefill-first-name"
								onkeyup="recalculateDisplayName();"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_first_name'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_first_name'][0]); } else if (isset($currentuser)) { echo $currentuser->user_firstname; } ?>"
								title="<?php esc_html_e( '* Provide First Name!', 'inspiry-real-estate' ); ?>"
								autofocus />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-last-name"><?php esc_html_e( 'Last Name', 'inspiry-real-estate' ); ?></label>
							<input class="required" name="prefill-last-name" type="text"
								id="prefill-last-name"
								onkeyup="recalculateDisplayName();"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_last_name']))) { echo esc_attr($postmeta['CallCenter_prefill_profile_last_name'][0]); } else if (isset($currentuser)) { echo $currentuser->user_lastname; } ?>"
								title="<?php esc_html_e( '* Provide Last Name!', 'inspiry-real-estate' ); ?>" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-user-name"><?php esc_html_e( 'User Name', 'inspiry-real-estate' ); ?>
      							*</label> <input class="required" name="prefill-user-name"
								type="text" id="prefill-user-name"
								<?php echo (!$isnewregistration)?"disabled":""; ?>
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_user_name']))) { echo esc_attr($postmeta['CallCenter_prefill_profile_user_name'][0]); } else if (isset($currentuser)) { echo $currentuser->user_login; } ?>"
								title="<?php esc_html_e( '* Provide User Name!', 'inspiry-real-estate' ); ?>"
								required />
							<div>
								<span id="loading"> </span><span id="usernamemsg"></span>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-display-name"><?php esc_html_e( 'Display Name', 'inspiry-real-estate' ); ?>
      							*</label> <input class="required"
								name="prefill-display-name" type="text"
								id="prefill-display-name"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_display_name']))) { echo esc_attr($postmeta['CallCenter_prefill_profile_display_name'][0]); } else if (isset($currentuser)) { echo $currentuser->display_name; } ?>"
								title="<?php esc_html_e( '* Provide Display Name!', 'inspiry-real-estate' ); ?>"
								required />
						</div>
					</div>
					<?php if ($isnewregistration) {?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 well">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="fieldCont">
								<label for="password">Password *</label> <input class="required" name="password"
									type="password" id="password"
									title="<?php esc_html_e( '* Provide a Password!', 'inspiry-real-estate' ); ?>"
									required />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="fieldCont">
								<label for="password2">Repeat Password*</label> <input class="required" name="password2"
									type="password2" id="password2"
									title="<?php esc_html_e( '* Provide a Password!', 'inspiry-real-estate' ); ?>"
									required />
							</div>
						</div>
					</div>
					<?php }?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-email"><?php esc_html_e( 'Email', 'inspiry-real-estate' ); ?>
      							*</label> <input class="email required"
      							onkeyup="mirrorvals($('#prefill-email')[0].val(), '#agent_Email');"
								name="prefill-email" type="email" id="prefill-email"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_email']))) { echo esc_attr($postmeta['CallCenter_prefill_profile_email'][0]); } else if (isset($currentuser)) { echo $currentuser->user_email; } ?>"
								title="<?php esc_html_e( '* Provide Valid Email Address!', 'inspiry-real-estate' ); ?>"
								required />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-mobile-number"><?php esc_html_e( 'Mobile Number', 'inspiry-real-estate' ); ?></label>
							<input class="digits" name="prefill-mobile-number" type="text"
								id="prefill-mobile-number"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_phone'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_phone'][0]); } else if (isset($currentuser)) { echo $currentuser->mobile_number; } ?>"
								title="<?php esc_html_e( '* Only Digits are allowed!', 'inspiry-real-estate' ); ?>" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-office-number"><?php esc_html_e( 'Office Number', 'inspiry-real-estate' ); ?></label>
							<input class="digits" name="prefill-office-number" type="text"
								id="prefill-office-number"
								value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_office_number'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_office_number'][0]); } else if (isset($currentuser)) { echo $currentuser->office_number; } ?>"
								title="<?php esc_html_e( '* Only Digits are allowed!', 'inspiry-real-estate' ); ?>" />
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-facebook-url"><?php esc_html_e( 'Facebook URL', 'inspiry-real-estate' ); ?></label>
							<div class="smField">
								<input class="url" name="prefill-facebook-url" type="text"
									id="prefill-facebook-url"
									value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_facebook_url'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_facebook_url'][0]); } else if (isset($currentuser)) { echo $currentuser->facebook_url; } ?>"
									title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
								<a class="smBtn fbbtn"><img
									src="/wp-content/plugins/inspiry-real-estate/public/images/fb.svg"
									width="10" alt="Facebook" /></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-twitter-url"><?php esc_html_e( 'Twitter URL', 'inspiry-real-estate' ); ?></label>
							<div class="smField">
								<input class="url" name="prefill-twitter-url" type="text"
									id="prefill-twitter-url"
									value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_twitter_url'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_twitter_url'][0]); } else if (isset($currentuser)) { echo $currentuser->twitter_url; } ?>"
									title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
								<a class="smBtn twbtn"><img
									src="/wp-content/plugins/inspiry-real-estate/public/images/twitter.svg"
									width="22" alt="Twitter" /></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-linkedin-url"><?php esc_html_e( 'LinkedIn URL', 'inspiry-real-estate' ); ?></label>
							<div class="smField">
								<input class="url" name="prefill-linkedin-url" type="text"
									id="prefill-linkedin-url"
									value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_linkedin_url'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_linkedin_url'][0]); } else if (isset($currentuser)) { echo $currentuser->linkedin_url; } ?>"
									title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
								<a class="smBtn lnbtn"><img
									src="/wp-content/plugins/inspiry-real-estate/public/images/linkedin.svg"
									width="20" alt="LinkedIn" /></a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="fieldCont">
							<label for="prefill-instagram-url"><?php esc_html_e( 'Instagram URL', 'inspiry-real-estate' ); ?></label>
							<div class="smField">
								<input class="url" name="prefill-instagram-url" type="text"
									id="prefill-instagram-url"
									value="<?php if (($isnewregistration) && (isset($postmeta['CallCenter_prefill_profile_instagram_url'][0]))) { echo esc_attr($postmeta['CallCenter_prefill_profile_instagram_url'][0]); } else if (isset($currentuser)) { echo $currentuser->instagram_url; } ?>"
									title="<?php esc_html_e( '* Provide Valid URL!', 'inspiry-real-estate' ); ?>" />
								<a class="smBtn instagrambtn"><img
									src="/wp-content/plugins/inspiry-real-estate/public/images/instagram.svg"
									width="22" alt="Instagram" /></a>
							</div>
						</div>
					</div>
				</div>
				<div class="d-flex">
					<div class="form-option">
						<?php if ($issecreturl || $isnewregistration) { ?>
                        <input type="hidden" name="action" value="new_user"/>
                        <?php } else { ?>
                        <input type="hidden" name="action" value="inspiry_update_profile"/>
                        <?php } 
                        wp_nonce_field('update_user', 'user_profile_nonce');
                        ?>
					</div>
					<div class="ml-auto">
						<?php if ($issecreturl) { ?>
						<div class="alert alert-primary" style="margin: 20px;">
							<p>Registering will allow you more functionality <strong>free</strong> of charge and will ensure your intended communications originating from EveryListing.com get to you!</p>
						</div>
						<?php } ?>
						<div class="form-option">
							<input type="submit" id="update-user" name="update-user" class="btn btn-primary btn-lg" value="<?php esc_html_e(($isnewregistration || $issecreturl)?'REGISTER':'SUBMIT', 'inspiry-real-estate'); ?>">
							<img src="../wp-content/plugins/inspiry-real-estate/public/images/ajax-loader-2.gif" id="ajax-loader" alt="<?php esc_attr_e( 'Loading...', 'inspiry-real-estate' ); ?>">
						</div>
						<p id="form-message"></p>
						<ul id="form-errors"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
	function submitForm(oFormElement) {
		var closebtnhtml = '<div id="topalert" class="alert alert-dismissible alert-primary" style="margin: 20px; display: none;">__MESSAGE__<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		if ($('#topalert').length == 0) {
			$('#topalertdiv').html(closebtnhtml.replaceAll('__MESSAGE__', ''));
		}
		$('#topalert').hide();
    	var locHttp = new XMLHttpRequest();
    	locHttp.open("POST", "/wp-content/plugins/inspiry-real-estate/includes/forms/registrationsave.php", true);
    	// locHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    	locHttp.send(new FormData(oFormElement));
    	locHttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
        		if (this.responseText === 'OK') {
        			$('#topalertdiv').html(closebtnhtml.replaceAll('__MESSAGE__', "The information was saved."));
        			$('#topalert').show();
        			$('#topalert').addClass("alert-success").removeClass("alert-primary").removeClass("alert-danger");
        		} else {
        			$('#topalertdiv').html(closebtnhtml.replaceAll('__MESSAGE__', (this.responseText.startsWith("ERROR:"))?(this.responseText.substring("ERROR:".length).trim()):this.responseText));
        			$('#topalert').show();
        			$('#topalert').removeClass("alert-success").removeClass("alert-primary").addClass("alert-danger");
        		}
        		$(document).scrollTop($("#topalertpos").offset().top - 160);  
    			<?php if ($issecreturl) { ?>
            	eraseCookie("secret-url-agent-id");
            	var loginval = encodeURIComponent(getCookie('emailreg'));
            	<?php unset($_COOKIE['emailreg']); 	?>
            	console.log("<?php echo site_url('', 'http'); ?>?login=" + loginval);
            	window.location.href = "<?php echo site_url('', 'http'); ?>?login=" + loginval;
            <?php } ?>
    		}
    	}
    	return false;
	}

	/* cookie stuff */
	function setCookie(name,value,days) {
	  if (days) {
		var date = new Date(); 
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	  }
	  else var expires = "";
	  document.cookie = name+"="+value+expires+"; path=/";
	} 
	
	function getCookie(name) {
	  var nameEQ = name + "=";
	  
	  var ca = document.cookie.split(";");
	  for(var i=0;i < ca.length;i++) {
		var c = ca[i]; 
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	  }
	  return null;
	}
	
	function eraseCookie(name) {setCookie(name,"",-1);}
    
	function mirrorvals(val1, elem2) {
		console.log(val1);
		if ($(elem2).length > 0) {
			$(elem2).val(val1);
		}
	}
	
	function recalculateDisplayName() {
		var first = $('#prefill-first-name').val();
		var last = $('#prefill-last-name').val();
		$('#prefill-display-name').val((first + " " + last).trim());
		if ($('#prefill-display-name').val() == "") {
			$('#prefill-display-name').val($('#prefill-user-name').val().trim());
		}
		while ($('#prefill-display-name').val().indexOf("  ") >= 0) {
			$('#prefill-display-name').val($('#prefill-display-name').val().replace("  ", " "));
		}
	}
	recalculateDisplayName();
    var imageId = undefined;
    var imageUrl = undefined;
    function setImageToAvatar() {
    	var figure = $('#btn-upload-image').siblings("figure");
    	figure.html(<?php echo $emptyProfileImage;?>);
    }
    <?php if (!isset($_COOKIE['secret-url-agent-id'])) { ?>
    	function populatePrefillData(from) {
        	if (from != undefined) {
        		if (from["prefill_profile_bio"] != undefined) {
        			$("#prefill-bio-text").val(from["prefill_profile_bio"]);
        		} else {
        			$("#prefill-bio-text").val("");
        		}
        		if (from["prefill_profile_first_name"] != undefined) {
        			$("#prefill-first-name").val(from["prefill_profile_first_name"]);
        		} else {
        			$("#prefill-first-name").val("");
        		}
        		if (from["prefill_profile_last_name"] != undefined) {
        			$("#prefill-last-name").val(from["prefill_profile_last_name"]);
        		} else {
        			$("#prefill-last-name").val("");
        		}
        		if (from["prefill_profile_user_name"] != undefined) {
        			$("#prefill-user-name").val(from["prefill_profile_user_name"]);
        		} else {
        			$("#prefill-user-name").val("");
        		}
        		if (from["prefill_profile_display_name"] != undefined) {
        			$("#prefill-display-name").val(from["prefill_profile_display_name"]);
        		} else {
        			$("#prefill-display-name").val("");
        		}
        		if (from["prefill_profile_office_number"] != undefined) {
        			$("#prefill-office-number").val(from["prefill_profile_office_number"]);
        		} else {
        			$("#prefill-office-number").val("");
        		}
        		if (from["prefill_profile_facebook_url"] != undefined) {
        			$("#prefill-facebook-url").val(from["prefill_profile_facebook_url"]);
        		} else {
        			$("#prefill-facebook-url").val("");
        		}
        		if (from["prefill_profile_instagram_url"] != undefined) {
        			$("#prefill-instagram-url").val(from["prefill_profile_instagram_url"]);
        		} else {
        			$("#prefill-instagram-url").val("");
        		}
        		if (from["prefill_profile_linkedin_url"] != undefined) {
        			$("#prefill-linkedin-url").val(from["prefill_profile_linkedin_url"]);
        		} else {
        			$("#prefill-linkedin-url").val("");
        		}
        		if (from["prefill_profile_twitter_url"] != undefined) {
        			$("#prefill-twitter-url").val(from["prefill_profile_twitter_url"]);
        		} else {
        			$("#prefill-twitter-url").val("");
        		}
        		if (from["prefill_profile_phone"] != undefined) {
        			$("#prefill-mobile-number").val(from["prefill_profile_phone"]);
        		} else {
        			$("#prefill-mobile-number").val("");
        		}
        		if (from["prefill_profile_email"] != undefined) {
        			$("#prefill-email").val(from["prefill_profile_email"]);
        		} else {
        			$("#prefill-email").val("");
        		}
        		imageId = from["prefill_profile_image_id"];
        		imageUrl = from["prefill_profile_image_url"];
        		var figure = $('#btn-upload-image').siblings("figure");
        		if (imageUrl != undefined) {
        			figure.html($('<img/>', {'src': imageUrl, 'style': 'width: 220px; height: 220px;'}));
        		} else {
        			setImageToAvatar();
        		}
        	}
    	}
    
    	function acquirePrefillData(into) {
    		into["prefill_profile_bio"] = $("#prefill-bio-text").val();
    		into["prefill_profile_first_name"] = $("#prefill-first-name").val();
    		into["prefill_profile_last_name"] = $("#prefill-last-name").val();
    		into["prefill_profile_user_name"] = $("#prefill-user-name").val();
    		into["prefill_profile_display_name"] = $("#prefill-display-name").val();
    		into["prefill_profile_office_number"] = $("#prefill-office-number").val();
    		into["prefill_profile_facebook_url"] = $("#prefill-facebook-url").val();
    		into["prefill_profile_instagram_url"] = $("#prefill-instagram-url").val();
    		into["prefill_profile_linkedin_url"] = $("#prefill-linkedin-url").val();
    		into["prefill_profile_twitter_url"] = $("#prefill-twitter-url").val();
    		into["prefill_profile_phone"] = $("#prefill-mobile-number").val();
    		into["prefill_profile_email"] = $("#prefill-email").val();
    		into["prefill_profile_image_id"] = imageId;
    		into["prefill_profile_image_url"] = imageUrl;
    	}
<?php } ?>
</script>
