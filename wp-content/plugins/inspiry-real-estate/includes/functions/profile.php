<?php
/**
 *  Profile image upload handler
 */

if ( ! function_exists( 'inspiry_admin_styles' ) ) :
function ire_profile_image_upload( ) {

	// Verify Nonce
	$nonce = $_REQUEST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'inspiry_allow_upload' ) ) {
		$ajax_response = array(
			'success' => false ,
			'reason' => esc_html__( 'Security check failed!', 'inspiry-real-estate' )
		);
		echo json_encode( $ajax_response );
		//die;
	}

	$submitted_file = $_FILES['inspiry_upload_file'];
	$uploaded_image = wp_handle_upload( $submitted_file, array( 'test_form' => false ) );   //Handle PHP uploads in WordPress, sanitizing file names, checking extensions for mime type, and moving the file to the appropriate directory within the uploads directory.

	if ( isset( $uploaded_image['file'] ) ) {
		$file_name          =   basename( $submitted_file['name'] );
		$file_type          =   wp_check_filetype( $uploaded_image['file'] );   //Retrieve the file type from the file name.

		// Prepare an array of post data for the attachment.
		$attachment_details = array(
			'guid'           => $uploaded_image['url'],
			'post_mime_type' => $file_type['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		$attach_id      =   wp_insert_attachment( $attachment_details, $uploaded_image['file'] );       // This function inserts an attachment into the media library
		$attach_data    =   wp_generate_attachment_metadata( $attach_id, $uploaded_image['file'] );     // This function generates metadata for an image attachment. It also creates a thumbnail and other intermediate sizes of the image attachment based on the sizes defined
		wp_update_attachment_metadata( $attach_id, $attach_data );                                      // Update metadata for an attachment.

		$thumbnail_url = ire_get_profile_image_url( $attach_data ); // returns escaped url

		$ajax_response = array(
			'success'   => true,
			'url' => $thumbnail_url,
			'attachment_id'    => $attach_id
		);
		echo json_encode( $ajax_response );
		die;

	} else {
		$ajax_response = array(
			'success' => false,
			'reason' => esc_html__( 'Image upload failed!', 'inspiry-real-estate' )
		);
		echo json_encode( $ajax_response );
		die;
	}

}
add_action( 'wp_ajax_profile_image_upload', 'ire_profile_image_upload' );
endif;

/**
 * Get thumbnail url based on attachment data
 * @param $attach_data
 * @return string
 */
function ire_get_profile_image_url( $attach_data ) {
	$upload_dir         =   wp_upload_dir();
	$image_path_array   =   explode( '/', $attach_data['file'] );
	$image_path_array   =   array_slice( $image_path_array, 0, count( $image_path_array ) - 1 );
	$image_path         =   implode( '/', $image_path_array );
	$thumbnail_name     =   null;
	if ( isset( $attach_data['sizes']['inspiry-agent-thumbnail'] ) ) {
		$thumbnail_name     =   $attach_data['sizes']['inspiry-agent-thumbnail']['file'];
	} else {
		$thumbnail_name     =   $attach_data['sizes']['thumbnail']['file'];
	}

	return esc_url( $upload_dir['baseurl'] . '/' . $image_path . '/' . $thumbnail_name );
}


/**
 * AJAX register request handler
 */
function ire_register() {

	
			$info                  = array();
			$info['user_nicename'] = sanitize_user( $_POST['username'] );
			$info['user_pass']     = wp_generate_password( 12 );
			$info['user_email']    = sanitize_email( $_POST['register_email'] );

			// Register the user
			$user_register = wp_insert_user( $info );
 
 			/*
			$user_id       = wp_create_user( $user_name, $user_password, $user_email );
			update_user_option( $user_id, 'default_password_nag', true, true );
			$email_password = true;
			$user_created   = true;
			*/



	if ( is_wp_error( $user_register ) ) {

		$error = $user_register->get_error_codes();
		if ( in_array( 'empty_user_login', $error ) ) {
			echo json_encode( array(
				'success' => false,
				'message' => esc_html__( $user_register->get_error_message( 'empty_user_login' ) )
			) );
		} elseif ( in_array( 'existing_user_login', $error ) ) {
			echo json_encode( array(
				'success' => false,
				'message' => esc_html__( 'This username already exists.', 'inspiry-real-estate' )
			) );
		} elseif ( in_array( 'existing_user_email', $error ) ) {
			echo json_encode( array(
				'success' => false,
				'message' => esc_html__( 'This email is already registered.', 'inspiry-real-estate' )
			) );
		}

	} else {

		/* send password as part of email to newly registered user */
		ire_new_user_notification( $user_register, $info['user_pass'] );

		return $user_register;

		/*
		echo json_encode( array(
			'success' => true,
			'message' => esc_html__( 'Registration is complete. Check your email for details!', 'inspiry-real-estate' ),
		) ); */
	}

	//die();
}

function ire_synch_user_and_agent($create) {
    // if ($create) delete_user_meta(get_current_user_id(), 'agent_id'); // TEMPORARY
    $user_meta = get_user_meta(get_current_user_id());
    if (((!isset($user_meta['agent_id'])) || ($user_meta['agent_id'] <= 0) || ($user_meta['agent_id'] == false)) && ($create)) {
        delete_user_meta(get_current_user_id(), 'agent_id');
        $array = array();
        $array['post_title'] = trim($user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0]);
        $array['post_content'] = 'Not customer facing';
        $array['post_type'] = "agent";
        $array['post_status'] = "publish";
        $id = wp_insert_post($array);
        if (($id > 0) && ($id != false)) {
            add_user_meta(get_current_user_id(), 'agent_id', $id, true);
        }
        $user_meta = get_user_meta(get_current_user_id());
    }
    if ((isset($user_meta['agent_id'])) && ($user_meta['agent_id'][0] > 0)) {
        $postid = $user_meta['agent_id'][0];
//         if (FALSE === get_post_status($postid)) {
//             echo 'post does not exist\n';
//         } else {
//             echo 'post exists\n';
//         }
        $name = wp_get_current_user()->display_name;
        if ($name == '') {
            $name = trim($user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0]);
        }
        if ($name == '') {
            wp_get_current_user()->user_login;
        }
        if ($name == '') {
            wp_get_current_user()->user_email;
        }
        wp_update_post(array('ID' => $postid, 'post_title' => $name));
        delete_post_meta($postid, 'job_title'); add_post_meta($postid, 'job_title', $name, true);
        delete_post_meta($postid, 'inspiry_job_title'); add_post_meta($postid, 'inspiry_job_title', $name, true);
        delete_post_meta($postid, 'IDXGenerator_name'); add_post_meta($postid, 'IDXGenerator_name', $name, true);
        delete_post_meta($postid, 'mobile'); add_post_meta($postid, 'mobile', $user_meta['mobile_number'][0], true);
        delete_post_meta($postid, 'REAL_HOMES_mobile_number'); add_post_meta($postid, 'REAL_HOMES_mobile_number', $user_meta['mobile_number'][0], true);
        delete_post_meta($postid, 'IDXGenerator_phone'); add_post_meta($postid, 'IDXGenerator_phone', $user_meta['mobile_number'][0], true);
        delete_post_meta($postid, 'office'); add_post_meta($postid, 'office', $user_meta['office_number'][0], true);
        delete_post_meta($postid, 'email'); add_post_meta($postid, 'email', $user_meta['user_email'][0], true);
        delete_post_meta($postid, 'REAL_HOMES_agent_email'); add_post_meta($postid, 'REAL_HOMES_agent_email', $user_meta['user_email'][0], true);
        delete_post_meta($postid, 'IDXGenerator_email'); add_post_meta($postid, 'IDXGenerator_email', $user_meta['user_email'][0], true);
        delete_post_meta($postid, 'facebook'); add_post_meta($postid, 'facebook', $user_meta['facebook_url'][0], true);
        delete_post_meta($postid, 'twitter'); add_post_meta($postid, 'twitter', $user_meta['twitter_url'][0], true);
        delete_post_meta($postid, 'linkedin'); add_post_meta($postid, 'linkedin', $user_meta['linkedin_url'][0], true);
        delete_post_meta($postid, 'instagram'); add_post_meta($postid, 'instagram', $user_meta['instagram_url'][0], true);
        delete_post_meta($postid, 'user_id'); add_post_meta($postid, 'user_id', get_current_user_id(), true);
//         echo 'post id is : ' . $postid . '\n';
//         print_r(get_post_custom($postid));
    } else {
//         echo 'did not enter as expected: ';
//         print_r($user_meta);
    }
    return ((isset($user_meta['agent_id']))?($user_meta['agent_id'][0]):false);
}


/**
 * Edit profile request handler
 */
function ire_update_profile() {
    
	if (!wp_get_current_user() && $_POST['new_user']){

		$current_user = new stdClass();
   		$current_user->ID = ire_register();

		//$user = new WP_User( $user_id );
		$user = new WP_User( $current_user->ID );
		$user->set_role( 'agent_-_inactive' ); 

		//$user_id        = username_exists( $user_name );
		/*
		$user_password  = trim( $user_password );
		$email_password = false;
		$user_created   = false;
		if ( ! $user_id && empty( $user_password ) ) {
			$user_password = wp_generate_password( 12, false );
			$message       = __( '<strong><em>Note that password</em></strong> carefully! It is a <em>random</em> password that was generated just for you.' );
			$user_id       = wp_create_user( $user_name, $user_password, $user_email );
			update_user_option( $user_id, 'default_password_nag', true, true );
			$email_password = true;
			$user_created   = true;
		} elseif ( ! $user_id ) {
			// Password has been provided.
			$message      = '<em>' . __( 'Your chosen password.' ) . '</em>';
			$user_id      = wp_create_user( $user_name, $user_password, $user_email );
			$user_created = true;
		} else {
			$message = __( 'User already exists. Password inherited.' );
		}

		$user = new WP_User( $user_id );
		$user->set_role( 'agent_-_inactive' ); // set agent
		$current_user = wp_get_current_user();
		*/

		//$current_user = new stdClass();
   		//$current_user->ID = 8;

	} else {

    	// Get user info
    	$current_user = wp_get_current_user();
    
    	if ((isset($_COOKIE['secret-url-agent-id'])) && (isset(get_post_meta($_COOKIE['secret-url-agent-id'], 'IDXGenerator_realEstateBrokerId')[0]))) {
    	    $agentid = ire_synch_user_and_agent(true);
    	    update_post_meta($agentid, 'IDXGenerator_realEstateBrokerId', get_post_meta($_COOKIE['secret-url-agent-id'], 'IDXGenerator_realEstateBrokerId')[0]);
    	} else {
    	   ire_synch_user_and_agent(false);
    	}
	
	}

	// Array for errors
	$errors = array();

	if( wp_verify_nonce( $_POST['user_profile_nonce'], 'update_user' ) ) {

		// profile-image-id
		// Update profile image
		if ( !empty( $_POST['profile-image-id'] ) ) {
			$profile_image_id = intval( $_POST['profile-image-id'] );
			update_user_meta( $current_user->ID, 'profile_image_id', $profile_image_id );
		} else {
			delete_user_meta( $current_user->ID, 'profile_image_id' );
		}

		// Update first name
		if ( !empty( $_POST['first-name'] ) ) {
			$user_first_name = sanitize_text_field( $_POST['first-name'] );
			update_user_meta( $current_user->ID, 'first_name', $user_first_name );
		} else {
			delete_user_meta( $current_user->ID, 'first_name' );
		}

		// Update last name
		if ( !empty( $_POST['last-name'] ) ) {
			$user_last_name = sanitize_text_field( $_POST['last-name'] );
			update_user_meta( $current_user->ID, 'last_name', $user_last_name );
		} else {
			delete_user_meta( $current_user->ID, 'last_name' );
		}

		// Update display name
		if ( !empty( $_POST['display-name'] ) ) {
			$user_display_name = sanitize_text_field( $_POST['display-name'] );
			$return = wp_update_user( array(
				'ID' => $current_user->ID,
				'display_name' => $user_display_name
			) );
			if ( is_wp_error( $return ) ) {
				$errors[] = $return->get_error_message();
			}
		}

		// Update user email
		if ( !empty( $_POST['email'] ) ){
			$user_email = is_email( sanitize_email ( $_POST['email'] ) );
			if ( !$user_email )
				$errors[] = esc_html__( 'Provided email address is invalid.', 'inspiry-real-estate' );
			else {
				$email_exists = email_exists( $user_email );    // email_exists returns a user id if a user exists against it
				if( $email_exists ) {
					if( $email_exists != $current_user->ID ){
						$errors[] = esc_html__('Provided email is already in use by another user. Try a different one.', 'inspiry-real-estate');
					} else {
						// no need to update the email as it is already current user's
					}
				} else {
					$return = wp_update_user( array ('ID' => $current_user->ID, 'user_email' => $user_email ) );
					if ( is_wp_error( $return ) ) {
						$errors[] = $return->get_error_message();
					}
				}
			}
		}

		// update user description
		if ( !empty( $_POST['description'] ) ) {
			$user_description = sanitize_text_field( $_POST['description'] );
			update_user_meta( $current_user->ID, 'description', $user_description );
		} else {
			delete_user_meta( $current_user->ID, 'description' );
		}

		// Update mobile number
		if ( !empty( $_POST['mobile-number'] ) ) {
			$user_mobile_number = sanitize_text_field( $_POST['mobile-number'] );
			update_user_meta( $current_user->ID, 'mobile_number', $user_mobile_number );
		} else {
			delete_user_meta( $current_user->ID, 'mobile_number' );
		}

		// Update office number
		if ( !empty( $_POST['office-number'] ) ) {
			$user_office_number = sanitize_text_field( $_POST['office-number'] );
			update_user_meta( $current_user->ID, 'office_number', $user_office_number );
		} else {
			delete_user_meta( $current_user->ID, 'office_number' );
		}

		// Update fax number
		if ( !empty( $_POST['fax-number'] ) ) {
			$user_fax_number = sanitize_text_field( $_POST['fax-number'] );
			update_user_meta( $current_user->ID, 'fax_number', $user_fax_number );
		} else {
			delete_user_meta( $current_user->ID, 'fax_number' );
		}

		// Update facebook url
		if ( !empty( $_POST['facebook-url'] ) ) {
			$facebook_url = esc_url_raw( sanitize_text_field( $_POST['facebook-url'] ) );
			update_user_meta( $current_user->ID, 'facebook_url', $facebook_url );
		} else {
			delete_user_meta( $current_user->ID, 'facebook_url' );
		}

		// Update twitter url
		if ( !empty( $_POST['twitter-url'] ) ) {
			$twitter_url = esc_url_raw( sanitize_text_field( $_POST['twitter-url'] ) );
			update_user_meta( $current_user->ID, 'twitter_url', $twitter_url );
		} else {
			delete_user_meta( $current_user->ID, 'twitter_url' );
		}

		// Update linkedIn url
		if ( !empty( $_POST['linkedin-url'] ) ) {
			$linkedin_url = esc_url_raw( sanitize_text_field( $_POST['linkedin-url'] ) );
			update_user_meta( $current_user->ID, 'linkedin_url', $linkedin_url );
		} else {
			delete_user_meta( $current_user->ID, 'linkedin_url' );
		}

		// todo: add instagram and pin

		// Update user password
		if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
			if ( $_POST['pass1'] == $_POST['pass2'] ) {
				$return = wp_update_user( array(
					'ID' => $current_user->ID,
					'user_pass' => $_POST['pass1']
				) );
				if ( is_wp_error( $return ) ) {
					$errors[] = $return->get_error_message();
				}
			} else {
				$errors[] = esc_html__('The passwords you entered do not match.  Your password is not updated.', 'inspiry-real-estate');
			}
		}

		// if everything is fine
		if ( count( $errors ) == 0 ) {

			//action hook for plugins and extra fields saving
			do_action( 'edit_user_profile_update', $current_user->ID );

			$response = array(
				'success' => true,
				'message' => esc_html__( 'Profile information is updated successfully!', 'inspiry-real-estate' ),
			);
			echo json_encode( $response );
			die;
		}

	} else {
		$errors[] = esc_html__('Security check failed!', 'inspiry-real-estate');
	}

	// in case of errors
	$response = array(
		'success' => false,
		'errors' => $errors
	);
	echo json_encode( $response );
	die;

}
add_action( 'wp_ajax_inspiry_update_profile', 'ire_update_profile' );    // only for logged in user
add_action( 'wp_ajax_nopriv_inspiry_update_profile', 'ire_update_profile' ); 


/**
 * agent profile save request handler
 */
function ire_update_profile_agent() {

	$errors = array();
	$response = array();

	if (!wp_get_current_user()){
	$errors[] = esc_html__( 'No login found.', 'inspiry-real-estate' );
	$response[] = array('error_login' => esc_html__( 'No login found.', 'inspiry-real-estate' ));
	} else {
	// Get user info
	$current_user = wp_get_current_user();
	}

	if ($_POST[aId] == ""){

	$errors[] = esc_html__( 'No agent id for profile update.', 'inspiry-real-estate' );
	$response[] = array('error_id' => esc_html__( 'No agent id for profile update.', 'inspiry-real-estate' ));

	}

	$agent_profile[aId] = $_POST[aId];
	

	//if( wp_verify_nonce( $_POST['user_profile_nonce'], 'update_user' ) ) {


		// Update first name
		if ( !empty( $_POST['aName'] ) ) {
			$user_name = sanitize_text_field( $_POST['aName'] );
			add_post_meta( $agent_profile[aId], 'name-override', $user_name );
		} else {
			delete_post_meta( $agent_profile[aId], 'name-override' );
		}
		//‘name-override’.‘email-override’.‘phone-override’. ‘agent-secret-url’.‘agent-follow-up’.‘call-center-agent’.



				// Update first name
		if ( !empty( $_POST['aPhone'] ) ) {
			$user_phone = sanitize_text_field( $_POST['aPhone'] );
			add_post_meta( $agent_profile[aId], 'phone-override', $user_phone );
		} else {
			delete_post_meta( $agent_profile[aId], 'phone-override' );
		}

		// Update user email
		if ( !empty( $_POST['aEmail'] ) ){
			$user_email = is_email( sanitize_email ( $_POST['aEmail'] ) );
			if ( !$user_email ) {
				$errors[] = esc_html__( 'Provided email address is invalid.', 'inspiry-real-estate' );
				//$response[] = array('error_email' => esc_html__('Provided email address is invalid.', 'inspiry-real-estate' ));

			 } else {
				$email_exists = get_post_meta($agent_profile[aId],"email-override",true);
				//$email_exists = email_exists( $user_email );    // email_exists returns a user id if a user exists against it
				if($email_exists) {
					
						$errors[] = esc_html__('Provided email is already in use by another user. Try a different one.', 'inspiry-real-estate');
						$response[] = array('error_email_exist' => esc_html__('Provided email is already in use by another user. Try a different one.', 'inspiry-real-estate' ));
			
				} else {
					//$return = wp_update_user( array ('ID' => $agent_profile[aId], 'user_email' => $user_email ) );
					$return = add_post_meta( $agent_profile[aId], 'phone-override', $user_email );
					if ( is_wp_error( $return ) ) {
						$errors[] = $return->get_error_message();
					}
				}
			}
		}

		// 
		if ( !empty( $_POST['aUrl'] ) ) {
			$user_aurl = sanitize_text_field( $_POST['aUrl'] );
			add_post_meta( $agent_profile[aId], 'agent-secret-url', $user_aurl );
		} else {
			delete_post_meta( $agent_profile[aId], 'agent-secret-url' );
		}

				// ‘agent-follow-up’.‘call-center-agent’.
		if ( !empty( $_POST['aFallowup'] ) ) {
			$user_fallowup = sanitize_text_field( $_POST['aFallowup'] );
			add_post_meta( $agent_profile[aId], 'agent-follow-up', $user_fallowup );
		} else {
			delete_post_meta( $agent_profile[aId], 'agent-follow-up' );
		}

				// 
		if ( !empty( $current_user->ID ) ) {
			$call_center_agent = sanitize_text_field( $current_user->ID  );
			add_post_meta( $agent_profile[aId], 'call-center-agent', $call_center_agent );
		} 


			if ( !empty(  $agent_profile[aId] ) ) {

			// get the user meta

			if($user_meta = get_user_meta($agent_profile[aId], 'agent-history')){

			//json_decode()
			$user_meta_array = json_decode($user_meta);

			// add to array
			//$agent_history[] = [ 'lagentId' => $agent_profile[aId] , 'name' => $user_name, 'phone' => $user_phone , 'email' => $user_email ] ;

			$agent_history[] = array ( 'lagentId' => $agent_profile["aId"] , 'name' => $user_name, 'phone' => $user_phone , 'email' => $user_email ) ;

			$user_meta_json = json_encode($user_meta_array);

			update_user_meta($agent_profile[aId],'agent-history', $user_meta_json );

			} else {

				$agent_history[] = array ( 'lagentId' => $agent_profile["aId"] , 'name' => $user_name, 'phone' => $user_phone , 'email' => $user_email ) ;

				$user_meta_json = json_encode($agent_history);

				add_user_meta($agent_profile[aId], 'agent-history', $user_meta_json);

			}

		}
			//add_user_meta ,delete_user_meta, get_user_meta, update_user_meta


		// update user description

		/*
		if ( !empty( $_POST['description'] ) ) {
			$user_description = sanitize_text_field( $_POST['description'] );
			update_user_meta( $agent_profile[aId], 'description', $user_description );
		} else {
			delete_user_meta( $agent_profile[aId], 'description' );
		}
		*/




		// if everything is fine
		if ( count( $errors ) < 1 ) {

			//action hook for plugins and extra fields saving
			//do_action( 'edit_user_profile_update', $agent_profile[aId] );

			$response = array(
				'success' => true,
				'message' => esc_html__( 'Profile information is updated successfully!', 'inspiry-real-estate' ),
			);
			echo json_encode( $response );
			die;
		//} 

	} else {
		//$errors[] = esc_html__('Security check failed!', 'inspiry-real-estate');

			// in case of errors
	$response[] = array(
		'success' => false,
		'errors' => $errors
	);
	echo json_encode( $response );
	die;
	}





}
add_action( 'wp_ajax_ire_update_profile_agent', 'ire_update_profile_agent' );    // only for logged in user
//add_action( 'wp_ajax_nopriv_inspiry_update_profile', 'ire_update_profile' ); 


