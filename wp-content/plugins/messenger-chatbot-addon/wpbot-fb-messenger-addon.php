<?php
/**
* Plugin Name: Messenger ChatBot Addon
* Plugin URI: https://www.quantumcloud.com/products/plugins/
* Description: Add the ability for WPBot to chat with the users on a Facebook page & messenger
* Version: 2.0.0
* Author: QuantumCloud
* Author URI: https://www.quantumcloud.com/
* Requires at least: 4.6
* Tested up to: 5.3
* License: GPL2
*/


defined('ABSPATH') or die("No direct script access!");

if( !defined('WBFB_PATH') )
	define( 'WBFB_PATH', plugin_dir_path(__FILE__) );
if( !defined('WBFB_URL') )
	define( 'WBFB_URL', plugin_dir_url(__FILE__ ) );

require_once 'wpbot-fb-messenger-functions.php';
require_once 'admin/wpbot-fb-admin-page.php';
require('plugin-upgrader/plugin-upgrader.php');

add_action('init', 'qcpd_wpfb_messenger_checking_dependencies');
function qcpd_wpfb_messenger_checking_dependencies(){
	include_once(ABSPATH.'wp-admin/includes/plugin.php');
	
	if ( !class_exists('QCLD_Woo_Chatbot') && !class_exists('qcld_wb_Chatbot') && (qcpd_wpfb_is_kbxwpbot_active() != true) ) {
		add_action('admin_notices', 'qcpd_wpfb_require_notice');
	}
}


function qcpd_wpfb_require_notice()
{
?>
	<div id="message" class="error">
		<p>
			<?php echo esc_html__('Please install & activate the Chatbot Pro plugin and configure the Artificial Intelligence properly to get the Facebook Messenger Addon to work.', 'wpfb'); ?>
		</p>
	</div>
<?php
}

function qcpd_wpfb_activation_redirect( $plugin ) {
    if( $plugin == plugin_basename( __FILE__ ) ) {
        exit( wp_redirect( admin_url( 'admin.php?page=messenger-chatbot-help-license') ) );
    }
}
add_action( 'activated_plugin', 'qcpd_wpfb_activation_redirect' );

/**
 *
 * Function to load translation files.
 *
 */
function qcpd_wpfb_addon_lang_init() {
    load_plugin_textdomain( 'wpfb', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

add_action( 'plugins_loaded', 'qcpd_wpfb_addon_lang_init');

//Let's go
add_action('init', 'qcpd_wpfb_messenger_callback');


function qcpd_wpfb_is_kbxwpbot_active(){
	if ( defined( 'KBX_WP_CHATBOT' ) && (KBX_WP_CHATBOT == '1') ) {
		return true;
	}else{
		return false;
	}
}

//form builder support functions
if(!function_exists('qcwpbot_get_form_fb')){
	function qcwpbot_get_form_fb($formid){
		global $wpdb;

		$formid = sanitize_text_field($formid);

		$result = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix."wfb_forms where form_id='".$formid."' and type='primary'");
		$form = unserialize($result->config);
		$fields = $form['fields'];
		//print_r($form['layout_grid']['fields']);exit;
		if(isset($form['layout_grid']['fields']) && !empty($form['layout_grid']['fields'])){
			
			$firstfield = qc_get_first_field($form['layout_grid']['fields']);
			$field = $fields[$firstfield];
			return $field;
		}
		
	}
}

if(!function_exists('qcwpbot_capture_form_value_fb')){
	function qcwpbot_capture_form_value_fb($formid, $fieldid, $answer, $entry){
		global $wpdb;

		$formid = sanitize_text_field($formid);
		$fieldid = sanitize_text_field($fieldid);
		$answer = $answer;
		$entry = sanitize_text_field($entry);

		if($entry==0){
			$wpdb->insert(
				$wpdb->prefix."wfb_form_entries",
				array(
					'datestamp'  => current_time( 'mysql' ),
					'user_id'   => 0,
					'form_id'	=> $formid,
					'status'	=> 'active'
				)
			);

			$entry = $wpdb->insert_id;
		}

		$result = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix."wfb_forms where form_id='".$formid."' and type='primary'");
		$form = unserialize($result->config);
		
		
		$processors = (isset($form['processors'])?$form['processors']:array());
		
		$mailer = (isset($form['mailer'])?$form['mailer']:array());
		
		$variables = $form['variables'];

		$fieldetails = qc_get_details_by_fieldid($form, $fieldid);

		if($answer!=''){
			$wpdb->insert(
				$wpdb->prefix."wfb_form_entry_values",
				array(
					'entry_id'  => $entry,
					'field_id'   => $fieldid,
					'slug'	=> (isset($fieldetails['slug'])?$fieldetails['slug']:''),
					'value'	=> stripslashes($answer)
				)
			);
		}

		$fields = $form['fields'];
		$conditions = array();
		if(isset($form['conditional_groups']['conditions'])){
			$conditions = $form['conditional_groups']['conditions'];
		}

		
		if(isset($form['layout_grid']['fields']) && !empty($form['layout_grid']['fields'])){
			
			$nextfield = qc_get_next_field($form, $fieldid, $entry);
			

			if($nextfield!='none' && !empty($fields[$nextfield])){

				$field = $fields[$nextfield];
				$field = qc_check_field_variables($field, $variables, $entry);
				$field['entry'] = $entry;
				$field['status'] = 'incomplete';
				if($field['type']=='calculation'){
					$field = qc_formbuilder_do_calculation($field, $entry);
				}
				
				return $field;

			}else{

				if(isset($mailer['on_insert']) && $mailer['on_insert']==1){
					$answers = qc_form_answer($fields, $entry);
					qcld_wb_chatbot_send_form_query($answers, $name, $mailer);
				}
				
				if(!empty($processors) && isset($processors[qc_array_key_first($processors)]['runtimes'])){
					$entrydetails = qc_form_entry_details($fields, $entry);
					qcld_wb_chatbot_send_autoresponse($entrydetails, $processors);
				}
				
				return array('status'=>'complete');
			}
			
		}else{

			if(isset($mailer['on_insert']) && $mailer['on_insert']==1){
				$answers = qc_form_answer($fields, $entry);
				qcld_wb_chatbot_send_form_query($answers, $name, $mailer);
			}

			if(!empty($processors) && isset($processors[qc_array_key_first($processors)]['runtimes'])){
				$entrydetails = qc_form_entry_details($fields, $entry);
				qcld_wb_chatbot_send_autoresponse($entrydetails, $processors);
			}
			
			return array('status'=>'complete');
		}
		
		die();
	}
}

function qcld_handle_formbuilder_response($get_formidby_keyword, $sender, $access_token){
	
	$jsonData = '{
		"recipient":{
			"id":"'.$sender.'"
		},
		"sender_action":"typing_on"
	}';
	qcpd_wpfb_send_fb_reply($jsonData, $access_token);
	sleep(2);
	
	update_option($sender.'_conversational_form', 'active');
	update_option($sender.'_conversational_form_id', $get_formidby_keyword);
	
	$formresponse = qcwpbot_get_form_fb($get_formidby_keyword);
	
	$fieldid = $formresponse['ID'];
	$formtype = $formresponse['type'];
	$formlabel = $formresponse['label'];
	update_option($sender.'_conversational_field_id', $fieldid);
	update_option($sender.'_conversational_field_entry', 0);
	
	if($formtype=='dropdown' || $formtype=='checkbox'){
		
		$fieldoptions = $formresponse['config']['option'];
		$all_faqs = array();
		foreach($fieldoptions as $fieldoption){
			$all_faqs[] = $fieldoption['value'];
		}
		
		$multiarray = array();
		while(!empty($all_faqs)){
			if(count($all_faqs)>3){
				$multiarray[] = array_slice($all_faqs, 0, 3);
				unset($all_faqs[0]);
				unset($all_faqs[1]);
				unset($all_faqs[2]);
				$all_faqs = array_values($all_faqs);
			}else{
				$multiarray[] = $all_faqs;
				unset($all_faqs);
				$all_faqs = array();
			}
		}

		$elementjson = '';
		foreach($multiarray as $element){
			$buttonjson = '';
			foreach($element as $button){
				$buttonjson .= '{
					"type":"postback",
					"title":"'.$button.'",
					"payload":"'.$button.'"
				},';
			}
			$elementjson .= '{
				"title": "'.$formlabel.'",
				"buttons": [
				  '.$buttonjson.'
				]
			  },';
			
		}
		
		$jsonData = '{
			"recipient":{
				"id":"'.$sender.'"
			},
			"message":{
				"attachment":{
				  "type":"template",
				  "payload":{
					"template_type":"generic",
					"elements":[
						'.$elementjson.'
					]
				  }
				}
			  }
		}';
		qcpd_wpfb_send_fb_reply($jsonData, $access_token);exit;
		
	}elseif($formtype=='html'){
		$formlabel = $formresponse['config']['default'];
		$jsonData = '{
			"recipient":{
				"id":"'.$sender.'"
			},
			"message":{
				"text":"'.$formlabel.'"
			}
		}';
		qcpd_wpfb_send_fb_reply($jsonData, $access_token);
		qcld_handle_cfb_next('', $sender, $access_token);
		
	}else{
		$jsonData = '{
			"recipient":{
				"id":"'.$sender.'"
			},
			"message":{
				"text":"'.$formlabel.'"
			}
		}';
		qcpd_wpfb_send_fb_reply($jsonData, $access_token);
		
	}
}

function qcld_handle_cfb_next($answer='', $sender, $access_token){
	
	$formid = get_option($sender.'_conversational_form_id');
	$fieldid = get_option($sender.'_conversational_field_id');
	$entry = get_option($sender.'_conversational_field_entry');
	
	$formresponse = qcwpbot_capture_form_value_fb($formid, $fieldid, $answer, $entry);
	
	if($formresponse['status']=='incomplete'){
		update_option($sender.'_conversational_field_entry', $formresponse['entry']);
		update_option($sender.'_conversational_field_id', $formresponse['ID']);
		
		$formtype = $formresponse['type'];
		$formlabel = $formresponse['label'];
		
		
		if($formtype=='dropdown' || $formtype=='checkbox'){			
			$fieldoptions = $formresponse['config']['option'];
			$all_faqs = array();
			foreach($fieldoptions as $fieldoption){
				$all_faqs[] = $fieldoption['value'];
			}
			
			$multiarray = array();
			while(!empty($all_faqs)){
				if(count($all_faqs)>3){
					$multiarray[] = array_slice($all_faqs, 0, 3);
					unset($all_faqs[0]);
					unset($all_faqs[1]);
					unset($all_faqs[2]);
					$all_faqs = array_values($all_faqs);
				}else{
					$multiarray[] = $all_faqs;
					unset($all_faqs);
					$all_faqs = array();
				}
			}

			$elementjson = '';
			foreach($multiarray as $element){
				$buttonjson = '';
				foreach($element as $button){
					$buttonjson .= '{
						"type":"postback",
						"title":"'.$button.'",
						"payload":"'.$button.'"
					},';
				}
				$elementjson .= '{
					"title": "'.$formlabel.'",
					"buttons": [
					  '.$buttonjson.'
					]
				  },';
				
			}
			
			$jsonData = '{
				"recipient":{
					"id":"'.$sender.'"
				},
				"message":{
					"attachment":{
					  "type":"template",
					  "payload":{
						"template_type":"generic",
						"elements":[
							'.$elementjson.'
						]
					  }
					}
				  }
			}';
			qcpd_wpfb_send_fb_reply($jsonData, $access_token);exit;
			
			
		}elseif($formtype=='html'){
			$formlabel = $formresponse['config']['default'];
			$jsonData = '{
				"recipient":{
					"id":"'.$sender.'"
				},
				"message":{
					"text":"'.$formlabel.'"
				}
			}';
			qcpd_wpfb_send_fb_reply($jsonData, $access_token);
			qcld_handle_cfb_next('', $sender, $access_token);
			
		}elseif($formtype=='calculation'){
			
			$formlabel = $formresponse['calresult'];
			$jsonData = '{
				"recipient":{
					"id":"'.$sender.'"
				},
				"message":{
					"text":"'.$formlabel.'"
				}
			}';
			qcpd_wpfb_send_fb_reply($jsonData, $access_token);
			qcld_handle_cfb_next($formresponse['calvalue'], $sender, $access_token);
			
		}elseif($formtype=='hidden'){
			qcld_handle_cfb_next($formresponse['config']['default'], $sender, $access_token);
			
		}else{
			$jsonData = '{
				"recipient":{
					"id":"'.$sender.'"
				},
				"message":{
					"text":"'.$formlabel.'"
				}
			}';
			qcpd_wpfb_send_fb_reply($jsonData, $access_token);exit;
		}
		
		
	}else{
		delete_option($sender.'_conversational_field_entry');
		delete_option($sender.'_conversational_field_id');
		delete_option($sender.'_conversational_form_id');
		delete_option($sender.'_conversational_form');
		exit;
	}
	
}

function qcld_wpbo_search_response_fb($keyword){
	global $wpdb;
	$keyword = sanitize_text_field($keyword);
	$table = $wpdb->prefix.'wpbot_response';

	$response_result = array();

	$status = array('status'=>'fail', 'multiple'=>false);

	$results = $wpdb->get_results("SELECT `query`, `response`, MATCH(`query`,`keyword`,`response`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) as score FROM $table WHERE MATCH(`query`,`keyword`,`response`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) order by score desc limit 15");

	$weight = get_option('qc_bot_str_weight')?get_option('qc_bot_str_weight'):'0.4';

	if(!empty($results)){
		foreach($results as $result){
			if($result->score > $weight){
				$response_result[] = array('query'=>$result->query, 'response'=>$result->response, 'score'=>$result->score);
			}
		}
	}

	if(empty($response_result)){

		$wpdb->query("ALTER TABLE $table ADD FULLTEXT(`query`)");
        $wpdb->query("ALTER TABLE $table ADD FULLTEXT(`keyword`)");
		$wpdb->query("ALTER TABLE $table ADD FULLTEXT(`response`)");

		$results = $wpdb->get_results("SELECT `query`, `response`, MATCH(`query`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) as score FROM $table WHERE MATCH(`query`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) order by score desc limit 15");

		$weight = get_option('qc_bot_str_weight')?get_option('qc_bot_str_weight'):'0.4';

		if(!empty($results)){
			foreach($results as $result){
				if($result->score > $weight){
					$response_result[] = array('query'=>$result->query, 'response'=>$result->response, 'score'=>$result->score);
				}
			}
		}

	}

	if(empty($response_result)){

		$results = $wpdb->get_results("SELECT `query`, `response`, MATCH(`keyword`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) as score FROM $table WHERE MATCH(`keyword`) AGAINST('".$keyword."' IN NATURAL LANGUAGE MODE) order by score desc limit 15");

		$weight = get_option('qc_bot_str_weight')?get_option('qc_bot_str_weight'):'0.4';

		if(!empty($results)){
			foreach($results as $result){
				if($result->score > $weight){
					$response_result[] = array('query'=>$result->query, 'response'=>$result->response, 'score'=>$result->score);
				}
			}
		}

	}


	if(!empty($response_result)){

		if(count($response_result)>1){
			$status = array('status'=>'success', 'multiple'=>true, 'data'=>$response_result);
		}else{
			$status = array('status'=>'success', 'multiple'=>false, 'data'=>$response_result);
		}

	}
	return $status;

}