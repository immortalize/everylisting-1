<?php commentPHPContext('/wp-content/themes/inspiry-real-places/single-jurisdiction.php'); ?>

<?php
/*
 * Header
 */

get_header();

get_template_part( 'partials/header/banner' );
// echo get_the_title();

$id = null;

?>
<!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">-->
    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">

                <div class="row">

                    <div class="col-md-12 site-main-content">
                        
                        <main id="main" class="site-main default-page blog-single-post clearfix">
                            <?php
                            $content1 = '';
                            $countrywithlatest = false;
                            $country = '';
                            if ( have_posts() ):

                                while ( have_posts() ):

                                    the_post();
                                    
                                    $format = get_post_format(get_the_ID());
                                    if ( false === $format ) {
                                        $format = 'standard';
                                    }
                                    ?>
                                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
                                        
                                        <?php
                                        // image, gallery or video based on format
                                        if ( in_array( $format, array( 'standard', 'image', 'gallery', 'video' ) ) ) :
                                            
                                           
                                            get_template_part( 'partials/post/entry-format', $format );
                                        endif;
                                        ?>
                            
                                        <!--<header >-->
                                           
                                            <?php
                                            //class="entry-header blog-post-entry-header"
                                            // title
                                            //get_template_part( 'partials/post/entry-title' );

                                            // meta
                                            //get_template_part( 'partials/post/entry-meta' );
                                            ?>
                                        <!--</header>-->

                                        <div class="entry-content clearfix">
                                            <?php  $content =  get_the_content();
                                            $countrywithlatest = true;
                                           
                                            $id = get_the_ID();
                                            
                                            $code = get_post_meta($id, 'IDXGenerator_jurisdiction_code', true);
                                            if ((isset($code)) && ($code != "")) {
                                                ?>
                                                <h1 class="text-center"><img src="https://www.countryflags.io/<?php echo strtolower($code);?>/shiny/64.png" style="padding: 20px;"><?php the_title(); ?></h1> 
                                                <?php
                                            } else {
                                                ?> 
                                                <h1 class="text-center"><?php get_template_part( 'partials/post/entry-title' ); ?></h1> 
                                                <?php
                                            }
                                            
                                            $content = str_replace("[valuation_accordion]",do_shortcode("[juris-accordion post_id='$id' accordiontitle='Categories and Valuation' is_set_search='1']"),$content);
                                                                                        
                                            $title = get_post_meta($id, 'IDXGenerator_purchase_valuation_title', true);
                                            if ($title) {
                                                $title = 'Purchases - ' .$title;
                                            } else {
                                                $title = 'Purchases';
                                            }
                                                                                        
                                            $content = str_replace("[purchase_valuation_table]",do_shortcode("[table-juris-purchases post_id='$id' title='" . $title . "' is_set_search='1']"),$content);     
                                            
                                            $title = get_post_meta($id, 'IDXGenerator_lease_valuation_title', true);
                                            if ($title) {
                                                $title = 'Leases - '.$title;
                                            } else {
                                                $title = 'Leases (per month)';
                                            }
                                            
                                            $content = str_replace("[lease_valuation_table]",do_shortcode("[table-juris-leases post_id='$id' title='" . $title . "' is_set_search='1']"),$content);    
                                            
                                            $title = get_post_meta($id, 'IDXGenerator_rent_valuation_title', true);
                                            if ($title) {
                                                $title = 'Vacation Rentals - '.$title ;
                                            } else {
                                                $title = 'Vacation Rentals (per day)';
                                            }
                                           
                                            $content = str_replace("[rent_valuation_table]",do_shortcode("[table-juris-rents post_id='$id' title='" . $title . "' is_set_search='1']"),$content);
                                            echo $content;
                                            ?>
                                            
                                            <?php
                                            wp_link_pages( array(
                                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'inspiry' ),
                                                'after'  => '</div>',
                                            ) );                                            
                                            ?>
                                        </div>

                                        <!--<footer class="post-footer">-->
                                        <!--    <?php //the_tags( '<span class="tag-links">', '', '</span>' ); ?>-->
                                        <!--</footer>-->

                                    </article>

                                    <?php
                                    // comments
                                    //get_template_part( 'partials/post/entry-comments' );
                                    
                                    //$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
                                   // print_r($_SERVER['REQUEST_URI']);
                                    
                                endwhile;

                            endif;
                            wp_reset_postdata();
                            ?>

                        </main>
                        <!-- .site-main -->

                    </div>
                    <!-- .site-main-content -->

                    <?php if ( inspiry_is_active_custom_sidebar( 'default-sidebar' ) ) : ?>
                        <div class="col-md-3 site-sidebar-content">
                            <?php //get_sidebar(); ?>
                        </div>
                        <!-- .site-sidebar-content -->
                    <?php endif; ?>                    

                </div>
                <!-- .row -->

            <!-- .container -->

        </div>
              
        <!-- .site-content -->
 <?php get_template_part( 'partials/latest_properties' ); ?>

 <?php // echo '<div class="" id="aftersearchshow">';
       // echo do_shortcode('[ajax_load_more post_type="property"  orderby="relevance" posts_per_page="20" scroll="true" css_classes="plain-text" sticky_posts="true" progress_bar="true" progress_bar_color="007000" button_label="Show more properties" button_loading_label="Searching Properties" no_results_text="No property found"]');
       // echo '</div>';
 ?>
    </div><!-- .site-content-wrapper -->

<?php
/*
 * Footer
 */

get_footer();
?>

<!--<script src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>-->
<script>
// jQuery('.datatable').DataTable();

 if (jQuery(window).width() > 1024) {
        jQuery( "#purchase" ).prop( "checked", false );
        jQuery("#purchases_table").slideDown();
        
        jQuery( "#lease" ).prop( "checked", false );
        jQuery("#lease_table").slideDown();
        
        jQuery( "#rent" ).prop( "checked", false );
        jQuery("#rent_table").slideDown();
 }

 
</script>