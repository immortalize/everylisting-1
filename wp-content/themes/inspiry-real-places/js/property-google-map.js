if (typeof propertyMapData !== "undefined") {

    function initialize_property_map() {

        var url = propertyMapData.icon;
        var size = new google.maps.Size(42, 57);

        // retina
        if (window.devicePixelRatio > 1.5) {
            if (propertyMapData.retinaIcon) {
                url = propertyMapData.retinaIcon;
                size = new google.maps.Size(83, 113);
            }
        }

        var image = {
            url: url,
            size: size,
            scaledSize: new google.maps.Size(42, 57),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(21, 56)
        };

        var propertyLocation = new google.maps.LatLng(propertyMapData.lat, propertyMapData.lang);
        var propertyMapOptions = {
            center: propertyLocation,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };
        var propertyMap = new google.maps.Map(document.getElementById("property-map"), propertyMapOptions);
        var propertyMarker = new google.maps.Marker({
            position: propertyLocation,
            map: propertyMap,
            icon: image
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize_property_map);
}