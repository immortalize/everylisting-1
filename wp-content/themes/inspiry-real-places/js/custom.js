
(function ($) {
    
        
        jQuery("#purchase").change(function() {
            if(this.checked) {
                
                $("#purchases_table").slideUp();
            }else{
                $("#purchases_table").slideDown();
            }
        });
        
        
        jQuery("#rent").change(function() {
            if(this.checked) {
                
                $("#rent_table").slideUp();
            }else{
                $("#rent_table").slideDown();
            }
        });
        
        jQuery("#lease").change(function() {
            if(this.checked) {
                
                $("#lease_table").slideUp();
            }else{
                $("#lease_table").slideDown();
            }
        });

    jQuery('.table-responsive-stack').find("th").each(function (i) {
      
      jQuery('.table-responsive-stack td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">'+ $(this).text() + ':</span> ');
      jQuery('.table-responsive-stack-thead').hide();
   });
   
   
   jQuery( '.table-responsive-stack' ).each(function() {
      var thCount = $(this).find("th").length; 
       var rowGrow = 100 / thCount + '%';
       //console.log(rowGrow);
      jQuery(this).find("th, td").css('flex-basis', rowGrow);   
    });
        
         $(window).on('scroll', function () {
            if($( "#moreloadidof" ).hasClass( "moreloaddivcls" )){
                  if ($(window).scrollTop() > 250) {
                          $('.loadmore').click();
                         // $('.loadmore').hide();
                  }
            }
        });
        
    //  $('.site-pages').scroll(function(){
    //      alert();
    //       if ($(window).height() > 250) {
                  
    //               $('.loadmore').click();
    //       }
    //  });
  
    
            function flexTable(){
                   if ($(window).width() < 768) {
                      
                   jQuery(".table-responsive-stack").each(function (i) {
                      jQuery(this).find(".table-responsive-stack-thead").show();
                      jQuery(this).find('thead').hide();
                   });
                   
                  
                    
                   // window is less than 768px   
                   } else {
                      
                      
                   jQuery(".table-responsive-stack").each(function (i) {
                      jQuery(this).find(".table-responsive-stack-thead").hide();
                      jQuery(this).find('thead').show();
                   });
                      
                   }
                // flextable   
            }  
        
        flexTable();
        
        function toggle_show(){
                     $("#lease_table").slideUp();
                     $("#purchases_table").slideUp();
                     $("#rent_table").slideUp();
                   if ($(window).width() < 1024) {
                       $("#purchases_table").slideUp();
                       $("#rent_table").slideUp();
                       $("#lease_table").slideUp();
                       
                       $( "#purchase" ).prop( "checked", true );
                       $( "#rent" ).prop( "checked", true );
                       $( "#lease" ).prop( "checked", true );
                   }
        }  
        
        toggle_show();
   
        window.onresize = function(event) {
            flexTable();
            toggle_show();
        };


        jQuery('.table-responsive-stack').DataTable( {       

            hideEmptyCols: true,
            bPaginate: false,
            searching: false,
            info:   false
        } );
    
    jQuery('.table-responsive-stack tbody tr').on('click', function () {
       href = jQuery(this).data('href');
        
        if(href !== "" && href !== undefined ){
              window.location.href = href;
        }
        
       
    } );
    
    "use strict";

    var $window = $(window),
        $body = $('body'),
        isRtl = $body.hasClass('rtl');

    /*-----------------------------------------------------------------------------------*/
    /* Sliders
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().flexslider) {
        var homepageSlider = $('.homepage-slider'),
            gallerySlider = $('.gallery-slider'),
            gallerySliderTwo = $('.gallery-slider-two');

        homepageSlider.flexslider({
            slideshow: true,
            slideshowSpeed: 4000,
            pauseOnHover: true,
            touch: true,
            prevText: "",
            nextText: "",
            controlNav: false,
            rtl: isRtl,
            start: function (slider) {
                slider.delay(400).removeClass('slider-loader');
                centerSlideDetails(slider.h);
            }
        });

        gallerySlider.flexslider({
            animation: "slide",
            slideshow: true,
            rtl: isRtl,
            prevText: "",
            nextText: ""
        });

        gallerySliderTwo.flexslider({
            animation: "fade",
            slideshow: true,
            rtl: isRtl,
            directionNav: false
        });
    }

    function centerSlideDetails(slideHeight) {
        var slider = '',
            siteHeader = $('.site-header'),
            isHeaderOne = siteHeader.hasClass('header-variation-one') && $window.width() > 1182;

        if (homepageSlider.hasClass('slider-variation-two')) {
            slider = $('.slider-variation-two .slides li');
        }else if (homepageSlider.hasClass('slider-variation-three')) {
            slider = $('.slider-variation-three .slides li');
        }

        if( !slideHeight && slider ){
            slideHeight = slider.first().height();
        }

        if(slider){
            slider.each(function () {
                var slideOverlay = $(this).find('.slide-inner-container');
                if (isHeaderOne) {
                    slideOverlay.css('top', siteHeader.height() + 40);
                } else {
                    slideOverlay.css('top', Math.abs(((slideHeight - slideOverlay.outerHeight()) / 2)));
                }
            });
        }
    }

    $window.on('load resize', function () {
        centerSlideDetails();
    });


    if (jQuery().lightSlider) {
        $('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 10,
            loop: true,
            slideMargin: 0,
            galleryMargin: 0,
            thumbMargin: 2,
            currentPagerPosition: 'middle',
            rtl: isRtl
        });
        $('.lSPager').wrap('<div class="slider-thumbnail-nav-wrapper"></div>');
    }

    /*-----------------------------------------------------------------------------------*/
    /*  Carousels
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().owlCarousel) {
        var similarPropertiesCarousel = $(".similar-properties-carousel .owl-carousel"),
            similarPropertiesCarouselNav = $('.similar-properties-carousel-nav'),
            similarPropertiesItem = $('.similar-properties-carousel .hentry'),
            recentPostsCarousel = $(".recent-posts-carousel .owl-carousel"),
            recentPostsCarouselNav = $('.recent-posts-carousel-nav'),
            recentPostsItem = $('.recent-posts-item'),
            carouselNext = $(".carousel-next-item"),
            carouselPrev = $(".carousel-prev-item");


        similarPropertiesCarousel.owlCarousel({
            onInitialized: navToggle(similarPropertiesItem,similarPropertiesCarouselNav,1),
            rtl: isRtl,
            items: 1,
            smartSpeed: 500,
            loop: similarPropertiesItem.length > 1,
            autoHeight: similarPropertiesItem.length > 1,
            dots: false
        });


        carouselNext.on( 'click', function () {
            similarPropertiesCarousel.trigger('next.owl.carousel');
        });

        carouselPrev.on( 'click', function () {
            similarPropertiesCarousel.trigger('prev.owl.carousel');
        });

        recentPostsCarousel.owlCarousel({
            onInitialized: navToggle(recentPostsItem,recentPostsCarouselNav,2),
            rtl: isRtl,
            smartSpeed: 500,
            margin: 20,
            dots: false,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                1199: {
                    items: 2
                }
            }
        });


        carouselNext.on( 'click', function (event) {
            recentPostsCarousel.trigger('next.owl.carousel');
            event.preventDefault();
        });

        carouselPrev.on( 'click', function (event) {
            recentPostsCarousel.trigger('prev.owl.carousel');
            event.preventDefault();
        });
    }

    // Carousel Nav Toggle
    function navToggle(element,nav,items) {
        element.length > items ? nav.show() : nav.hide();
    }

    /*-----------------------------------------------------------------------------------*/
    /* Select2
    /* URL: http://select2.github.io/select2/
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().select2) {
        var selectOptions = {
            //minimumResultsForSearch: -1,  // Disable search feature in drop down
            width: 'off'
        };

        $('.search-select, .option-bar select').select2(selectOptions)
        .on("select2:open", function (e) {
            $('.select2-dropdown').hide();
            $('.select2-dropdown').slideDown(200);
        });

        if (isRtl) {
            selectOptions.dir = "rtl";
        }


        var AgentSelectOptions = {
            placeholder: "Select an Agent or more"
        }

        $('select:not(.lifestyles,.lifestyles1)').select2(selectOptions);
        $('#agent-selectbox').select2(AgentSelectOptions);
    }
    /*-----------------------------------------------------------------------------------*/
    /* Swipebox
    /* http://brutaldesign.github.io/swipebox/
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().swipebox) {
        $('.clone .swipebox').removeClass('swipebox');
        $(".swipebox").swipebox();

        $('a[data-rel]').each(function () {
            $(this).attr('rel', $(this).data('rel'));
        });
    }
    /*-----------------------------------------------------------------------------------*/
    /* Magnific Popup
    /* https://github.com/dimsemenov/Magnific-Popup
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().magnificPopup) {
        $(".video-popup").magnificPopup({
            type: 'iframe'
        });
    }

    /*-----------------------------------------------------------------------------------*/
    /*	Scroll to Top
     /*-----------------------------------------------------------------------------------*/
    $(function(){
        var scroll_anchor = $( '#scroll-top' ),
            post_nav = $( '.inspiry-post-nav' );
        $( window ).on('scroll', function () {
            if ( $( window ).width() > 980 ) {
                if ( $(this).scrollTop() > 250 ) {
                    scroll_anchor.fadeIn( 'fast' );
                    post_nav.fadeIn( 'fast' );
                    return;
                }
            }
            scroll_anchor.fadeOut( 'fast' );
            post_nav.fadeOut( 'fast' );
        });

        scroll_anchor.on( 'click', function ( event ) {
            event.preventDefault();
            $('html, body').animate( { scrollTop:0 }, 'slow' );
        });
    });

    $(function() {
        // eraseCookie('lifestyle');
        var selected = $('.lifestyles').val();
        var lifestylearr;
        var lifeStyleCookie = getCookie('lifestyle');
        if(lifeStyleCookie){
            lifestylearr = lifeStyleCookie.split(',');
        }
        
        var options = [{
            name   : 'Beach Living',
            value  : 'Beach',
            checked: found_in_array('Beach',lifestylearr)
        },{
            name   : 'Mountain Living',
            value  : 'Mountain',
            checked: found_in_array('Mountain',lifestylearr)
        },{
            name   : 'City Life',
            value  : 'City',
            checked: found_in_array('City',lifestylearr)
        },{
            name   : 'Private Island',
            value  : 'Private Island',
            checked: found_in_array('Private Island',lifestylearr)
        },{
            name   : 'Hotel Investment',
            value  : 'Hotel',
            checked: found_in_array('Hotel',lifestylearr)
        },
        {
            name   : 'Farm Land',
            value  : 'Farm Land',
            checked: found_in_array('Farm Land',lifestylearr)
        },
        {
            name   : 'Short-Term Rental Investment',
            value  : 'Short-Term Rental Investment',
            checked: found_in_array('Short-Term Rental Investment',lifestylearr)
        }];
        $('.lifestyles').val(selected);
        $('.lifestyles').attr('multiple', true);
        $('.lifestyles').find('option[value=""]').remove();
        $('.lifestyles').multiselect({
            search   : true,
            texts    : {
                placeholder: 'Lifestyles',
                search     : 'Search Lifestyles'
            }
        });
        $('.lifestyles').multiselect( 'loadOptions', options);
    });

function found_in_array(element,arr) {

    if(jQuery.inArray(element, arr) !== -1){
        return true;
    }else{
        return false;
    }
}

//$(function() {
//
//    var selected = $('.lifestyles1').val();
//
//    var options1 = [{
//        name   : 'Beach Living',
//        value  : 'Beach',
//        checked: false
//    },{
//        name   : 'Mountain Living',
//        value  : 'Mountain',
//        checked: false
//    },{
//        name   : 'City Life',
//        value  : 'City',
//        checked: false
//    },{
//        name   : 'Private Island',
//        value  : 'Private Island',
//        checked: false
//    },{
//        name   : 'Hotel Investment',
//        value  : 'Hotel',
//        checked: false
//    },
//    {
//        name   : 'Farm Land',
//        value  : 'Farm Land',
//        checked: false
//    },
//    {
//        name   : 'Short-Term Rental Investment',
//        value  : 'Short-Term Rental Investment',
//        checked: false
//    }];
//
//    $('.lifestyles1').val(selected);
//    $('.lifestyles1').attr('multiple', true);
//    $('.lifestyles1').find('option[value=""]').remove();
//    $('.lifestyles1').multiselect({
//        search   : true,
//        texts    : {
//            placeholder: 'Lifestyles',
//            search     : 'Search Lifestyles'
//        },
//
//    });
//    $('.lifestyles1').multiselect( 'loadOptions', options1);
//});

    /*-----------------------------------------------------------------------------------*/
    /* Sticky Header Function
    /*-----------------------------------------------------------------------------------*/
    var inspiryStickyHeader = $body.hasClass('inspiry-sticky-header');

    if( inspiryStickyHeader ) {
        var siteHeader = $('.site-header');

        $window.on( 'scroll', function() {
            var HeaderClasses = 'inspiry-sticked-header slideInDown animated',
                isHeaderVariationOne = siteHeader.hasClass('header-variation-one'),
                siteHeaderHeight = siteHeader.outerHeight(),
                windowPosition = $window.scrollTop(),
                adminbarOffset = $body.is( '.admin-bar' ) ? $( '#wpadminbar' ).height() : 0;

            if( windowPosition > siteHeaderHeight && $window.width() > 992 ){
                siteHeader.css('top', adminbarOffset ).addClass( HeaderClasses );

                if( isHeaderVariationOne ){
                    siteMainNav.show();
                    menuClose.show();
                } else {
                    $body.css('padding-top', siteHeaderHeight);
                }
            } else if ( windowPosition < 1 ) {
                siteHeader.css('top', 'auto' ).removeClass( HeaderClasses );
                $body.css('padding-top', 0);
            }
        });
    }

    /*-----------------------------------------------------------------------------------*/
    /* Main Menu
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().meanmenu) {
        $('#site-main-nav').meanmenu({
            meanMenuContainer: '#mobile-header',
            meanRevealPosition: "left",
            meanMenuCloseSize: "20px",
            meanScreenWidth: "991",
            meanExpand: '',
            meanContract: ''
        });
        $('#site-main-nav-left').meanmenu({
            meanMenuContainer: '#mobile-header',
            meanRevealPosition: "left",
            meanMenuCloseSize: "20px",
            meanScreenWidth: "991",
            meanExpand: '',
            meanContract: ''
        });
    }

    var mainMenuItem = $('#site-main-nav li');
    mainMenuItem.on( 'mouseenter',
        function () {
            $(this).children('ul').stop(true, true).slideDown(200);
        });

    mainMenuItem.on( 'mouseleave',
        function () {
            $(this).children('ul').stop(true, true).slideUp(200);
        }
    );
    var mainMenuItemLeft = $('#site-main-nav-left li');
    mainMenuItemLeft.on( 'mouseenter',
        function () {
            $(this).children('ul').stop(true, true).slideDown(200);
        });

    mainMenuItemLeft.on( 'mouseleave',
        function () {
            $(this).children('ul').stop(true, true).slideUp(200);
        }
    );

    // Code to show and hide main menu for home first variation.
    var siteMainNav = $('.header-menu-wrapper #site-main-nav'),
    	siteMainNav2 = $('.header-menu-wrapper #site-main-nav-left'),
        menuReveal = $('.button-menu-reveal'),
        menuClose = $('.button-menu-close');

    menuReveal.css('display', 'inline-block');
    menuClose.hide();
    siteMainNav.hide();
    siteMainNav2.hide();

    menuReveal.on('click', function (event) {
        $(this).stop(true, true).toggleClass('active');
        if (siteMainNav.is(":visible")) {
            siteMainNav.hide();
            menuClose.hide();
        } else {
            siteMainNav.show();
            menuClose.show();
        }
        if (siteMainNav2.is(":visible")) {
            siteMainNav2.hide();
            menuClose.hide();
        } else {
            siteMainNav2.show();
            menuClose.show();
        }
        event.preventDefault();
    });

    menuClose.on('click', function (event) {
        $(this).fadeToggle(20);
        siteMainNav.fadeToggle(20);
        siteMainNav2.fadeToggle(20);
        menuReveal.stop(true, true).toggleClass('active');
        event.preventDefault();
    });

    // Function to add User Nav and Social Nav in Mean Menu
    function customNav() {
        var menu = $('.mean-nav'),
            meanMenuReveal = $('.meanmenu-reveal'),
            backdrop = '<div class="mobile-menu-backdrop fade in"></div>',
            mobileHeaderNav = $('.mobile-header-nav').html();

        if ( $window.width() < 991 && menu.find('.mobile-header-nav-wrapper').length < 1 ) {
            menu.append(mobileHeaderNav);
        }

        // Show and hide user and social nav. Also add menu backdrop.
        meanMenuReveal.on('click', function () {
            menu.find('.mobile-header-nav-wrapper').stop(true, true).slideToggle();
            menu.stop(true, true).toggleClass('mobile-menu-visible');
            if (menu.hasClass('mobile-menu-visible')) {
                $('body').append(backdrop);
            } else {
                $('.mobile-menu-backdrop').remove();
            }
        });

        // Resolve Model Backdrop issue.
        $('.login-register-link').on('click', function () {
            meanMenuReveal.trigger('click');
        });
    }
    customNav();
    $window.on( 'resize', function () {
        customNav();
        menuClose.hide();
        siteMainNav.hide();
        siteMainNav2.hide();
        $('.mobile-menu-backdrop').remove();
    });

    /*-----------------------------------------------------------------------------------*/
    /*	Search Form Slide Toggle
    /*-----------------------------------------------------------------------------------*/
    // Function to show hidden fields on variation one
    var hiddenFields = $('.hidden-fields');

    $('.hidden-fields-reveal-btn').on( 'click', function (event) {
        $(this).stop(true, true).toggleClass('field-wrapper-expand');
        hiddenFields.stop(true, true).slideToggle(200);
        event.preventDefault();
    });

    var featureTitle = $('.expand-adv'),
        featureWrapper = $('.extra-search-fields .features-checkboxes-wrapper');

    featureTitle.on( 'click', function () {
        
          if($(this).hasClass('class111')){
            $(this).removeClass('class111');
            $(this).html('<i class="fa fa-plus" aria-hidden="true"></i>');
             $('.tooltiptext_1').show();
             $('.widthandhide').hide();
        }else{
            $(this).addClass('class111');
            $(this).html('<i class="fa fa-minus" aria-hidden="true"></i>');
            $('.tooltiptext_1').hide();
            $('.widthandhide').show();
        }
        
        $(this).stop(true, true).toggleClass('is-expand123');
        featureWrapper.stop(true, true).slideToggle(200);
      
        
    });

    /*-----------------------------------------------------------------------------------*/
    /*	Equal Height Function
    /*-----------------------------------------------------------------------------------*/
    function equalHeights(element) {
        var $element = $(element),
            elementHeights = [];

        $element.each(function () {
            var $this = $(this);
            elementHeights.push($this.outerHeight());
        });

        if ($window.width() > 750) {
            $element.css('height', Math.max.apply(Math, elementHeights));
        }
    }

    equalHeights('.featured-properties-one .property-description');
    equalHeights('.agent-listing-post');
    /*-----------------------------------------------------------------------------------*/
    /*	Home Property Listing hover Effect
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().hoverIntent) {
        var propertyListing = $(".property-listing-two .property-description, .featured-properties-two .property-description");

        propertyListing.each(function(){
            var $this = $(this);
            $this.css( 'height', $this.find('.entry-header').outerHeight());
        });

        propertyListing.hoverIntent(
            function () {
                var $this = $(this);

                $this.find('.property-meta').show();
                $this.stop(true, true).animate({
                    height: '100%'
                }, 300).addClass('hovered');
            },
            function () {
                var $this = $(this);

                $this.removeClass('hovered').stop(true, true).animate({
                    height: $this.find('.entry-header').outerHeight()
                }, 300);
            }
        );
    }

    /*-----------------------------------------------------------------------------------*/
    /*	Isotope for gallery pages
    /*-----------------------------------------------------------------------------------*/
    if (jQuery().isotope) {
        var galleryContainer = $('#gallery-container');

        $window.on('load', function () {
            galleryContainer.isotope({
                itemSelector: '.isotope-item',
                layoutMode: 'fitRows'
            });
        });

        $('#gallery-items-filter').on('click', 'a', function (event) {
            var $this = $(this),
                filterValue = $this.attr('data-filter');
            $(this).addClass('active').siblings().removeClass('active');
            galleryContainer.isotope({
                filter: filterValue
            });
            event.preventDefault();
        });
    }


    /*-----------------------------------------------------------------------------------*/
    /*	Animation CSS integrated with Appear Plugin
    /*----------------------------------------------------------------------------------*/
    function ie_10_or_older() {
        // check if IE10 or older
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            // return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            return true;
        }
        // other browser
        return false;
    }

    if (jQuery().appear) {
        if (($window.width() > 991) && (!ie_10_or_older())) {
            // apply animation on only big screens and browsers other than IE 10 and Older Versions of IE
            $('.animated').appear().fadeTo('fast', 0);

            $(document.body).on('appear', '.fade-in-up', function (event, $all_appeared_elements) {
                $(this).each(function () {
                    $(this).addClass('fadeInUp')
                });
            });

            $(document.body).on('appear', '.fade-in-down', function (event, $all_appeared_elements) {
                $(this).each(function () {
                    $(this).addClass('fadeInDown')
                });
            });

            $(document.body).on('appear', '.fade-in-right', function (event, $all_appeared_elements) {
                $(this).each(function () {
                    $(this).addClass('fadeInRight')
                });
            });

            $(document.body).on('appear', '.fade-in-left', function (event, $all_appeared_elements) {
                $(this).each(function () {
                    $(this).addClass('fadeInLeft')
                });
            });
        }
    }
    /*----------------------------------------------------------------------------------*/
    /* Placeholder Support for older browsers
    /*----------------------------------------------------------------------------------*/
    if (jQuery().placeholder) {
        $('input, textarea').placeholder();
    }

    /*----------------------------------------------------------------------------------*/
    /*	IE Browsers Detection
    /*----------------------------------------------------------------------------------*/
    function detectIE() {
        var ms_ie = false,
            ua = window.navigator.userAgent,
            new_ie = ua.indexOf('Trident/');
        if (ie_10_or_older() || (new_ie > -1)) {
            ms_ie = true;
        }
        if (ms_ie) {
            $(".gallery-slider-two").find('img').removeClass('img-thumbnail-responsive');
            $("body").addClass('ie-userAgent');
            return true;
        }
        return false;
    }

    detectIE();

    /*-----------------------------------------------------------------------------------*/
    /* WPML Language Selection
    /*-----------------------------------------------------------------------------------*/
    $(function () {
        var head = $("head"),
            inspiry_language_list = $('#inspiry_language_list'),
            headStyleLast = head.find("style[rel='stylesheet']:last"),
            styleElement = "<style rel='stylesheet' media='screen'>#inspiry_language_list{background-image: url('" + inspiry_language_list.find('li > img').attr("src") + "');}</style>";
        if (headStyleLast.length) {
            headStyleLast.after(styleElement);
        }
        else {
            head.append(styleElement);
        }
    });


    /*-----------------------------------------------------------------------------------*/
    /* Inspiry Highlighted Message
    /*-----------------------------------------------------------------------------------*/
    $('.inspiry-highlighted-message .close-message').on('click', function () {
        $('.inspiry-highlighted-message').remove();
    });


    /*-----------------------------------------------------------------------------------*/
    /* Page Loader
    /*-----------------------------------------------------------------------------------*/
    $window.on('load', function () {
        $(".page-loader-img").fadeOut();
        $(".page-loader").delay(300).fadeOut("slow", function () {
            $(this).remove();
        });

        // Trigger once to avoid the auto height issue.
        $('.similar-properties-carousel-nav .carousel-next-item').trigger('click');
    });

    /*-----------------------------------------------------------------------------------*/
    /*	Scroll to Top
    /*-----------------------------------------------------------------------------------*/
    $(function(){
        var scroll_anchor = $('#scroll-top');
        $( window ).on('scroll', function () {
            if ( $( window ).width() > 980 ) {
                if ( $(this).scrollTop() > 250 ) {
                    scroll_anchor.fadeIn('fast');
                    return;
                }
            }
            scroll_anchor.fadeOut('fast');
        });

        scroll_anchor.on( 'click', function ( event ) {
            event.preventDefault();
            $('html, body').animate( { scrollTop:0 }, 'slow' );
        });
    });

    /*-----------------------------------------------------------------*/
    /* Property Floor Plans
     /*-----------------------------------------------------------------*/
    $('.floor-plans-accordions .floor-plan:first-child').addClass('current')
        .children('.floor-plan-content').css('display', 'block').end()
        .find('i.fa').removeClass( 'fa-plus').addClass( 'fa-minus' );

    $('.floor-plan-title').on( 'click', function(){
        var parent_accordion = $(this).closest('.floor-plan');
        if ( parent_accordion.hasClass('current') ) {
            $(this).find('i.fa').removeClass( 'fa-minus').addClass( 'fa-plus' );
            parent_accordion.removeClass('current').children('.floor-plan-content').slideUp(300);
        } else {
            $(this).find('i.fa').removeClass('fa-plus').addClass( 'fa-minus' );
            parent_accordion.addClass('current').children('.floor-plan-content').slideDown(300);
        }
        var siblings = parent_accordion.siblings('.floor-plan');
        siblings.find('i.fa').removeClass( 'fa-minus').addClass( 'fa-plus' );
        siblings.removeClass('current').children('.floor-plan-content').slideUp(300);
    });

    /*-----------------------------------------------------------------------------------*/
    /* Compare Properties
     /*-----------------------------------------------------------------------------------*/
    var compare_properties_number = $( '.compare-properties .compare-carousel > div' ).length;
    if ( compare_properties_number != 0 ) {
        $( '.compare-properties' ).slideDown( 200 );
    }

    /*-----------------------------------------------------------------------------------*/
    /* Add to compare
     /*-----------------------------------------------------------------------------------*/
    $( document ).on( 'click', 'a.add-to-compare', function(e) {
        e.preventDefault();

        var slides_number = $( '.compare-carousel .compare-carousel-slide' ).length;
        if ( slides_number > 3 ) {
            var remove_last_check = 1;
            $( '.compare-carousel .compare-carousel-slide:nth-child(1) a.compare-remove' ).trigger( "click", [ $( this ), remove_last_check ] );
        } else {
            var plus            = $( this ).find( 'i' );
            var compare_target  = $( this ).parents( '.add-to-compare-span' ).find( '.compare_target' );
            var compare_link    = $( this );
            var compare_output  = $( this ).parents( '.add-to-compare-span' ).find( '.compare_output' );

            plus.addClass( 'fa-spin' );

            var add_compare_request = $.ajax({
                url         : $( compare_link ).attr('href'),
                type        : "POST",
                data        : {
                    property_id     : $( compare_link ).data( 'property-id' ),
                    action          : "inspiry_add_to_compare"
                },
                dataType    : "json"
            });

            add_compare_request.done( function( response ) {
                plus.removeClass( 'fa-spin' );
                if( response.success ) {
                    $( compare_link ).hide( 0, function() {
                        $( compare_target ).html( response.message );
                        $( compare_output ).delay( 200 ).show();
                    });
                    $( '.compare-carousel' ).append(
                        '<div class="compare-carousel-slide"><div class="compare-slide-img"><img src="' + response.img.replace(/^http:\/\//i, '//') + '"></div><a class="compare-remove" data-property-id="' + response.property_id + '" href="' + response.ajaxURL + '" width="' + response.img_width + '" height="' + response.img_height + '"><i class="fa fa-close"></i></a></div>'
                    );
                    var compare_properties_number = $( '.compare-properties .compare-carousel > div' ).length;
                    if ( compare_properties_number == 1 ) {
                        $( '.compare-properties' ).slideDown();
                    }
                } else {
                    compare_output.text( response.message );
                }
            });

            add_compare_request.fail( function( jqXHR, textStatus ) {
                compare_output.text( "Request failed: " + textStatus );
            });
        }
    });

    /*-----------------------------------------------------------------------------------*/
    /* Remove from compare
     /*-----------------------------------------------------------------------------------*/
    $( document ).on( 'click', 'a.compare-remove', function( event, add_compare_target, remove_last ) {
        event.preventDefault();
        var current_link    = $( this );
        var cross           = $( this ).find( 'i' );
        var plus            = $( add_compare_target ).find( 'i' );

        cross.addClass( 'fa-spin' );
        plus.addClass( 'fa-spin' );

        $.when(
            $.ajax({
                    url     : current_link.attr( 'href' ),
                    type    : "POST",
                    data    : {
                        property_id : current_link.data( 'property-id' ),
                        action      : "inspiry_remove_from_compare"
                    },
                    dataType        : "json"
                })

                .done( function( response ) {
                    cross.removeClass( 'fa-spin' );
                    if( response.success ) {
                        current_link.parents( 'div.compare-carousel-slide' ).remove();
                        var property_item = $( 'div.add-to-compare-span *[data-property-id="' + response.property_id + '"]' ).parents( '.add-to-compare-span' );
                        property_item.find( 'div.compare_output' ).hide().removeClass('show');
                        $( 'div.add-to-compare-span *[data-property-id="' + response.property_id + '"]' ).removeClass( 'hide' ).delay(200).show();
                        current_link.parents('.span2.compare-properties-column').remove();
                       if($('.compare-template').find('.compare-properties-column').length == 0){
                           $('.compare-template').remove();
                           $( '.compare-empty-message').removeClass('hide');
                       }
                        var compare_properties_number = $( '.compare-properties .compare-carousel > div' ).length;
                        if ( compare_properties_number == 0 ) {
                            $( '.compare-properties' ).slideUp();
                        }
                    }
                })

                .fail( function( jqXHR, textStatus ) {
                    compare_output.text( "Request failed: " + textStatus );
                })
            )

            .then( function( response ) {
                if ( remove_last ) {
                    var compare_target  = $( add_compare_target ).parents( '.add-to-compare-span' ).find( '.compare_target' );
                    var compare_link    = $( add_compare_target );
                    var compare_output  = $( add_compare_target ).parents( '.add-to-compare-span' ).find( '.compare_output' );

                    var add_compare_request = $.ajax({
                        url         : $( compare_link ).attr('href'),
                        type        : "POST",
                        data        : {
                            property_id     : $( compare_link ).data( 'property-id' ),
                            action          : "inspiry_add_to_compare"
                        },
                        dataType    : "json"
                    });

                    add_compare_request.done( function( response ) {
                        plus.removeClass( 'fa-spin' );
                        if( response.success ) {
                            $( compare_link ).hide( 0, function() {
                                $( compare_target ).html( response.message );
                                $( compare_output ).delay( 200 ).show();
                            });
                            $( '.compare-carousel' ).append(
                                '<div class="compare-carousel-slide"><div class="compare-slide-img"><img src="' + response.img.replace(/^http:\/\//i, '//') + '"></div><a class="compare-remove" data-property-id="' + response.property_id + '" href="' + response.ajaxURL + '" width="' + response.img_width + '" height="' + response.img_height + '"><i class="fa fa-close"></i></a></div>'
                            );
                        } else {
                            compare_output.text( response.message );
                        }
                    });
                };
            } );
    });

//    make equal height container for property compare thumbnails
    $(function(){
        var highestBox = 0;
        $('.compare-template .property-thumbnail', this).each(function(){
            if($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        });
        $('.compare-template .property-thumbnail',this).height(highestBox);
    });


    //position the thumbnails on property compare buttons
        $(window).on('scroll', function () {
            if($(window).width() > 991) {

            var scroll = $(window).scrollTop();
            var headerBanner = $('.page-head').height();
            var headerHeight = $('.site-header ').height();
            var adminBarHeight = $('wpadminbar').height();
            var thumbnailHeight = $('.property-thumbnail').outerHeight();
            var totalHeight = headerHeight + adminBarHeight + headerBanner + 50;
            //>=, not <=
            if (scroll >= totalHeight) {
                //clearHeader, not clearheader - caps H
                $(".wrapper-template-compare-contents .property-thumbnail").addClass("compare-thumbnail-fix");
                $(".wrapper-cells").css('margin-top', thumbnailHeight + 'px');
            } else {
                $(".wrapper-template-compare-contents .property-thumbnail").removeClass("compare-thumbnail-fix");
                $(".wrapper-cells").css('margin-top', '0');
            }
        }

    });

    /*-----------------------------------------------------------------*/
    /* Currency Switcher
     /*-----------------------------------------------------------------*/
    $(function ()
    {
        var currencySwitcherList = $('#currency-switcher-list');
        if (currencySwitcherList.length > 0) {     // if currency switcher exists

            var currencySwitcherForm = $('#currency-switcher-form');
            var currencySwitcherOptions = {
                success: function (ajax_response, statusText, xhr, $form) {
                    var response = $.parseJSON(ajax_response);
                    if (response.success) {
                        window.location.reload();
                    }
                }
            };

            $('#currency-switcher-list > li').on('click', function (event) {
                event.stopPropagation();
                currencySwitcherList.slideUp(200);

                // get selected currency code
                var selectedCurrencyCode = $(this).data('currency-code');

                if (selectedCurrencyCode) {
                    $('#selected-currency').html(selectedCurrencyCode);
                    $('#switch-to-currency').val(selectedCurrencyCode);           // set new currency code
                    currencySwitcherForm.ajaxSubmit(currencySwitcherOptions);    // submit ajax form to update currency code cookie
                }
            });

            $('.switcher__currency #currency-switcher').on('click', function (event) {
                $(this).find("ul").slideToggle(200);
                //currencySwitcherList.slideToggle(200);
                event.stopPropagation();
            });

            $('html').on('click', function () {
                currencySwitcherList.slideUp(100);
            });

        }
    });


//Tooltip for property compare button
    $( function() {
        $( '.add-to-compare-span' ).tooltip({
            position: {
                my: "center bottom-20",
                at: "center top",
                using: function( position, feedback ) {
                    $( this ).css( position );
                    $( "<div>" )
                        .addClass( "arrow" )
                        .addClass( feedback.vertical )
                        .addClass( feedback.horizontal )
                        .appendTo( this );
                }
            }
        });


    } );


// Google Map Half Map


    function halfMapSize(){
        var windowWidth = $(window).width();
        var halfWidth = (windowWidth / 2) - 15;
        var contentHeight = $('#content-wrapper').height();
        if($(window).width() > 991) {
            $('.properties-with-half-map .map-inner').width(halfWidth).height(contentHeight);
            $('.properties-with-half-map #listing-map').height(contentHeight);
        }else {
            $('.properties-with-half-map .map-inner').width("auto").height("500");
            $('.properties-with-half-map #listing-map').height("500");
        }
    }
//    Move Map Container For Small Devices

    function mapForSmallDevices() {

        if ($(window).width() <= 991) {
            if (!$('.map-container-sm').hasClass('small-device-map')) {
                $(".map-container").detach().appendTo('.map-container-sm');
                $('.map-container-sm').addClass("small-device-map");
                $('.map-container-md').removeClass("large-device-map");
            }
        } else if ($(window).width() > 991) {
            if (!$('.map-container-md').hasClass('large-device-map')) {
                $(".map-container").detach().appendTo('.map-container-md');
                $('.map-container-md').addClass("large-device-map");
                $('.map-container-sm').removeClass("small-device-map");
            }
        }
    }

    $window.on('load resize', function () {
        halfMapSize();
        mapForSmallDevices();

    });


    jQuery(document).ready(function($){
        
        $('.advancesearchLink').click(function(){
            $(window).scrollTop(0);
            
            $('.advanceSearchBox').show();
            $("body").css("overflow", "hidden");
        });
       
        $('.showmob1').click(function(){
            $('#hide123').toggle(300);
        });

        $('.showmob2').click(function(){
            $('#hide12').toggle(300);
        });

        

        $('.advancesearchClose').click(function(){
            $('.advanceSearchBox').hide();
            $("body").css("overflow", "scroll");
        });                  
    });

     jQuery(document).on("click",".features-checkboxes-wrapper li",function () {
            if($(document).width() <= 576){
            if($(".advance-search-form input:checkbox:checked").length > 0){
                if(!$(".extra-search-fields").hasClass('seach11')){
                    $(".extra-search-fields").append('<div class="searchinmobile"><input type="submit" value="Search" class="form-submit-btn searchbtn widthandhide" style="width:100% !important" /></div>');
                    $(".extra-search-fields").addClass('seach11');
                }
            }
            else
            {
                $(".extra-search-fields").removeClass('seach11');
                $(".searchinmobile").remove();
            }
        }
    });

    jQuery(document).on("click",".ms-options li",function () {


        var arr = [];
        var cb = jQuery(this).find(":checkbox")[0];
        if(cb.checked){
            jQuery(this).parent().parent().parent().parent().parent().parent().find('.clear_btn').css({"visibility": "visible"});
        }

        jQuery('.ms-options input').each(function(){
            if(this.checked){
                arr.push(this.value);
            }
        });

        var country = jQuery("#selected_country").val();
        var checked_length = jQuery('.ms-options input:checked').size();

        if(checked_length == 0){
            jQuery(this).parent().parent().parent().parent().parent().parent().find('.clear_btn').css({"visibility": "hidden"});
        }

        if(checked_length > 0){
            eraseCookie('lifestyle');
            setCookie('lifestyle',arr,7);
        }
    });

//    jQuery('select').on('select2:select', function (e) {
//        var selval = e.params.data.id;
//        var arr = [];
//        if(selval == ""){
//            jQuery(this).parent().parent().parent().find('.clear_btn').css({"visibility": "hidden"});
//        }
//        else
//        {
//            jQuery(this).parent().parent().parent().find('.clear_btn').css({"visibility": "visible"});
//            jQuery("#selected_country").val(selval);
//            jQuery('.ms-options input').each(function(){
//                if(this.checked){
//                    arr.push(this.value);
//                }
//            });
//            eraseCookie('country');
//            setCookie('country',selval,7);
//            var countryCookie = getCookie('country');
//            jQuery('.map-options select').val(countryCookie); 
//            jQuery('.map-options select').trigger('change');
//        }
//    });

    // eraseCookie('country');
    // eraseCookie('lifestyle');
    // eraseCookie('adv_search_data');

    jQuery(document).on("click",".countrySearch",function() {
     
        var country = jQuery("#selected_country").val();
        
     if(country != 'Worldwide'){
       
// var geocoder = new google.maps.Geocoder();
// geocoder.geocode( { 'address': country}, function(results, status) {
// if (status == google.maps.GeocoderStatus.OK) {
// map.setCenter(results[0].geometry.location);
// map.fitBounds(results[0].geometry.viewport);
// }
// });
    	 centerOnJurisdiction(map, country);
        var template = `
    <div class = "col-md-12" style="text-align: center;">
    <a name="properties_list">
        <div id="ajax-load-more" class="ajax-load-more-wrap blue alm-0 alm-loading" data-alm-id="0" data-canonical-url="/news/" data-slug="home" data-post-id="0" data-localized="ajax_load_more_vars">
            <div class="alm-btn-wrap" style="visibility: visible;">
                <button class="alm-load-more-btn more loading" rel="next">Searching Properties</button>
            </div>
            <div class="alm-no-results" style="display: none;">No Property Found</div>
        </div>

    </a> </div>`;
        $("#aftersearchshow").html(template);
        
       console.log("Loading more (1296)...");
   
        jQuery.ajax({
            method:'POST',
            url: '/wp-admin/admin-ajax.php',
            data:{action: 'folder_contents', country:country},
            success: function(res) {
            	if ((document.getElementById("countryname") != null) && (document.getElementById("countryname").value === country)) {
            		$('.countrySearch').css({"visibility": "hidden"});
            	}
            	$("#aftersearchshow").html(res);
            }
        });
    }else{
    	console.log("Loading more (1310)...");
            var country = '';
            jQuery.ajax({
                method:'POST',
                url: '/wp-admin/admin-ajax.php',
                data:{action: 'folder_contents',country:country},
                success: function(res) {
            		if ((document.getElementById("countryname") != null) && (document.getElementById("countryname").value === country)) {
                		$('.countrySearch').css({"visibility": "hidden"});
                	}
                	$("#aftersearchshow").html(res);
                }
             });
             
            $.removeCookie("country");
            $('.clear_btn').css({"visibility": "hidden"});
            mapWorldWide();
    }
        
    });


    jQuery(document).on("click",".loadmore",function() {

        jQuery( ".loadmorediv" ).remove();
        jQuery( ".loadmore" ).remove();
        var template = `
        <div class = "col-md-12 searchingbtn" style="text-align: center;">
        <a name="properties_list">
        <div id="ajax-load-more" class="ajax-load-more-wrap blue alm-0 alm-loading" data-alm-id="0" data-canonical-url="/news/" data-slug="home" data-post-id="0" data-localized="ajax_load_more_vars">
        <div class="alm-btn-wrap" style="visibility: visible;">
        <button class="alm-load-more-btn more loading" rel="next">Searching Properties</button>
        </div>
        <div class="alm-no-results" style="display: none;">No Property Found</div>
        </div>

        </a> </div>`;
         $("#aftersearchshow").append(template);
        var country = jQuery(this).data("country");
        var state = jQuery(this).data("state");
        var city = jQuery(this).data("city");
        var sector = jQuery(this).data("sector");
        var offset = jQuery(this).data("offset");
        var iteration = jQuery(this).data("iteration");
        var catid = jQuery(this).data("catid");
        var propertytype = jQuery(this).data("propertytype");
        var statustype = jQuery(this).data("statustype");
        var json = jQuery(this).data("json");
        var beds = jQuery(this).data("beds");
        var baths = jQuery(this).data("baths");
        var catid = jQuery(this).data("catid");
        var min_price = jQuery(this).data("min_price");
     	var max_price = jQuery(this).data("max_price");
     	var min_area = jQuery(this).data("min_area");
     	var max_area = jQuery(this).data("max_area");
     	var total = jQuery(this).data("total");
     	var showingtotal = jQuery(this).data("showingtotal");
     	var aftersearchshowid = jQuery(this).data("aftersearchshowid");
     	var addclassthumbnail = jQuery(this).data("addclassthumbnail");
     	var addclassdescription = jQuery(this).data("addclassdescription");
     	var properties_id_json = jQuery(this).data("properties_id_json");
     	var sortasc = jQuery(this).data("sortasc");
     	var sortdesc = jQuery(this).data("sortdesc");
     	if (aftersearchshowid == undefined) {
     		aftersearchshowid = 'aftersearchshow';
     	}
        
        console.log("Loading more (1354)...");
        jQuery.ajax({
            method:'POST',
            url: '/wp-admin/admin-ajax.php',
            data:{	action: 'folder_contents', 
            		country: country,
            		state, state, 
            		city: city,
            		sector: sector,
            		offset: offset, 
            		iteration: iteration, 
            		catid: catid, 
            		propertytype: propertytype, 
            		statustype: statustype,
            		json: json,
            		beds: beds,
            		baths: baths,
            		min_price: min_price,
            		max_price: max_price,
            		min_area: min_area,
            		max_area: max_area,
            		aftersearchshowid: aftersearchshowid,
            		total: total,
            		showingtotal: showingtotal,
            		addclassthumbnail: addclassthumbnail,
            		addclassdescription: addclassdescription,
            		properties_id_json: properties_id_json,
            		sortasc: sortasc,
            		sortdesc: sortdesc },
            success: function(res) {
               jQuery(".searchingbtn").remove();
               $("#" + aftersearchshowid).append(res);
            }
        });
        
        
    });


//    jQuery(document).on("click",".clear_btn",function() {
//        
//        var countryname = (document.getElementById("countryname") != null)?document.getElementById("countryname").value:null; 
//        
//        if(countryname != 'Worldwide'){
//
//        var country = countryname;
//        // var geocoder = new google.maps.Geocoder();
//        jQuery('.map-options select').parent().parent().parent().find('.clear_btn').css({"visibility": "hidden"});
//        centerOnJurisdiction(map, country);
////        geocoder.geocode( { 'address': countryname}, function(results, status) {
////            if (status == google.maps.GeocoderStatus.OK) {
////                map.setCenter(results[0].geometry.location);
////                map.fitBounds(results[0].geometry.viewport);
////            }
////        });
//        
//        var template = `
//        <div class = "col-md-12 searchingbtn" style="text-align: center;"><br/><br/>
//       
//        <div id="ajax-load-more_1" class="ajax-load-more-wrap blue alm-0 alm-loading" data-alm-id="0" data-canonical-url="/news/" data-slug="home" data-post-id="0" data-localized="ajax_load_more_vars">
//        <div class="alm-btn-wrap" style="visibility: visible;">
//        <button class="alm-load-more-btn more loading" rel="next">Searching Jurisdiction</button>
//        </div>
//        <div class="alm-no-results" style="display: none;">No Property Found</div>
//        </div> </div>`;
//        
//        $("#load_table_home").html(template);
//        jQuery.ajax({
//            method:'POST',
//            url: '/wp-admin/admin-ajax.php',
//            data:{action: 'load_table_short_code'},
//            success: function(res) {
//                
//                 jQuery('#load_table_home').html(res);
//                  jQuery('#countryheadingshow').html(country); 
//                    
//                 jQuery('.table-responsive-stack').DataTable( {    
//                     
//                    autoWidth: false, 
//                    hideEmptyCols: true,
//                    bPaginate: false,
//                    searching: false,
//                    info:   false
//                    
//                } );
//                
//                jQuery( '.table-responsive-stack' ).each(function() {
//                     var thCount = $(this).find("th").length; 
//                     var rowGrow = 80 / thCount + '%';
//                     //console.log(rowGrow);
//                     jQuery(this).find("th, td").css('flex-basis', rowGrow);   
//                });
//                
//                jQuery('.table-responsive-stack tbody tr').on('click', function () {
//                    href = jQuery(this).data('href');
//                    if(href !== "" && href !== undefined ){
//                          window.location.href = href;
//                    }
//                });
//                
//                    jQuery("#purchase").change(function() {
//                        if(this.checked) {
//                            
//                            $("#purchases_table").slideUp();
//                        }else{
//                            $("#purchases_table").slideDown();
//                        }
//                    });
//                    
//                    
//                    jQuery("#rent").change(function() {
//                        if(this.checked) {
//                            
//                            $("#rent_table").slideUp();
//                        }else{
//                            $("#rent_table").slideDown();
//                        }
//                    });
//                    
//                   
//                    jQuery("#lease").change(function() {
//                        if(this.checked) {
//                            
//                            $("#lease_table").slideUp();
//                        }else{
//                            $("#lease_table").slideDown();
//                        }
//                    });
//    
//                 
//            }
//            
//        });
//        
//        
//         var template = `
//            <div class = "col-md-12" style="text-align: center;">
//            <a name="properties_list">
//                <div id="ajax-load-more" class="ajax-load-more-wrap blue alm-0 alm-loading" data-alm-id="0" data-canonical-url="/news/" data-slug="home" data-post-id="0" data-localized="ajax_load_more_vars">
//                    <div class="alm-btn-wrap" style="visibility: visible;">
//                        <button class="alm-load-more-btn more loading" rel="next">Searching Properties</button>
//                    </div>
//                    <div class="alm-no-results" style="display: none;">No Property Found</div>
//                </div>
//        
//            </a> </div>`;
//        $("#aftersearchshow").html(template);
//        
//        console.log("Loading more (1480)...");
//        
//        jQuery.ajax({
//            method:'POST',
//            url: '/wp-admin/admin-ajax.php',
//            data:{action: 'folder_contents',country:country},
//            success: function(res) {
//            	if ((document.getElementById("countryname") != null) && (document.getElementById("countryname").value === country)) {
//            		$('.countrySearch').css({"visibility": "hidden"});
//            	}
//              $("#aftersearchshow").html(res);
//            }
//        });
//        
//        
//         var countryCookie = getCookie('country');
//         jQuery('#country_listing1').val(countryCookie);
//         jQuery('#country_listing1').trigger('change');
//       
//        }else{
//            
//             var template = `
//        <div class = "col-md-12 searchingbtn" style="text-align: center;"><br/><br/>
//       
//        <div id="ajax-load-more_1" class="ajax-load-more-wrap blue alm-0 alm-loading" data-alm-id="0" data-canonical-url="/news/" data-slug="home" data-post-id="0" data-localized="ajax_load_more_vars">
//        <div class="alm-btn-wrap" style="visibility: visible;">
//        <button class="alm-load-more-btn more loading" rel="next">Searching Jurisdiction</button>
//        </div>
//        <div class="alm-no-results" style="display: none;">No Property Found/div>
//        </div> </div>`;
//        
//        $("#load_table_home").html(template);
//        jQuery.ajax({
//            method:'POST',
//            url: '/wp-admin/admin-ajax.php',
//            data:{action: 'load_table_short_code'},
//            success: function(res) {
//                
//                 jQuery('#load_table_home').html(res);
//                  jQuery('#countryheadingshow').html(country); 
//                    
//                 jQuery('.table-responsive-stack').DataTable( {    
//                     
//                    autoWidth: false, 
//                    hideEmptyCols: true,
//                    bPaginate: false,
//                    searching: false,
//                    info:   false
//                    
//                } );
//                
//                jQuery( '.table-responsive-stack' ).each(function() {
//                     var thCount = $(this).find("th").length; 
//                     var rowGrow = 80 / thCount + '%';
//                     //console.log(rowGrow);
//                     jQuery(this).find("th, td").css('flex-basis', rowGrow);   
//                });
//                
//                jQuery('.table-responsive-stack tbody tr').on('click', function () {
//                    href = jQuery(this).data('href');
//                    if(href !== "" && href !== undefined ){
//                          window.location.href = href;
//                    }
//                });
//                
//                    jQuery("#purchase").change(function() {
//                        if(this.checked) {
//                            
//                            $("#purchases_table").slideUp();
//                        }else{
//                            $("#purchases_table").slideDown();
//                        }
//                    });
//                    
//                    
//                    jQuery("#rent").change(function() {
//                        if(this.checked) {
//                            
//                            $("#rent_table").slideUp();
//                        }else{
//                            $("#rent_table").slideDown();
//                        }
//                    });
//                    
//                   
//                    jQuery("#lease").change(function() {
//                        if(this.checked) {
//                            
//                            $("#lease_table").slideUp();
//                        }else{
//                            $("#lease_table").slideDown();
//                        }
//                    });
//    
//                 
//            }
//            
//        });
//        
//        
//         jQuery('#country_listing1').val('Worldwide');
//         jQuery('#country_listing1').trigger('change');
//            
//            console.log("Loading more (1583)...");
//            
//            var country = '';
//            jQuery.ajax({
//                method:'POST',
//                url: '/wp-admin/admin-ajax.php',
//                data:{action: 'folder_contents',country:country},
//                success: function(res) {
//                	if ((document.getElementById("countryname") != null) && (document.getElementById("countryname").value === country)) {
//                		$('.countrySearch').css({"visibility": "hidden"});
//                	}
//                  $("#aftersearchshow").html(res);
//                  jQuery('#countryheadingshow').html(''); 
//                }
//             });
//            $.removeCookie("country");
//            $('.clear_btn').css({"visibility": "hidden"});
//            jQuery('.map-options select').parent().parent().parent().find('.clear_btn').css({"visibility": "hidden"});
//            mapWorldWide();
//            
//            
//         
//        }
//    });
    
    function mapWorldWide() {
        // initializePropertiesMap();
   	 	console.log("Calling centerOnJurisdiction for Worldwide");
    	centerOnJurisdiction(map, 'Worldwide');
    }


    jQuery(document).on("submit",".advance-search-form",function() {
        var values = $(this).serialize();
       
        setCookie('adv_search_data',values,7);
    });


    function setCookie(name,value,days) {
        console.log("*** setting cookie '" + name + "' at " + value);
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            expires = "; expires=" + date.toUTCString();
        }
            document.cookie = name + "=" + (value || "")  + expires + "; path=/";
    }
                
    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
             while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {   
        document.cookie = name+'=; Max-Age=-99999999;';  
    }

function get(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

    var adv_search_data = getCookie('adv_search_data');
    if(adv_search_data){
        var res = adv_search_data.split("&");
        jQuery.each( res, function( key, value ) {
          
          var res1 = value.split("=");
          //console.log(res1);
          if(res1[0] == 'location'){
            if(res1[1] != 'any'){
                jQuery('.property-location select').val(res1[1]); 
                jQuery('.property-location select').trigger('change');  
            } 
          }
          else if(res1[0] == 'type'){
            if(res1[1] != 'any'){
                jQuery('.property-type select').val(res1[1]); 
                jQuery('.property-type select').trigger('change');  
            } 
          }
          else if(res1[0] == 'bathrooms'){
            if(res1[1] != 'any'){
                jQuery('.property-bathrooms select').val(res1[1]); 
                jQuery('.property-bathrooms select').trigger('change');  
            } 
          }
          else if(res1[0] == 'bedrooms'){
            if(res1[1] != 'any'){
                jQuery('.property-bedrooms select').val(res1[1]); 
                jQuery('.property-bedrooms select').trigger('change');  
            } 
          }
          else if(res1[0] == 'min-price'){
            if(res1[1] != 'any'){
                jQuery('.property-min-price select').val(res1[1]); 
                jQuery('.property-min-price select').trigger('change');  
            } 
          }
          else if(res1[0] == 'max-price'){
            if(res1[1] != 'any'){
                jQuery('.property-max-price select').val(res1[1]); 
                jQuery('.property-max-price select').trigger('change');  
            } 
          }

          else if(res1[0] == 'status'){
            if(res1[1] != 'any'){
                jQuery('.property-status select').val(res1[1]); 
                jQuery('.property-status select').trigger('change');  
            } 
          }
          else if(res1[0].startsWith('features')){
            jQuery( "#feature-"+res1[1]).prop( "checked", true );
            jQuery(".advsearch").addClass('is-expand123');
            jQuery(".advsearch").addClass('class111');
            jQuery(".advsearch").html(`<i class="fa fa-minus" aria-hidden="true"></i>`);
            jQuery(".tooltiptext_1").css({"display": "none"});
            jQuery(".features-checkboxes-wrapper").css({"display": "block"});
          }
          else{
            // alert(res1[1]);
            var resofkeywork =  res1[1].replace('+',' ');
            jQuery('input[name="'+res1[0]+'"]').val(resofkeywork);
            
            var checkofval = get('keyword');
            
            if (typeof checkofval === 'undefined') {
               
                jQuery('input[name="'+res1[0]+'"]').val('');
            }
           
          }
      });
    }
        
 //$('#hideosearchcookies').val('');
    if (document.getElementById("country_listing1") != null) {
	    var countryname = document.getElementById("country_listing1").value; 
	    if(countryname == ''){
	         $('.loadmore').click();
	    }
    }

})(jQuery);



