if (typeof propertiesMapData !== "undefined") {	
    google.maps.event.addDomListener(window, 'load', initializePropertiesMap);
}

var mapOptions = {
    zoom: 12,
    minZoom: 2,
    maxZoom: 16,
    scrollwheel: false
};

const KMARKERSTOINSERT = 100;

const KHOUSEICON = 0;
const KAPTICON = 1;
const KSHOPICON = 2;
const KOFFICEICON = 3;
const KVILLAICON = 4;
const KCONDOICON = 5;
const KBUILDINGICON = 6;
const KLARGECLUSTERICON = 7;
const KSMALLCLUSTERICON = 8;
const KLOADINGICON = 9;
const KSPIDEFYICON = 10;
const KLANDICON = 11;
const KSMALLBLUECLUSTER = 12;
const KLARGEBLUECLUSTER = 13;
const KSMALLORANGECLUSTER = 14;
const KLARGEORANGECLUSTER = 15;
const KSMALLGREENCLUSTER = 16;
const KLARGEGREENCLUSTER = 17;
const KSMALLREDCLUSTER = 18;
const KLARGEREDCLUSTER = 19;
const KSMALLYELLOWCLUSTER = 20;
const KLARGEYELLOWCLUSTER = 21;

const KLARGECLUSTERSIZE = 64;
const KSMALLCLUSTERSIZE = 48;

const KMARKERXANCHOR = 21;
const KMARKERYANCHOR = 49;
const KMARKERXLABEL = 21;
const KMARKERYLABEL = 38;

const KMAXCLUSTERINGZOOMLEVEL = 1;

const KMAPMESSAGESTYLE = "-moz-border-radius:10px; -webkit-border-radius:10px; border-radius:10px; opacity:0.8;";

var lastCalculatedBounds = null;
var lastCalculatedZoom = null;
var basiclogs = true;
var debugdisplaywithconsole = false;
var debugclickswithconsole = false;
var debugspidefywithconsole = false;
var debugservletinteractions = false;
var useLabels = true;
var openedMarkerInfoBox = null;
var map = null;
var markersHasOMSCluster = [];
var bounds = new google.maps.LatLngBounds();
// Loop to generate marker and infowindow based on properties array
var markers = [];
var fetchedProperties = [];
var markersAwaitingPropertyDetails = [];
var isClustering = false;
var lastZoom = null;
var lastSpidefyZoom = null;
var lastMarkersZoom = null;
var lastMarkersBounds = null;
var Http = null;
var curHttpProcessingTimeout = null;
var msgStack = [];
var oms = null;
var requestedReset = false;
var screendevice = null;
var dIdleListener = null;
var nextIdleData = null;
var replacementImageShown = false;
var imageIcons = [{url: "/wp-content/themes/inspiry-real-places/images/map/housemarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/apartmentmarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/shopmarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/officemarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/villamarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/condomarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/buildingmarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    null,
    null,
    {url: "/wp-content/themes/inspiry-real-places/images/map/loading.gif",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(0, 0),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/housemarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/landmarker.png",
    size: new google.maps.Size(83, 113),
    scaledSize: new google.maps.Size(42, 57),
    labelOrigin: new google.maps.Point(KMARKERXLABEL, KMARKERYLABEL),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KMARKERXANCHOR, KMARKERYANCHOR)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/purple-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KSMALLCLUSTERSIZE, KSMALLCLUSTERSIZE),
    labelOrigin: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/purple-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KLARGECLUSTERSIZE, KLARGECLUSTERSIZE),
    labelOrigin: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/orange-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KSMALLCLUSTERSIZE, KSMALLCLUSTERSIZE),
    labelOrigin: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/orange-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KLARGECLUSTERSIZE, KLARGECLUSTERSIZE),
    labelOrigin: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/green-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KSMALLCLUSTERSIZE, KSMALLCLUSTERSIZE),
    labelOrigin: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/green-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KLARGECLUSTERSIZE, KLARGECLUSTERSIZE),
    labelOrigin: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/red-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KSMALLCLUSTERSIZE, KSMALLCLUSTERSIZE),
    labelOrigin: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/red-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KLARGECLUSTERSIZE, KLARGECLUSTERSIZE),
    labelOrigin: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/yellow-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KSMALLCLUSTERSIZE, KSMALLCLUSTERSIZE),
    labelOrigin: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KSMALLCLUSTERSIZE/2, KSMALLCLUSTERSIZE/2)},
    {url: "/wp-content/themes/inspiry-real-places/images/map/yellow-96.png",
    size: new google.maps.Size(96, 96),
    scaledSize: new google.maps.Size(KLARGECLUSTERSIZE, KLARGECLUSTERSIZE),
    labelOrigin: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(KLARGECLUSTERSIZE/2, KLARGECLUSTERSIZE/2)}];
var myTitle = document.createElement('h1');
myTitle.innerHTML = "<span style='display:none; " + KMAPMESSAGESTYLE + "'>Calculating points...</span>";

if (!replacementImageShown) {
	document.getElementById("map-image").style.width = "100%";
	document.getElementById("map-image").style.position = "absolute";
	document.getElementById("map-image").style.zIndex  = "11";
	var width = jQuery(window).width();
	if (width < 650){
		document.getElementById("map-image").style.backgroundImage = "url(/mediacode/images/map/SmallDefaultMap.jpg)";
		document.getElementById("map-image").style.height  = "382px";
	};
	if (width > 650 && width < 1190){
		document.getElementById("map-image").style.backgroundImage = "url(/mediacode/images/map/MediumDefaultMap.jpg)";
		document.getElementById("map-image").style.height  = "500px";
	};
	if (width > 1190){
		document.getElementById("map-image").style.backgroundImage = "url(/mediacode/images/map/LargeDefaultMap.jpg)";
		document.getElementById("map-image").style.height  = "500px";
	};
	replacementImageShown = true;
}

function clickOrigin(evt){
    var target = evt.target;
    var tag = [];
    tag.tagType = target.tagName.toLowerCase();
    tag.tagClass = target.className.split(' ');
    tag.id = target.id;
    tag.parent = target.parentNode;
    return tag;
}

function dispatchClick(evt) {
	if ((openedMarkerInfoBox != null) && (openedMarkerInfoBox.isIBOpened === true)) {
		var tagsToIdentify = ['a'];
		elem = clickOrigin(evt);
		for (i=0; i < tagsToIdentify.length; i++) {
	        if (elem.tagType == tagsToIdentify[i]) {
	            console.log('You\'ve clicked a monitored tag (' + elem.id + ', in this case).');
	            break;
	        }
	    }
	} else {
		if (debugclickswithconsole) console.log("dispatchClick called, but not handled: " + JSON.stringify(evt) + ", " + JSON.stringify(openedMarkerInfoBox));
	}
}

function populateListings() {
	if (debugclickswithconsole) console.log("populateListings called");
	if ((openedMarkerInfoBox != null) && (openedMarkerInfoBox.clusterIdsCache != null)) {
		var bounds = map.getBounds();
		if ((openedMarkerInfoBox.west !== undefined) && (openedMarkerInfoBox.east !== undefined) && (openedMarkerInfoBox.north !== undefined) && (openedMarkerInfoBox.south !== undefined)) {
			bounds = new google.maps.LatLngBounds(new google.maps.LatLng(openedMarkerInfoBox.south, openedMarkerInfoBox.west), new google.maps.LatLng(openedMarkerInfoBox.north, openedMarkerInfoBox.east));
		}
		var link = '/properties-search/?file=' + encodeURI(openedMarkerInfoBox.clusterIdsCache.ref + "&showmap=" + ((openedMarkerInfoBox.clusterCount > 300)?"false":"true") + "&showsearch=false" + ((bounds !== null)?("&west=" + bounds.getSouthWest().lng() + "&east=" + bounds.getNorthEast().lng() + "&north=" + bounds.getNorthEast().lat() + "&south=" + bounds.getSouthWest().lat()):"") + "#properties_list");
		window.location.href = link;
		if (debugclickswithconsole) console.log("populateListings invoked " + link);
	} else {
		if (debugclickswithconsole) console.log("populateListings did not perform any action");
	}
	if (openedMarkerInfoBox != null) {
	    openedMarkerInfoBox.infoBox.close();
	    openedMarkerInfoBox.isIBOpened = false;
	    if (useLabels) openedMarkerInfoBox.setLabel(calculateLabel(map, openedMarkerInfoBox));
        openedMarkerInfoBox = null;
	}
}

function zoomIntoCluster(e) {
	var cluster = openedMarkerInfoBox;
	if (debugclickswithconsole) console.log("zoomIntoCluster called: " + JSON.stringify(e));
	if (openedMarkerInfoBox != null) {
	    openedMarkerInfoBox.infoBox.close();
	    openedMarkerInfoBox.isIBOpened = false;
	    if (useLabels) openedMarkerInfoBox.setLabel(calculateLabel(map, openedMarkerInfoBox));
        openedMarkerInfoBox = null;
	}
	google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
		map.setZoom(map.getZoom() + 2);
	});
	if ((cluster != null) || (e !== undefined)) map.setCenter((e !== undefined)?e.latLng:{lat: cluster.getPosition().lat(), lng: cluster.getPosition().lng()});
}

function idleListenerFunc() {
	if ((lastCalculatedBounds == null) && (lastCalculatedZoom == null)) {
		var country = getCookie('country');
		if (country == null) {
			country = 'Worldwide';
		}
		if (propertiesMapData.properties == undefined) {
			if (basiclogs) console.log('First map idle, recentering on country ' + country);
			centerOnJurisdiction(map, country);
			setTimeout(recalculateClusters);
		} else if (propertiesMapData.properties_bounds != undefined) {
			if (basiclogs) console.log('First map idle, centering on ' + propertiesMapData.properties_bounds);
			map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(propertiesMapData.properties_bounds.south, propertiesMapData.properties_bounds.west), new google.maps.LatLng(propertiesMapData.properties_bounds.north, propertiesMapData.properties_bounds.east)));			
			setTimeout(recalculateClusters);
		} else {
			if (basiclogs) console.log('First map idle, do not know what to do');
		}
	} else {
		setTimeout(recalculateClusters);
	}
}

function initializePropertiesMap() {
	
	if (typeof propertiesMapData.zoom === 'string' || propertiesMapData.zoom instanceof String) propertiesMapData.zoom = jQuery.parseJSON(propertiesMapData.zoom);
	if (typeof propertiesMapData.countries === 'string' || propertiesMapData.countries instanceof String) propertiesMapData.countries = jQuery.parseJSON(propertiesMapData.countries);

	if (basiclogs) console.log('initializePropertiesMap');
	
	if (debugspidefywithconsole) console.log('Loading Spidefy javascript');
	loadScript('/wp-content/themes/inspiry-real-places/js/OverlappingMarkerSpiderfier.min.js', function() {
    	oms = new OverlappingMarkerSpiderfier(map, {
            markersWontMove: true,
            markersWontHide: true,
            basicFormatEvents: true,
            keepSpiderfied: false,
            ignoreMapClick: false,
            nearbyDistance: 5
        });
		if (debugspidefywithconsole) console.log('Spidefy javascript loaded');
		oms.addListener('spiderfy', function(markers) {
			for (var i = 0; i < markers.length; i ++) {
				if ((markers[i].isSpiderfyMaster == true) || (markers[i].isSpiderfyMinor == true)) {
					markers[i].isSpiderfied = true;
					if (markers[i].unspiderfiedIcon != undefined) {
						markers[i].setIcon(imageIcons[markers[i].unspiderfiedIcon]);
					} else {
						markers[i].setIcon(imageIcons[KLOADINGICON]);
					}
				}
				markers[i].setLabel(calculateLabel(map, markers[i], true));
			} 
		});
		oms.addListener('unspiderfy', function(markers) {
			for (var i = 0; i < markers.length; i ++) {
				
				if ((markers[i].isSpiderfyMaster == true) || (markers[i].isSpiderfyMinor == true)) {
					markers[i].isSpiderfied = false;
					markers[i].setIcon(imageIcons[KSPIDEFYICON]);
				}
				markers[i].setLabel(calculateLabel(map, markers[i]));
			} 
		});
    });


	map = new google.maps.Map(document.getElementById("listing-map"), mapOptions);
    
	var myTextDiv = document.createElement('div');
	myTextDiv.appendChild(myTitle);
	map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(myTextDiv);
	
    google.maps.event.addListener(map, "tilesloaded", function() {
    	// document.getElementById("map-image").removeAttribute("src");
    	document.getElementById("map-image").removeAttribute("style");
    });

	function between(min, p, max) {
		result = false;
		if (min < max) {
			if (p > min && p < max) {
				result = true;
			}
		}
		if (min > max) {
			if (p > max && p < min) {
				result = true
			}
		}
		if (p == min || p == max) {
			result = true;
		}
		return result;
	}
	
	function point_in_rectangle(x, y, left, top, right, bottom) {
		result = false;
		if (between(left, x, right) && between(top, y, bottom)) {
			result = true;
		}
		return result;
	}
	    
    google.maps.event.addListener(map, 'click', function(evt) {
	    if (openedMarkerInfoBox !== null) {
	    	// if (debugclickswithconsole) console.log("Closing infobox considerations: " + JSON.stringify(evt) + ", " + JSON.stringify(document.getElementById("infoBoxId").getBoundingClientRect()));
		    var rect = document.getElementById("infoBoxId").getBoundingClientRect();
		    var pt = evt.pixel; // maybe qa, pixel
		    if (point_in_rectangle(pt.x, pt.y, rect.x, rect.y, rect.width, rect.height)) {
		    	if (debugclickswithconsole) console.log("Click into infoBox, not closing.");
		    } else {
		    	if (debugclickswithconsole) console.log("Click not into infoBox, closing.");
		    	openedMarkerInfoBox.infoBox.close();
			    openedMarkerInfoBox.isIBOpened = false;
			    if (useLabels) openedMarkerInfoBox.setLabel(calculateLabel(map, openedMarkerInfoBox));
	            openedMarkerInfoBox = null;
		    }
		}
	});
    
	dIdleListener = google.maps.event.addListener(map, "idle", idleListenerFunc);
}

function attachInfoBoxToMarker(map, marker, infoBox) {
	google.maps.event.addListener(marker, 'spider_click', function(event) {
		try {
			if (event.alreadyCalled_) {
				console.log("event.alreadyCalled_");
		        return;
		    } else {
		        event.alreadyCalled_ = true;
		    }
		} catch (err) {}
        if (debugclickswithconsole) console.log('click detected');
		var scale = Math.pow(2, map.getZoom());
		var offsety = ( (100 / scale) || 0 );
        var projection = map.getProjection();
		var markerPosition = marker.getPosition();
		var markerScreenPosition = projection.fromLatLngToPoint(markerPosition);
		var pointHalfScreenAbove = new google.maps.Point(markerScreenPosition.x, markerScreenPosition.y - offsety);
		var aboveMarkerLatLng = projection.fromPointToLatLng(pointHalfScreenAbove);
		if ((openedMarkerInfoBox !== null) && (openedMarkerInfoBox !== marker)) {
		    openedMarkerInfoBox.infoBox.close();
		    openedMarkerInfoBox.isIBOpened = false;
		    if (useLabels) openedMarkerInfoBox.setLabel(calculateLabel(map, openedMarkerInfoBox));
		}
		if (marker.isIBOpened !== true) {
			oms.unspiderfy();
		    marker.infoBox = infoBox;
		    // map.setCenter(aboveMarkerLatLng);
			infoBox.open(map, marker);
			openedMarkerInfoBox = marker;
			openedMarkerInfoBox.infoBox = infoBox;
			marker.isIBOpened = true;
			if (marker.isCluster !== true) marker.setLabel(null);
		} else {
		    infoBox.close();
		    marker.isIBOpened = false;
		    if (useLabels) marker.setLabel(calculateLabel(map, marker));
		    openedMarkerInfoBox = null;
		}
	});
}

function calculateTotalClusterSize(marker, childMarkers) {
    if (marker.omsCluster != undefined)  {
	    var count = marker.omsCluster.length;
	    for (var i = 0; i < marker.omsCluster.length; i++) {
	        if (marker != marker.omsCluster[i]) {
	            if (childMarkers != undefined) {
    	            childMarkers.push(marker.omsCluster[i]);
    	        }
	            count += calculateTotalClusterSize(marker.omsCluster[i]);
	        }
	    }
	    return count;
    } else {
        if (childMarkers != undefined) {
            childMarkers.push(marker);
        }
        return 0;
    }
}

function calculateLabel(map, marker, skipSpiderfyLabel) {
    if (marker.isCluster === true) {
    	return "" + marker.clusterCount;
    }
    if ((marker.omsCluster !== undefined) && (map.getZoom() >= KMAXCLUSTERINGZOOMLEVEL) && (skipSpiderfyLabel == undefined) || (!skipSpiderfyLabel)) {
        var dsize = calculateTotalClusterSize(marker);
    	var dReturn = (dsize > 0)?(" " + dsize + " "):null;
        return dReturn;
    }
    if ((marker.property != null) && (marker.property.REAL_HOMES_property_price.length > 0)) {
        // return nFormatter(marker.property.REAL_HOMES_property_price, 1) + ' ' + marker.property.REAL_HOMES_property_price_postfix;
    	return null;
    } else {
        return null;
    }
}

function createInfoBoxForMarker(map, marker) {
	var boxText = document.createElement("div");
	boxText.className = 'map-info-window';
	boxText.id = 'infoBoxId';
	if (marker.isCluster === true) {
		if (marker.clusterIdsCache) {
			boxText.innerHTML = '<h5 class="prop-title"><ul><li><a id="populateListingsId" class="title-link" href="javascript:populateListings()">See the ' + marker.clusterCount + ' listings...</a></li><li><a id="zoomIntoClusterId" class="title-link" href="javascript:zoomIntoCluster()">Zoom-in</a></li></ul></h5><div class="arrow-down"></div>';
		} else {
			boxText.innerHTML = '<h5 class="prop-title"><ul><li><a id="zoomIntoClusterId" class="title-link" href="javascript:zoomIntoCluster()">Zoom-in</a></li></ul></h5><div class="arrow-down"></div>';
		}
	} else if ((marker.property.thumbnail_image === false) || (marker.property.thumbnail_image === undefined)) {
	    boxText.innerHTML = '<h5 class="prop-title"><a class="title-link" href="' + marker.property.guid + '">' + marker.property.post_title + '</a></h5>' +
			(((marker.property.REAL_HOMES_property_price != undefined) && (marker.property.REAL_HOMES_property_price.length > 0))?('<p><span class="price"><a href="' + marker.property.guid + '">' + parseInt(marker.property.REAL_HOMES_property_price).toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + marker.property.REAL_HOMES_property_price_postfix + '</a></span></p>'):'') +
			'<div class="arrow-down"></div>';
	} else {
		boxText.innerHTML = '<a class="thumb-link" href="' + marker.property.guid + '">' +
			'<img class="prop-thumb" src="' + marker.property.thumbnail_image + '" alt="' + marker.property.post_title + '"/>' +
			'</a>' +
			'<h5 class="prop-title"><a class="title-link" href="' + marker.property.guid + '">' + marker.property.post_title + '</a></h5>' +
			(((marker.property.REAL_HOMES_property_price != undefined) && (marker.property.REAL_HOMES_property_price.length > 0))?('<p><span class="price"><a href="' + marker.property.guid + '">' + parseInt(marker.property.REAL_HOMES_property_price).toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + marker.property.REAL_HOMES_property_price_postfix + '</a></span></p>'):'') +
			'<div class="arrow-down"></div>';
	}
	var myOptions = {
		content: boxText,
		disableAutoPan: true,
		maxWidth: 0,
		alignBottom: true,
		pixelOffset: (marker.isCluster === true)?new google.maps.Size(-122, -15):new google.maps.Size(-122, -48),
		closeBoxMargin: "",
		closeBoxURL: "",
		infoBoxClearance: new google.maps.Size(1, 1),
		isHidden: false,
		pane: "floatPane",
		zIndex: 99,
		enableEventPropagation: false
	};
	var ib = new InfoBox(myOptions);
	if ((useLabels) && (marker != null)) marker.setLabel(calculateLabel(map, marker));
	attachInfoBoxToMarker(map, marker, ib);
}

function assignIcon(marker) {
    if ((marker.property !== null) && (marker.property.taxonomies !== null)) {
        marker.property.taxonomies.forEach(function(value) {
            if (value.taxonomy === 'property-type') {
            	var iconNdx = KHOUSEICON;
                if (value.slug === 'apartments') {
                	iconNdx = KAPTICON;
                } else if (value.slug === 'office') {
                	iconNdx = KOFFICEICON;
                } else if (value.slug === 'shop') {
                	iconNdx = KSHOPICON;
                } else if (value.slug === 'country-houses') {
                	iconNdx = KVILLAICON;
                } else if (value.slug === 'land') {
                	iconNdx = KLANDICON;
                } else if (value.slug === 'locals') {
                	iconNdx = KSHOPICON;
                }
                if ((marker.isSpiderfyMaster == true) || (marker.isSpiderfyMinor == true)) {
                	marker.unspiderfiedIcon = iconNdx;
                	if (!marker.isSpiderfied) {
                		iconNdx = KSPIDEFYICON;
                	}
        		}
                marker.setIcon(imageIcons[iconNdx]);
            }
        });
    }
}

function getOnePropertyDetail(map, marker) {
    const locHttp = new XMLHttpRequest();
    locHttp.open("GET", "/wp-content/themes/inspiry-real-places/inc/util/propertyInfo.php?id=" + marker.propertyId + "&filter=guid|post_title|REAL_HOMES_property_price|thumbnail_image|REAL_HOMES_property_price_postfix|taxonomies");
    locHttp.send();
    locHttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			if (debugclickswithconsole) console.log('Got content for propertyid ' + marker.propertyId + ', ' + markersAwaitingPropertyDetails.length + ' left to fetch.');
            if (this.responseText) {
				var response = jQuery.parseJSON(this.responseText);
				if (response != null) {
					fetchedProperties["" + marker.propertyId] = response;
					marker.property = response;
					marker.setLabel(calculateLabel(map, marker));
					assignIcon(marker);
					createInfoBoxForMarker(map, marker);
				}
            }
            markersAwaitingPropertyDetails.splice(0, 1);
            if (markersAwaitingPropertyDetails.length > 0) {
                setTimeout(getOnePropertyDetail, 10, map, markersAwaitingPropertyDetails[0]);
            }
		}
	};
}

function markerIsNotClustered(map, marker) {
	if (marker.property != null) {
	    assignIcon(marker);
		createInfoBoxForMarker(map, marker);
	} else if (marker.propertyId != undefined) {
		if (fetchedProperties["" + marker.propertyId] != undefined) {
			marker.property = fetchedProperties["" + marker.propertyId];
			assignIcon(marker);
			createInfoBoxForMarker(map, marker);
		} else {
		    markersAwaitingPropertyDetails.unshift(marker);
			if (markersAwaitingPropertyDetails.length == 1) {
			    getOnePropertyDetail(map, markersAwaitingPropertyDetails[0]);
			}
		}
	}
}

// Sets the map on all markers in the array.
function setMapOnAll(map, vmarkers) {
	var count = 0;
	if (vmarkers !== undefined) {
        for (var i = 0; i < vmarkers.length; i++) {
            if ((vmarkers[i] != undefined) && (vmarkers[i].destroyed !== true)) {
            	if (vmarkers[i].getMap() != map) {
            		count++;
            	}
            	if (map == null) {
            		vmarkers[i].setVisible(false);
            	}
            	vmarkers[i].setMap(map);
            	if (map != null) {
            		vmarkers[i].setVisible(true);
            	}
            }
        }
        
	}
	return count;
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers(vmarkers, alsoConstructMarkers) {
	if (vmarkers != undefined) {
    	var count = 0;
    	if (((alsoConstructMarkers == undefined) || (alsoConstructMarkers === true)) && (vmarkers.constructMarkers != undefined)) {
    		count = setMapOnAll(null, vmarkers.constructMarkers);
    		for (var i = 0; i < vmarkers.constructMarkers.length; i++) {
    			if (vmarkers.constructMarkers[i] != undefined) oms.removeMarker(vmarkers.constructMarkers[i]);
    		}
    	}
		for (var i = 0; i < vmarkers.length; i++) {
			if (vmarkers[i] != null) oms.removeMarker(vmarkers[i]);
		}
        return count + setMapOnAll(null, vmarkers);
	} else {
		return 0;
	}
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers(zoom, alsoConstructMarkers) {
    clearMarkers(markers["" + zoom], alsoConstructMarkers);
    markers["" + zoom] = [];
}
    
function anotherPropertyMarkerExists(allMarkers, id) {
	if (allMarkers != undefined) {
		for (var i=0; i < allMarkers.length; i++) {
			oneMarker = allMarkers[i];
    		if (oneMarker != undefined) {
    			if ((oneMarker.visible) && (oneMarker.map != null) && (oneMarker.propertyId == id) && (oneMarker.isCluster !== true)) {
    				return oneMarker;
    			}
			}
		}
		return null;
	} else {
		return null;
	}
}

function anotherClusterMarkerExists(allMarkers, pos) {
	if (allMarkers != undefined) {
		for (var i=0; i < allMarkers.length; i++) {
			oneMarker = allMarkers[i];
    		if (oneMarker != undefined) {
    			if ((oneMarker.visible) && (oneMarker.map != null) && (("" + oneMarker.getPosition()) === ("" + pos)) && (oneMarker.isCluster === true)) {
    				return oneMarker;
    			}
			}
		}
		return null;
	} else {
		return null;
	}
}

function reprioritizePropertiesForZoom(zoom) {
	if (markersAwaitingPropertyDetails.length > 0) {
		markersAwaitingPropertyDetails.sort(function (a, b) {
			if ((a.zoomLevel != zoom) && (b.zoomLevel == zoom)) {
				return 1;
			} else if ((a.zoomLevel == zoom) && (b.zoomLevel != zoom)) {
				return -1;
			} else {
				return 0;
			}
		});
	}
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function centerOnJurisdiction(map, jurisdictionName) {
	var dBounds = undefined;
	var data = null;
	var zoom = null;
	console.log('centerOnJurisdiction called for ' + jurisdictionName);
	updateCurrentScreenDevice(map);	
	if ((jurisdictionName === 'Worldwide') && (propertiesMapData.countries !== null) && (propertiesMapData.countries != undefined)) {
		if (propertiesMapData.countries['Worldwide'][screendevice].view.bounds !== undefined) {
			dBounds = new google.maps.LatLngBounds(new google.maps.LatLng(propertiesMapData.countries['Worldwide'][screendevice].view.bounds.south, propertiesMapData.countries['Worldwide'][screendevice].view.bounds.west), new google.maps.LatLng(propertiesMapData.countries['Worldwide'][screendevice].view.bounds.north, propertiesMapData.countries['Worldwide'][screendevice].view.bounds.east));
			data = propertiesMapData.countries['Worldwide'][screendevice].view;
			zoom = propertiesMapData.countries['Worldwide'][screendevice].zoom;
			if (basiclogs) console.log("Worldwide: " + data);
			if ((JSON.stringify(data) == "{}") && (!requestedReset)) {
				if (basiclogs) console.log('requesting reset.');
				requestedReset = true;
				Http = new XMLHttpRequest();
				var url = "/wp-content/themes/inspiry-real-places/inc/util/calculateClusters.php?cluster_cache_reset=true";
			    Http.open("POST", url);
			    Http.send(params);
			    Http.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {}
			    };
			}
		}
	} else {
		if ((propertiesMapData.zoom !== undefined) && (propertiesMapData.zoom !== null) && (propertiesMapData.zoom != undefined)) {
			if (propertiesMapData.zoom[jurisdictionName] !== undefined) {
				dBounds = new google.maps.LatLngBounds(new google.maps.LatLng(propertiesMapData.zoom[jurisdictionName].sw.latitude, propertiesMapData.zoom[jurisdictionName].sw.longitude), new google.maps.LatLng(propertiesMapData.zoom[jurisdictionName].ne.latitude, propertiesMapData.zoom[jurisdictionName].ne.longitude));
			}
		}
		if ((propertiesMapData.countries!= undefined) && (propertiesMapData.countries!= null) && (propertiesMapData.countries[jurisdictionName] != undefined) && (propertiesMapData.countries[jurisdictionName][screendevice] !== undefined) && (propertiesMapData.countries[jurisdictionName][screendevice].view !== undefined)) {
			if (propertiesMapData.countries[jurisdictionName][screendevice].view.bounds != undefined) {
				dBounds = new google.maps.LatLngBounds(new google.maps.LatLng(propertiesMapData.countries[jurisdictionName][screendevice].view.bounds.south, propertiesMapData.countries[jurisdictionName][screendevice].view.bounds.west), new google.maps.LatLng(propertiesMapData.countries[jurisdictionName][screendevice].view.bounds.north, propertiesMapData.countries[jurisdictionName][screendevice].view.bounds.east));
			}
			data = propertiesMapData.countries[jurisdictionName][screendevice].view;
			zoom = propertiesMapData.countries[jurisdictionName][screendevice].zoom;
		}
	}
	// console.log('centerOnJurisdiction called with bounds ' + dBounds + ', propertiesMapData is ' + JSON.stringify(propertiesMapData));
	if (dBounds != undefined) {
		map.fitBounds(dBounds);
		nextIdleData = data;
	}
	return dBounds;
}

function updateCurrentScreenDevice(map) {
	var pixels = (map.getDiv().offsetWidth * map.getDiv().offsetHeight);
	var deltaMobile = Math.abs(pixels - (360 * 640));
	var deltaTablet = Math.abs(pixels - (768 * 1024));
	var deltaComputer = Math.abs(pixels - (1366 * 768));
	if ((deltaMobile < deltaTablet) && (deltaMobile < deltaComputer)) {
		screendevice = "mobile";
	} else if ((deltaTablet < deltaMobile) && (deltaTablet < deltaComputer)) {
		screendevice = "tablet";
	} else {
		screendevice = "computer";
	}
}

function processData(response, zoom) {
	isClustering = true;
	lastMarkersZoom = zoom;
	if (markers["" + zoom] === undefined) {
		markers["" + zoom] = new Array(response.clusters.length);
		markers["" + zoom].constructMarkers = markers["" + zoom];
	} else {
		markers["" + zoom].constructMarkers = new Array(response.clusters.length);
	}
	if (basiclogs) console.log('processData: ' + response.clusters.length + ' clusters to process.');
	markers["" + zoom].clusterbounds =  new google.maps.LatLngBounds(new google.maps.LatLng(response.bounds.south, response.bounds.west), new google.maps.LatLng(response.bounds.north, response.bounds.east));
	markers["" + zoom].clusters = response.clusters;
	markers["" + zoom].zoomLevel = zoom;
	markers["" + zoom].curMarker = 0;
	markers["" + zoom].randomArray = new Array(markers["" + zoom].clusters.length);
	var curRandom = 0;
    for (var i = 0; i < markers["" + zoom].clusters.length; i++) {
    	markers["" + zoom].randomArray[curRandom] = {};
    	markers["" + zoom].randomArray[curRandom].i = i;
	    curRandom++;
    }
    shuffle(markers["" + zoom].randomArray);
	outputMarkerState("End of Http.onreadystatechange with zoom to " + zoom);
	curHttpProcessingTimeout = setTimeout(processSomeMarkers);
}

function populateMapForBounds(map, bounds, zoom, data) {
	if (zoom == null) {
		zoom = map.getZoom();
	}
	var lastisamove = ((zoom === lastMarkersZoom) && (bounds != null) && (lastMarkersBounds != null) && (bounds.intersects(lastMarkersBounds)));
	var originalBoundsNull = ((bounds === null) || (bounds == undefined));
	var includesDCLine = false;
	if (originalBoundsNull) {
		if ((propertiesMapData.countries !== null) && (propertiesMapData.countries != undefined) && (propertiesMapData.countries['Worldwide'] != undefined) && (propertiesMapData.countries['Worldwide'].computer != undefined) && (propertiesMapData.countries['Worldwide'].computer.view != undefined)) {
			if (basiclogs) console.log('Worldwide: ' + JSON.stringify(propertiesMapData.countries['Worldwide']));
			if ((JSON.stringify(propertiesMapData.countries['Worldwide'].computer.view) == "{}") && (!requestedReset)) {
				if (basiclogs) console.log('requesting reset.');
				requestedReset = true;
				Http = new XMLHttpRequest();
				var url = "/wp-content/themes/inspiry-real-places/inc/util/calculateClusters.php?cluster_cache_reset=true";
			    Http.open("POST", url);
			    Http.send(params);
			    Http.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {}
			    };
			}
		}
		var east = getParameterByName('east');
		var west = getParameterByName('west');
		var north = getParameterByName('north');
		var south = getParameterByName('south');
		var zoom = getParameterByName('zoom');
		if ((propertiesMapData.properties_bounds != undefined) && (typeof propertiesMapData.properties_bounds === 'string' || propertiesMapData.properties_bounds instanceof String) && (propertiesMapData.properties_bounds != "")) {
			propertiesMapData.properties_bounds = JSON.parse(propertiesMapData.properties_bounds.replace(/'/g, "\""));
		}
		if ((east !== null) && (west !== null) && (north !== null) && (south !== null) && (zoom !== null)) {
			bounds = new google.maps.LatLngBounds(new google.maps.LatLng(south, west), new google.maps.LatLng(north, east));
			map.fitBounds(bounds);
			setTimeout(populateMapForBounds, 10, map, bounds, zoom, data);
			return;
		} else if (propertiesMapData.properties == undefined) {
			if (getCookie('country') != null) {
				if (centerOnJurisdiction(map, (getCookie('country') != null)?getCookie('country'):'Worldwide') != null) {
					return;
				}
			}
		} else if ((propertiesMapData.properties_bounds != undefined) && (propertiesMapData.properties_bounds.south != undefined) && (propertiesMapData.properties_bounds.west != undefined) && (propertiesMapData.properties_bounds.north != undefined) && (propertiesMapData.properties_bounds.east != undefined)) {
			console.log('propertiesMapData.properties_bounds (handled): ' + JSON.stringify(propertiesMapData.properties_bounds) + ", " + propertiesMapData.properties_bounds);
			bounds = new google.maps.LatLngBounds(new google.maps.LatLng(propertiesMapData.properties_bounds.south, propertiesMapData.properties_bounds.west), new google.maps.LatLng(propertiesMapData.properties_bounds.north, propertiesMapData.properties_bounds.east));
			map.fitBounds(bounds);
			setTimeout(populateMapForBounds, 10, map, bounds, zoom, data);
			return;
		} else if (propertiesMapData.properties_bounds != undefined) {
			console.log('propertiesMapData.properties_bounds (not handled): ' + JSON.stringify(propertiesMapData.properties_bounds) + ", " + propertiesMapData.properties_bounds);
		}
	} else {
		includesDCLine = bounds.contains(new google.maps.LatLng((bounds.getNorthEast().lat() + bounds.getSouthWest().lat())/2, 180.0));
	}
    if (curHttpProcessingTimeout != null) {
		clearTimeout(curHttpProcessingTimeout);
		curHttpProcessingTimeout = null;
	}
    if (Http != null) {
    	setMessage(null);
    	Http.abort();
    }
    if (data == null) {
    	if (basiclogs) console.log('populateMapForBounds with data from http');
	    if (markers["" + zoom] !== undefined) {
	    	onlyCurrentZoomMarkersVisible();
	        setMapOnAll(map, markers["" + zoom]);
	    	setMessage("", true);
	    } else {
	    	setMessage("Rendering locations...", true);
	    }
	    reprioritizePropertiesForZoom(zoom);
	    isClustering = true;
	    Http = new XMLHttpRequest();
	    var tolerance = Math.round(1.2 * (Math.min(map.getDiv().offsetWidth, map.getDiv().offsetHeight)/(KSMALLCLUSTERSIZE)));
	    updateCurrentScreenDevice(map);
	    if (debugdisplaywithconsole) console.log("screendevice is " + screendevice + ", derived from screen size " + map.getDiv().offsetWidth + ", " + map.getDiv().offsetHeight);
	    // var url = "https://maps.every-listing.com?request=get" + ((zoom != null)?("&zoom=" + zoom):"") + "&tolerance=" + tolerance + "&max=5" + ((bounds !== null)?("&west=" + bounds.getSouthWest().lng() + "&east=" + bounds.getNorthEast().lng() + "&north=" + bounds.getNorthEast().lat() + "&south=" + bounds.getSouthWest().lat() + "&screendevice=" + screendevice + ((includesDCLine)?"&includesDCLine":"")):"");  
	    var url = "/wp-content/themes/inspiry-real-places/inc/util/calculateClusters.php?" + ((zoom != null)?("_zoom=" + zoom):"") + "&tolerance=" + tolerance + "&max=5" + ((bounds !== null)?("&west=" + bounds.getSouthWest().lng() + "&east=" + bounds.getNorthEast().lng() + "&north=" + bounds.getNorthEast().lat() + "&south=" + bounds.getSouthWest().lat() + "&screendevice=" + screendevice + ((includesDCLine)?"&includesDCLine=true":"")):"");
	    Http.open("POST", url);
	    var params = ((propertiesMapData != undefined) && (propertiesMapData.properties != undefined))?propertiesMapData.properties:undefined;
	    if (debugdisplaywithconsole || debugservletinteractions) {
	    	console.log("Requesting clusters from: " + url);
	    }
	    Http.send(params);
	    Http.onreadystatechange = function() {
	        if (this.readyState == 4 && this.status == 200) {
	        	if (basiclogs) console.log('populateMapForBounds processing http response');
	            if (lastMarkersZoom !== zoom) {
	            	deleteMarkers(lastMarkersZoom);
	            } else if (lastisamove) {
	            	deleteMarkers(zoom);
	            } else {
	            	// Let's always delete the markers.
	            	deleteMarkers(zoom);
	            }
	            lastMarkersZoom = zoom;
	            lastMarkersBounds = bounds;
	        	Http = null;
	            if (this.responseText) {
					var responseText = this.responseText.replace(/&quot;/g, '"').replace(/'/g, '"');
					responseText = responseText.replace(/^\s+|\s+$/g, '');
					if (debugdisplaywithconsole || debugservletinteractions) console.log("responseText: "+ responseText);
					var response = ((responseText !== '') && (responseText !== '{}') && (!responseText.includes("<NO CONTENT>")) && (!responseText.includes("Servlet Error: NO CONTENT")))?jQuery.parseJSON(responseText):null;
					if (response !== null) {
						if (originalBoundsNull) {
							if (basiclogs) console.log('populateMapForBounds, changing the bounds');
							map.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(response.bounds.south, response.bounds.west), new google.maps.LatLng(response.bounds.north, response.bounds.east)));
						} else if ((response != undefined) && (response.clusters != undefined)) {
							if (basiclogs) console.log('populateMapForBounds, calling processData with zoom ' + zoom);
							processData(response, zoom);
						} else {
							if (basiclogs) console.log('populateMapForBounds, http response was empty');
							outputMarkerState("End of Http.onreadystatechange with zoom to " + zoom + " as empty content.");
						}
					}
				}
	        }
	        setMessage(null);
	    };
    } else {
    	if (basiclogs) console.log('populateMapForBounds with provided data ' + JSON.stringify(data));
        setTimeout(processData, 10, data, zoom);
    }
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function verifyForOrphanMarkersCallback() {
    isClustering = false;
    if (map.getZoom() < KMAXCLUSTERINGZOOMLEVEL) {
        verifyForOrphanMarkers();
    }
}

function verifyForOrphanMarkers() {
    var count = 0;
    if (map.getZoom() < KMAXCLUSTERINGZOOMLEVEL) {
    	for (var i = 0; i < markers["" + zoom].length; i++) {
    		if (markers["" + zoom][i] != undefined) {
                var mrkr = markers["" + zoom][i];
                if ((mrkr.getMap() !== null) && (mrkr.visible) && (mrkr.getMap().getBounds().contains(mrkr.getPosition()))) {
                    count++;
                	markerIsNotClustered(mrkr.getMap(), mrkr); 
                }
    		}
        }
    }
    if (debugclickswithconsole) console.log('verifyForOrphanMarkers: ' + count);
}

function setMessage(message, newContext) {
	if (message == null) {
		message = msgStack.pop();
	} else if (newContext) {
		msgStack.push(message);
	}
	if (message == null) {
		myTitle.innerHTML = "<span style='display:none;'/>";
	} else {
		myTitle.innerHTML = "<span style='display:block; " + KMAPMESSAGESTYLE + "'>" + message + "</span>";
	}
}

function nFormatter(num, digits) {
	var si = [{ value: 1, symbol: "" },
		{ value: 1E3, symbol: "k" },
		{ value: 1E6, symbol: "M" },
		{ value: 1E9, symbol: "G" },
		{ value: 1E12, symbol: "T" },
		{ value: 1E15, symbol: "P" },
		{ value: 1E18, symbol: "E" }
	];
	var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
	var i;
	for (i = si.length - 1; i > 0; i--) {
		if (num >= si[i].value) {
			break;
		}
	}
	return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

//function removeDuplicateMarkers() {
//	markers.forEach (function (entry) {
//		for (var i = (entry.length - 1); i >= 0; i--) {
//			if ((entry[i] != undefined) && (entry[i].propertyId != undefined)) {
//				for (var j = (i-1); j >= 0; j--) {
//					if ((entry[j] != undefined) && (entry[i].propertyId === entry[j].propertyId)) {
//						entry[j] = undefined;
//					}
//				}
//			}
//		}
//	});
//}

function outputMarkerState(context) {
	if (!debugdisplaywithconsole) return;
	var ndx = 1;
	console.log(context + " -------------------------");
	markers.forEach (function (entry) {
		console.log("#"+ ndx + ". zoom: " + entry.zoomLevel + ", count: " + entry.length + ", curMarker: " + entry.curMarker + ((entry.randomArray != undefined)?(", randomArray: " + entry.randomArray.length):"") + ((entry.clusters != undefined)?(", clusters: " + entry.clusters.length):""));
		var count = 0;
		for (var i = 0; i < entry.length; i++) {
			if (entry[i] != undefined) {
				if (entry[i].getMap() == map) {
					console.log("\t" + (entry[i].getLabel()?("'" + entry[i].getLabel() + "'"):("Point" + " id " + entry[i].propertyId)) + " visible at " + entry[i].getPosition());
				} else {
					count++;
				}
			}
		}
		if (count > 0) {
			console.log("\tinvisible: " + count);
		}
		ndx++;
	});
	console.log("------------------------------------");
}

function recalculateSpidefy() {
	if (oms == null) {
		if (debugspidefywithconsole) console.log('Spidefy called too quickly, returning from recalculateSpidefy');
		return;
	}
	var zoomLevel = lastZoom;
	if (lastZoom != lastSpidefyZoom) {
		oms.removeAllMarkers();
	}
	lastSpidefyZoom = lastZoom;
	if (debugspidefywithconsole) console.log('recalculateSpidefy with zoom level ' + zoomLevel);
    if (markersHasOMSCluster.length > 0) {
        for (var i = 0; i < markersHasOMSCluster.length; i++) {
            markersHasOMSCluster[i].setVisible(true);
            for (var j = 0; j < markersHasOMSCluster[i].omsCluster.length; j++) {
                markersHasOMSCluster[i].omsCluster[j].setVisible(true);
            }
            if ((useLabels) && (markersHasOMSCluster[i] != null)) markersHasOMSCluster[i].setLabel(calculateLabel(map, markersHasOMSCluster[i]));
            markersHasOMSCluster[i].omsCluster = undefined;
        }
        markersHasOMSCluster = [];
    }
    if (markers["" + zoomLevel] != undefined) for (var ndx = 0; ndx < markers["" + zoomLevel].length; ndx++) {
    	if (markers["" + zoomLevel][ndx] != null) oms.trackMarker(markers["" + zoomLevel][ndx]);
    }
    if (map.getZoom() >= KMAXCLUSTERINGZOOMLEVEL) {
    	if (debugspidefywithconsole) console.log('spidefying: map.getZoom() [' + map.getZoom() + '] >= KMAXCLUSTERINGZOOMLEVEL [' + KMAXCLUSTERINGZOOMLEVEL + ']');
    	try {
			var nearbyMarkersArray = oms.markersNearAnyOtherMarker();
			if ((nearbyMarkersArray !== null) && (nearbyMarkersArray.length > 0)) {
			    var processed = [];
			    var markerWithProximity;
			    while ((nearbyMarkersArray.length > 0) && ((markerWithProximity = nearbyMarkersArray.pop()) !== null) && (markerWithProximity !== undefined)) {
			    	var found = false;
	                for (var k = 0; k < processed.length; k++) {
	                    if (processed[k] == markerWithProximity) {
	                        found = true;
	                        break;
	                    }
	                }
			        if (!found) {
					    if ((markerWithProximity !== null) && (markerWithProximity !== undefined) && (map.getBounds().contains(markerWithProximity.getPosition()))) {
			                processed.push(markerWithProximity);
			                if ((markerWithProximity.zoomLevel != undefined) && (markerWithProximity.zoomLevel == zoomLevel)) {
	    				        var thisProximity = oms.markersNearMarker(markerWithProximity, false);
	    				        var master;
	    				        if ((useLabels) && (master != null)) master.setLabel(calculateLabel(map, master));
	        				    if (thisProximity.length > 0) {
	        				        master = markerWithProximity;
	        				        markersHasOMSCluster.push(master);
	        				        master.omsCluster = [];
	                                do {
	        			                if ((markerWithProximity.zoomLevel != undefined) && (markerWithProximity.zoomLevel == zoomLevel)) {
	        			                	master.isSpiderfyMinor = true;
	        			                	markerIsNotClustered(map, markerWithProximity);
		                                    processed.push(markerWithProximity);
		                                    master.omsCluster.push(markerWithProximity);
		                                    markerWithProximity.setVisible(true);
		                                    // oms.forgetMarker(markerWithProximity);
	        			                } else {
	        			                	oms.removeMarker(markerWithProximity);
	        			                }
	                                } while (((markerWithProximity = thisProximity.pop()) !== null) && (markerWithProximity !== undefined));
	                                google.maps.event.trigger(master, 'click');
	                                master.isSpiderfyMaster = true;
	                                master.isSpiderfied = false;
	                                if ((useLabels) && (master != null)) master.setLabel(calculateLabel(map, master));
	        				    }
			                } else {
			                	oms.removeMarker(markerWithProximity);
			                }
					    }
			        }
			    }
			}
    	} catch (err) {
    		if (debugspidefywithconsole) console.log(err);
    		setTimeout(recalculateSpidefy);
    	}
    } else {
    	if (debugspidefywithconsole) console.log('not spidefying: map.getZoom() [' + map.getZoom() + '] < KMAXCLUSTERINGZOOMLEVEL [' + KMAXCLUSTERINGZOOMLEVEL + ']');
        if ((lastZoom !== undefined) && (lastZoom >= KMAXCLUSTERINGZOOMLEVEL)) {
        	oms.unspiderfy();
            clearMarkers(markers[lastZoom]);
        }
        if (!isClustering) verifyForOrphanMarkers();
    }
}

function recalculateClusters() {
	if (((map != null) && (map.getBounds() != null) && (!map.getBounds().equals(lastCalculatedBounds))) || ((map != null) && (lastCalculatedZoom !== map.getZoom()))) {
		if (basiclogs) console.log('recalculateClusters called populateMapForBounds at bounds ' + map.getBounds() + ' with zoom level ' + map.getZoom() + ' and data ' + nextIdleData);
	    var dIdleData = nextIdleData;
	    nextIdleData = null;
	    lastCalculatedBounds = map.getBounds();
	    lastCalculatedZoom = map.getZoom();
		populateMapForBounds(map, map.getBounds(), map.getZoom(), dIdleData);
	    lastZoom = map.getZoom();
		setTimeout(recalculateSpidefy);
	}
}

function processSomeMarkers() {
    if (oms == null) {
    	if (debugspidefywithconsole) console.log('Spidefy called too quickly, returning from processSomeMarkers');
        // we wait for the javascript to be available
    	curHttpProcessingTimeout = setTimeout(processSomeMarkers);
        return;
    }
    if (basiclogs) console.log('processSomeMarkers entered for zoom ' + lastMarkersZoom);
    curHttpProcessingTimeout = null;
    var zoomInProcess = lastMarkersZoom;
    var addedClustersCount = 0;
    var addedPointsCount = 0;
    var curMarkerOnEntry = markers[zoomInProcess].curMarker;
    for (var rndIndex = markers[zoomInProcess].randomArray.length-1; rndIndex >= 0; rndIndex--) {
    	var pos = new google.maps.LatLng(markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].lat, markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].lng);
    	var lbl = (markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations == undefined)?("" + markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].count):null;
    	var alreadyMarker;
    	//console.log(markers[zoomInProcess]);
    	if (markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations == undefined) {
    		alreadyMarker = anotherClusterMarkerExists(markers[zoomInProcess], pos);
    		if (alreadyMarker == undefined) {
    			var iconToUse;
    			switch (("" + markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].count).length) {
    				case 1:
    					iconToUse = KSMALLORANGECLUSTER;
    					break;
    				case 2:
    					iconToUse = KSMALLORANGECLUSTER;
    					break;
    				case 3:
    					iconToUse = KLARGEORANGECLUSTER;
    					break;    	
    				case 4:
    					iconToUse = KLARGEORANGECLUSTER;
    					break;  
					default:
						iconToUse = KSMALLORANGECLUSTER;
    			}
        		markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker] = new google.maps.Marker({
					position: pos,
        		    label: lbl,
					map: map,
					icon: imageIcons[iconToUse],
					title: null,
					property: null,
					visible: true
				});
        		addedClustersCount++;
				markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].id = markers[zoomInProcess].curMarker;
				markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].zoomLevel = zoomInProcess;
				markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].isCluster = true;
				markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].clusterCount = markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].count;
				markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].clusterIdsCache = markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].idsCache;
				createInfoBoxForMarker(map, markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker]);
        		google.maps.event.addListener(markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker], "dblclick", zoomIntoCluster);
        		markers[zoomInProcess].curMarker++;
    		}
    	} else if (markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations != undefined) {
    		for (var i = 0; i < markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations.length; i++) {
        		for (var j = 0; j < markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].ids.length; j++) {
            		alreadyMarker = anotherPropertyMarkerExists(markers[zoomInProcess], markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].ids[j]);
            		if (alreadyMarker == null) {
            			var pos = new google.maps.LatLng(markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].lat, markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].lng);
                		markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker] = new google.maps.Marker({
    						position: pos,
    						map: map,
    						label: lbl,
    						animation: google.maps.Animation.DROP,
    						icon: imageIcons[KLOADINGICON],
    						title: null,
    						property: null,
    						optimized: false,
    						visible: true
    					});
                		addedPointsCount++;
    					markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].zoomLevel = zoomInProcess;
    				    markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].propertyId = markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].ids[j];
    				    markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker].property = null;
    				    markerIsNotClustered(map, markers[zoomInProcess].constructMarkers[markers[zoomInProcess].curMarker]);
    				    markers[zoomInProcess].curMarker++;
    				    if (basiclogs) console.log('Marker added at ' + markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].lat + ', ' + markers[zoomInProcess].clusters[markers[zoomInProcess].randomArray[rndIndex].i].locations[i].lng);
            		}
        		}
    		}
	    }
	    if (markers[zoomInProcess].curMarker >= curMarkerOnEntry + KMARKERSTOINSERT) {
	    	markers[zoomInProcess].randomArray = markers[zoomInProcess].randomArray.slice(0, rndIndex-1);
	        break;
	    } else if (rndIndex == 0) {
	    	markers[zoomInProcess].randomArray = [];
	    }
    }
    if (basiclogs) console.log('processSomeMarkers: processed ' + (markers[zoomInProcess].curMarker - curMarkerOnEntry) + ' marker, added ' + addedPointsCount + ' point, ' + addedClustersCount + ' cluster.');
	if (markers[zoomInProcess].randomArray.length > 0) {
	    someMarkersProcessed();
	    curHttpProcessingTimeout = setTimeout(processSomeMarkers);
	} else {
	    markers[zoomInProcess].length = markers[zoomInProcess].curMarker;
	    for (var i = 0; i < markers[zoomInProcess].constructMarkers.length; i++) {
	    	if (markers[zoomInProcess].constructMarkers[i] != null) oms.trackMarker(markers[zoomInProcess].constructMarkers[i]);
	    }
    	if (debugdisplaywithconsole) console.log("constructMarkers copied to markers since they were not the same at zoom " + zoomInProcess);
    	markers[zoomInProcess] = markers[zoomInProcess].constructMarkers;
    	markers[zoomInProcess].constructMarkers = undefined;
    	markers[zoomInProcess].zoomLevel = zoomInProcess;
	    someMarkersProcessed();
	    onlyCurrentZoomMarkersVisible();
	    if (basiclogs) console.log('processSomeMarkers finished calculating');
	    // removeDuplicateMarkers();
		outputMarkerState("Finished calculation with zoom to " + zoomInProcess + ", current zoom level is: " + map.getZoom());
	}
}

function onlyCurrentZoomMarkersVisible() {
	if (markers != undefined) {
		for (var i = 0; i < markers.length; i++) {
			onezoomlevel = markers[i];
	    	if (onezoomlevel != undefined) {
		    	if (onezoomlevel.zoomLevel != map.getZoom()) {
		    		var count = deleteMarkers(onezoomlevel);
		    		if (debugdisplaywithconsole) console.log("Clearing " + count + " markers from zoom level " + onezoomlevel.zoomLevel);
		    	}
	    	}
	    }
	}
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
         while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function someMarkersProcessed() {


	// var countryCookie = getCookie('country');
	// if(countryCookie){
	// 	console.log(countryCookie);
	// }
	// var lifeStyleCookie = getCookie('lifestyle');
	// if(lifeStyleCookie){
	// 	console.log(lifeStyleCookie);
	// }
}

function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.head;
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}
