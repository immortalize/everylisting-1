<?php
/**
 * Template Name: Template without Header / Footer / Sidebar
 * Template will display only the contents you had entered in the page editor
 */
?>

<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body>
	<div id="content-wrapper" class="site-content-wrapper site-pages">

		<div id="content" class="site-content layout-boxed">

			<div class="container">

				<div class="row">

					<div class="col-xs-12 site-main-content">

						<main id="main" class="site-main default-page clearfix">
                            <?php

                            if (have_posts()) :

                                while (have_posts()) :

                                    the_post();
                                    ?>
                                    <article
								id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

								<div class="entry-content clearfix">
                                            <?php the_content(); ?>
                                            <?php
                                    wp_link_pages(array(
                                        'before' => '<div class="page-links">' . esc_html__('Pages:', 'inspiry'),
                                        'after' => '</div>'
                                    ));
                                    ?>
                                        </div>

								<!--                                         <footer class="entry-footer"> -->
                                            <?php
                                    // edit_post_link( esc_html__( 'Edit', 'inspiry' ), '<span class="edit-link">', '</span>' );
                                    ?>
<!--                                         </footer> -->

							</article>
                                    <?php
                                    // If comments are open or we have at least one comment, load up the comment template
                                    if (comments_open() || '0' != get_comments_number()) :
                                        comments_template();
                                    endif;

                                endwhile
                                ;

                            endif;

                            ?>

                        </main>
						<!-- .site-main -->

					</div>
					<!-- .site-main-content -->

				</div>
				<!-- .row -->

			</div>
			<!-- .container -->

		</div>
		<!-- .site-content -->

	</div>
	<!-- .site-content-wrapper -->
<?php wp_footer(); ?>
</body>
</html>