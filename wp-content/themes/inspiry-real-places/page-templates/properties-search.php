<?php commentPHPContext('/wp-content/themes/inspiry-real-places/page-templates/properties-search.php'); ?>

<?php
/*
 * Template Name: Properties Search
 */

get_header();
global $inspiry_options;

$search_layout = $inspiry_options['inspiry_search_layout'];

$skipmaps = false;
if (isset($_GET['nomap']) && !empty($_GET['nomap'])) {
	$skipmaps = $_GET['nomap'] == 'true';
}

if (( $search_layout == 'half-left' ) && (!$skipmaps)) {
	get_template_part( 'partials/property/half-map/properties-search-half-map-left' );
} elseif (($search_layout == 'half-right') && (!$skipmaps)) {
	get_template_part( 'partials/property/half-map/properties-search-half-map-right' );
} else {
	get_template_part( 'partials/property/templates/property-search' );
}
get_footer();
?>

