<?php commentPHPContext('/wp-content/themes/inspiry-real-places/test.php'); ?>

<?php

$req = '{
  "responseId": "cfcfe0ac-1622-4e2e-8236-a8c67d14312b-b4ef8d5f",
  "queryResult": {
    "queryText": "I want to buy a house",
    "action": "Property_Search",
    "parameters": {
      "Property_Type": "Houses",
      "Transaction_Type": "For Sale",
      "Tracer": "0",
      "Jurisdiction_Country": "",
      "Jurisdiction_City": "",
      "Property_Features": [],
      "price_maximum": "",
      "price_minimum": "",
      "Lifestyle": ""
    },
    "allRequiredParamsPresent": true,
    "fulfillmentText": "OK, got it! Here is what I have in terms of Houses for For Sale. To refine the search, feel free to state the city or country. You can also state the lifestyle living of your preference (mountain, beach, city-life or else).",
    "fulfillmentMessages": [
      {
        "platform": "ACTIONS_ON_GOOGLE",
        "simpleResponses": {
          "simpleResponses": [
            {
              "textToSpeech": "OK, got it! Here is what I have in terms of Houses for For Sale. To refine the search, feel free to state the city or country. You can also state the lifestyle living of your preference (mountain, beach, city-life or else)."
            }
          ]
        }
      },
      {
        "text": {
          "text": [
            "OK, got it! Here is what I have in terms of Houses for For Sale. To refine the search, feel free to state the city or country. You can also state the lifestyle living of your preference (mountain, beach, city-life or else)."
          ]
        }
      }
    ],
    "outputContexts": [
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/property_search",
        "lifespanCount": 25,
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/usergreeted",
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/property_search_follow_up",
        "lifespanCount": 5,
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/property_search-followup",
        "lifespanCount": 2,
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/property_search-followup-2",
        "lifespanCount": 2,
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/system_context",
        "lifespanCount": 5,
        "parameters": {
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Tracer.original": ""
        }
      },
      {
        "name": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23/contexts/__system_counters__",
        "parameters": {
          "no-input": 0,
          "no-match": 0,
          "Property_Type": "Houses",
          "Property_Type.original": "house",
          "Transaction_Type": "For Sale",
          "Transaction_Type.original": "buy",
          "Tracer": "0",
          "Jurisdiction_Country": "",
          "Jurisdiction_Country.original": "",
          "Jurisdiction_City": "",
          "Jurisdiction_City.original": "",
          "Property_Features": [],
          "Property_Features.original": [],
          "price_maximum": "",
          "price_maximum.original": "",
          "price_minimum": "",
          "price_minimum.original": "",
          "Lifestyle": "",
          "Lifestyle.original": "",
          "Tracer.original": ""
        }
      }
    ],
    "intent": {
      "name": "projects/every-listing/agent/intents/05d50dcc-5091-4a52-857c-277a6cfd19ce",
      "displayName": "Property_Search"
    },
    "intentDetectionConfidence": 1,
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {}
  },
  "session": "projects/every-listing/agent/sessions/5a81b0ea-f5af-bf5a-543f-e85eee0dbc23"
}';
$intent = $req['queryResult']['intent']['displayName'];
$parameters = $req['queryResult']['parameters'];
processChatbotWebHook($req, $intent, $parameters);
?>