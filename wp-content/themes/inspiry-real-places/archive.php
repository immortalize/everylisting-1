<?php commentPHPContext('/wp-content/themes/inspiry-real-places/archive.php'); ?>

<?php
/*
 * Archive page
 */

get_header();

get_template_part( 'partials/header/banner' );

global $wp;
$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$country = $uriSegments[2];
$country = str_replace("-"," ",$country);

$isCategory = false;

foreach ($uriSegments as $uriSegment) {
    if ((strcasecmp($uriSegment, 'category')) || (strcasecmp($uriSegment, 'author'))) {
        $isCategory = true;
        break;
    }
}
                            
?>

<div id="content-wrapper" class="site-content-wrapper site-pages">
<?php if (!$isCategory) { ?>
<div id="moreloadidof" class="moreloaddivcls">&nbsp;</div>   
<div id="homeProperties" class="property-listing-two ">
        <div class="col-md-6 ">
            <h1 class="font90"> Latest <?php echo $country; ?> Properties</h1>
        </div>
        <div class="col-md-6 ">
        </div>
    <div class="newContainer" id="aftersearchshow">
        <?php 
        //col-xs-7
    if(isset($_POST['iteration'])){
		$iteration = (int)$_POST['iteration'];
		$iteration++;
	}else{
		$iteration = 1;
	}

    if(isset($_POST['offset'])){
    	$offset = $_POST['offset'];
    	$passOffset = $iteration*16;
    }else{
    	$offset = '';
    	$passOffset = 16;
    }
   
   // $country = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
    
	$properties_search_arg = array(
	    'post_type' => 'property',
	    'offset' => $offset,
	    'meta' => $country,
	    'posts_per_page' => 16
	);

	$properties_search_query = new WP_Query( apply_filters( 'inspiry_property_search', $properties_search_arg )  );
	

	$show_btn = 'false';
    if ( $properties_search_query->have_posts() ) : 
            $properties_count = 1;
            $columns_count = 4;   // todo: add the ability to change number of columns between 2, 3, 4

            while ( $properties_search_query->have_posts() ) :
                $properties_search_query->the_post();
                if ( has_post_thumbnail() ) {                    
                    ?>
                     <div class='col-lg-3'>
                        <div class="homepropBoxes thumbnail-size">
                            <?php new_property_list_insert(get_the_ID()) ?>
                        </div>
                    </div>
                    <?php 
                }
                $properties_count++;
            endwhile;?>
            
        
        <div class = "col-md-12 loadmorediv"  style="text-align: center;">
        	<div class="alm-btn-wrap">
        		<button class="alm-load-more-btn more loadmore"   style="background: #0073aa !important;"  data-country = "<?php echo $country; ?>"  data-offset = "<?=$passOffset?>" data-iteration = "<?=$iteration?>" rel="next">Show More Propties</button>
        	</div>
        </div>
        <?php
        else: ?>
        	<div class = "col-md-12 " style="text-align: center;">
        		<div class="alm-btn-wrap">
        			<button class="alm-load-more-btn more " style="background: #0073aa !important;">No Property Found</button>
        		</div>
        	</div>
        <?php
         endif;
         wp_reset_postdata();
         ?>
    </div>
</div>
    <!-- .site-content -->
<?php } else { ?>
	<div id="content" class="site-content layout-boxed">

        <div class="container">

            <div class="row">

                <div class="col-md-9 site-main-content">

                    <main id="main" class="site-main blog-post-listing">
                        <?php
                        /* Main loop */
                        if ( have_posts() ) :

                            while ( have_posts() ) :

                                the_post();

                                $format = get_post_format( get_the_ID() );
                                if ( false === $format ) {
                                    $format = 'standard';
                                }
                                ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >

                                    <?php
                                    // image, gallery or video based on format
                                    if ( in_array( $format, array( 'standard', 'image', 'gallery', 'video' ) ) ) :
                                        get_template_part( 'partials/post/entry-format', $format );
                                    endif;
                                    ?>

                                    <header class="entry-header blog-post-entry-header">
                                        <?php
                                        // title
                                        get_template_part( 'partials/post/entry-title' );

                                        // meta
                                        get_template_part( 'partials/post/entry-meta' );
                                        ?>
                                    </header>

                                    <div class="entry-summary">
                                        <?php
                                        if( strpos( get_the_content(), 'more-link' ) === false ) {
                                            the_excerpt();
                                        } else {
                                            the_content( '' );
                                        }
                                        ?>
                                        <a href="<?php the_permalink(); ?>" rel="bookmark" class="btn-default btn-orange"><?php esc_html_e( 'Read More', 'inspiry' ); ?></a>
                                    </div>

                                </article>
                                <?php

                            endwhile;

                            global $wp_query;
                            inspiry_pagination( $wp_query );

                        else :

                            inspiry_nothing_found();

                        endif;
                        ?>

                    </main>
                    <!-- .site-main -->

                </div>
                <!-- .site-main-content -->
	            <?php if ( inspiry_is_active_custom_sidebar( 'default-sidebar' ) ) : ?>
                    <div class="col-md-3 site-sidebar-content">
			            <?php get_sidebar(); ?>
                    </div>
                    <!-- .site-sidebar-content -->
	            <?php endif; ?>

            </div>
            <!-- .row -->

        </div>
        <!-- .container -->
<?php } ?>
</div>
<!-- .site-content-wrapper -->

<?php
/*
 * Footer
 */
get_footer();

