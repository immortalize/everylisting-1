function getIdStartWith(criterion) {
     var tags = [],els = document.getElementsByTagName('*');
     for (var i=els.length;i--;) if ((els[i].id !== undefined) && (els[i].id).indexOf(criterion)===0) tags.push(els[i]);
     return tags;
}
function hideElements(criterion) {
     var tags = getIdStartWith(criterion);
     for (var i=tags.length;i--;) tags[i].visible = hidden;
}
var tags = getIdStartWith(/^content_/);
for (var i=tags.length;i--;) if (tags[i].innerHTML === "") hideElements("hide_on_" + tags[i].id);