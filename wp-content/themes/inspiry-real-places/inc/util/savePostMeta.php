<?php 

require_once('../../../../../wp-load.php');

if (isset($_GET['id'])) {
    
    foreach ($_GET as $key => $val) {
        if ($key == 'id') {
            continue;
        }
        if (urldecode($val) == "undefined") {
            delete_post_meta($_GET['id'], urldecode($key));
        } else {
            update_post_meta($_GET['id'], urldecode($key), urldecode($val));
        }
    }
    echo "OK";
    die;
}
echo "ERROR (no id)";

?>