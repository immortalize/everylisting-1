<?php 

require_once('../../../../../wp-load.php');

$post = array();

if (isset($_GET['startwith'])) {
    $startWith = $_GET['startwith'];
} else {
    unset($startWith);
}

$post_meta = get_user_meta(get_current_user_id());
    
foreach ($post_meta as $key => $value) {
    if ((!isset($startWith)) || (startsWith($key, $startWith))) {
        $post[$key] = $value[0];
    }
}

if ((!isset($startWith)) || ($startWith == "user_role_")) {
    $user = wp_get_current_user();
    foreach ($user->roles as $key => $value) {
        array_push($post, $value);
    }
}

if ($startWith == "user_role_") {
    echo '{"active": ' . str_replace("-", "_", str_replace("'", '"', json_encode($post))) . '}';
} else {
    echo json_encode($post);
}

?>