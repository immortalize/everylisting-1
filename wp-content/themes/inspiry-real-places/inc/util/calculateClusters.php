<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/inspiry-real-places/inc/util/lat-long-cluster-cache.php');

if (isset($_GET['cluster_cache_reset'])) {
    echo cluster_cache_reset($_GET['cluster_cache_reset']);
} else {
    $queries = array();
    parse_str($_SERVER['QUERY_STRING'], $queries);
    
    $extra = '';
    foreach ($queries as $key => $value) {
        if (substr($key,0,1) == '_') {
            $extra .= $key.'='.$value.';';
        }
    }
    $tolerance = isset($_GET['tolerance'])?$_GET['tolerance']:null;
    $max = isset($_GET['max'])?$_GET['max']:-1;
    $zoom = isset($_GET['_zoom'])?$_GET['_zoom']:null;
    $west = isset($_GET['west'])?$_GET['west']:null;
    $east = isset($_GET['east'])?$_GET['east']:null;
    $north = isset($_GET['north'])?$_GET['north']:null;
    $south = isset($_GET['south'])?$_GET['south']:null;
    $country = isset($_GET['country'])?$_GET['country']:null;
    $screendevice = isset($_GET['screendevice'])?$_GET['screendevice']:null;
    $dcline = isset($_GET['includesDCLine'])?$_GET['includesDCLine']:null;
    echo cluster_cache_global_json($tolerance, $max, $west, $east, $north, $south, $dcline, $extra, file_get_contents('php://input'), $zoom, $country, $screendevice);
}
?>
