<?php

define('WP_USE_THEMES', false);  
require_once('../../../../../wp-load.php');



if (isset($_GET['compound_id']) && !isset($_GET['id'])) {
	
	global $wpdb;
	$results = $wpdb->get_row( "select post_id, meta_key from $wpdb->postmeta where meta_value = '".$_GET['compound_id']."'" );

	if (!empty($results)) {
		
		$post_id = $results->post_id;
	} else {

		echo json_encode(array('Status' => false , 'message'=> "Invalid Compound ID" ));

		die;
	}
}


if (isset($_GET['id'])) {
	
	$post_id = $_GET['id'];
}

$the_query = new WP_Query(array('post_type' => 'property','p'=>$post_id)); 


$property_data = $the_query->posts;

$property_data[0]->post_content = htmlentities($property_data[0]->post_content);

$property = (array) $property_data[0];

$property_meta = get_post_meta($post_id);

foreach ($property_meta as $key => $value) {

	$property[$key] = $value[0];

}

$property['thumbnail_image'] = wp_get_attachment_url($property['_thumbnail_id']);

$attachments = get_children(array('post_parent' => $post_id,
                        'post_status' => 'inherit',
                        'post_type' => 'attachment',
                        'post_mime_type' => 'image',
                        'order' => 'ASC',
                        'orderby' => 'menu_order ID'));

foreach($attachments as $att_id => $attachment) {
    $property['images'][] = wp_get_attachment_url($attachment->ID);
}

    $taxonomies = get_object_taxonomies('property');
        
     $property['taxonomies'] = wp_get_post_terms( $post_id, $taxonomies);
     
if (isset($_GET['filter'])) {
    preg_match_all("/\"(".$_GET['filter'].")\":[ ]?\"(.*?)\"/", json_encode($property, JSON_PRETTY_PRINT), $out);
    $split = explode("|", $_GET['filter']);
    foreach ($split as $token) {
        if (is_array($property[$token])) {
            array_push($out[0], "\"".$token."\": ".json_encode($property[$token]));
        }
    }
    $copy = $out[0];
    echo "{ ";
    foreach ($out[0] as $one) {
        echo $one;
        if (next($copy )) {
            echo ", ";
        }
    }
    echo " }";
} else {
    echo json_encode($property);
}


