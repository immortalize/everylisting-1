<?php 

require_once('../../../../../wp-load.php');

// SELECT meta2.meta_value FROM wp_posts post JOIN wp_postmeta meta ON meta.post_id = post.ID 
// JOIN wp_postmeta meta2 ON meta2.post_id = post.ID WHERE post.post_type = 'property' AND meta.meta_key = 'IDXGenerator_Country' 
// AND meta.meta_value = 'Nigeria' AND meta2.meta_key = 'IDXGenerator_listing_agents_id'

if ((isset($_GET['post_type'])) && (isset($_GET['meta_key'])) && (isset($_GET['meta_value']))) {
    // SELECT meta2.meta_value FROM wp_posts post JOIN wp_postmeta meta ON meta.post_id = post.ID
    // JOIN wp_postmeta meta2 ON meta2.post_id = post.ID WHERE post.post_type = 'property' AND meta.meta_key = 'IDXGenerator_Country'
    // AND meta.meta_value = 'Nigeria' AND meta2.meta_key = 'IDXGenerator_listing_agents_id'
    global $wpdb;
    $stmt = 'SELECT meta2.meta_value FROM ' . $wpdb->posts . ' post JOIN ' . $wpdb->prefix . 'postmeta meta ON meta.post_id = post.ID JOIN ' . $wpdb->prefix . 'postmeta meta2 ON meta2.post_id = post.ID WHERE post.post_type = \'' . $_GET['post_type'] . '\' AND meta.meta_key = \'' . $_GET['meta_key'] .'\' AND meta.meta_value = \''. $_GET['meta_value'] .'\' AND meta2.meta_key = \'IDXGenerator_listing_agents_id\'';
    unset($_GET['post_type']);
    unset($_GET['meta_key']);
    unset($_GET['meta_value']);
    require_once('postInfo.php');
    $results = $wpdb->get_results($stmt);
    if ((isset($results)) && (isset($_GET['random']))) {
        shuffle($results);
        foreach($results as $result) {
            $tokens = explode("|", $result->meta_value);
            shuffle($tokens);
            foreach($tokens as $token) {
                if ($token != '') {
                    $args = array('post_type'	=> 'agent', 'meta_key' => 'IDXGenerator_realEstateBrokerId',
                        'meta_value'   => $token,
                        'meta_compare' => '=');
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()) {
                        $return = onePostInfo($the_query, $_GET['filter']);
                        if ((isset($_GET['mustphone'])) && ($_GET['mustphone'] == 'true')) {
                            preg_match_all("/\"(IDXGenerator_phone)\":[ ]?\"(.*+)\"/", $return, $out);
                            if (count($out) == 0) {
                                continue;
                            }
                        }
                        if (isset($_GET['language'])) {
                            preg_match_all("/\"IDXGenerator_language\":[ ]?\"([a-z]*)\"/", $return, $out);
                            if ((count($out) > 1) && (count($out[0]) > 0) && ($out[1][0] != $_GET['language'])) {
                                continue;
                            }
                        }
                        if (isset($_GET['visited'])) {
                            $visited = str_replace('\\"', '"', '{"res": ' . $_GET['visited']) . '}';
                            $arrayString = urldecode($visited);
                            $passed = true;
                            if (isset($arrayString)) {
                                $arr = json_decode($arrayString, true);
                                if ((isset($arr)) && (isset($arr['res']))) {
                                    foreach($arr['res'] as $value) {
                                        preg_match_all("/\"IDXGenerator_realEstateBrokerId\":[ ]?\"([0-9]*)\"/", $return, $out);
                                        if ((count($out) > 1) && (count($out[0]) > 0) && ($out[1][0] == $value)) {
                                            $passed = false;
                                        }
                                    }
                                    if (!$passed) {
                                        continue;
                                    }
                                }
                            }
                        }
                        echo $return;
                        wp_reset_postdata();
                        die;
                    }
                    wp_reset_postdata();
                }
            }
        }
    }
} else if (isset($_GET['id'])) {
    $post_id = $_GET['id'];
    unset($_GET['id']);
    require_once('postInfo.php');
    $the_query = new WP_Query(array('post_type'	=> 'agent', 'p'=>$post_id));
    $return = onePostInfo($the_query, $_GET['filter']);
    wp_reset_postdata();
    echo $return;
}

?>