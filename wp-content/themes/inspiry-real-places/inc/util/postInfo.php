<?php

require_once('../../../../../wp-load.php');

if (!function_exists('onePostInfo')) :
function onePostInfo($the_query, $filter) {        
    $post = $the_query->the_post();
    
    $currentId = get_the_ID();
    $post_id = $the_query->get_the_ID();
    
    $post_meta = get_post_custom($post_id);
    
    foreach ($post_meta as $key => $value) {
        if ($key == "delayed_images") {
            $post[$key] = str_replace("\"", "",$value[0]);
        } else {
            $post[$key] = $value[0];
        }
    }
    $post['id'] = $post_id;
    $post['permalink'] = get_permalink($post_id);
    $site = site_url('', 'http');
    $post['permalink'] = str_replace($site, "", $post['permalink']);
    $site = site_url('', 'https');
    $post['permalink'] = str_replace($site, "", $post['permalink']);
    $post['title'] = get_the_title($post_id);
    if (isset($post['_thumbnail_id'])) $post['thumbnail_image'] = wp_get_attachment_url($post['_thumbnail_id']);
    
    $attachments = get_children(array('post_parent' => $post_id,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'order' => 'ASC',
        'orderby' => 'menu_order ID'));
    
    foreach($attachments as $att_id => $attachment) {
        $post['images'][] = wp_get_attachment_url($attachment->ID);
    }
    
    $post['taxonomies'] = wp_get_post_terms($post_id);
            
    if (isset($filter)) {
        $count = 0;
        preg_match_all("/\"(".$filter.")\":[ ]?\"(.*?)\"/", json_encode($post, JSON_PRETTY_PRINT), $out);
        $split = explode("|", $filter);
        foreach ($split as $token) {
            if (is_array($post[$token])) {
                array_push($out[0], "\"".$token."\": ".json_encode($post[$token]));
            }
        }
        $copy = $out[0];
        $return = "{";
        foreach ($out[0] as $one) {
            $one = trim($one);
            if ((!endsWith($one, ":")) && (!endsWith($one, "\"\""))) {
                if ($count > 0) {
                    $return .= ", ";
                }
                $return .= str_replace("\/", "/", $one);
                $count++;
            }
        }
        unset($out);
        preg_match_all("/\"(".$filter.")\":[ ]?([0-9]*+)/", json_encode($post, JSON_PRETTY_PRINT), $out);
        $split = explode("|", $filter);
        foreach ($split as $token) {
            if (is_array($post[$token])) {
                array_push($out[0], "\"".$token."\": ".json_encode($post[$token]));
            }
        }
        $copy = $out[0];
        foreach ($out[0] as $one) {
            $one = trim($one);
            if ((!endsWith($one, ":")) && (!endsWith($one, "\"")) && (!endsWith($one, " "))) {
                if ($count > 0) {
                    $return .= ", ";
                }
                $return .= str_replace("\/", "/", $one);
                $count++;
            }
        }
        if ((strrpos($dReturn, ", \"id\":") == false) && (strrpos($dReturn, "{\"id\":") == false)) {
            if ($count > 0) {
                $return .= ", ";
            }
            $return .= ("\"id\": \"" . $currentId . "\"");
        }
        $return .= "}";
        return $return;
    } else {
        return json_encode($post);
    }
}
endif;

if (isset($_GET['id'])) {
    $post_id = $_GET['id'];
    $the_query = new WP_Query(array('p'=>$post_id));
    if ($the_query->have_posts()) {
        echo onePostInfo($the_query, $_GET['filter']);
    }
    wp_reset_postdata();
} else if ((isset($_GET['post_type'])) && (isset($_GET['meta_key'])) && (isset($_GET['meta_value']))) {
    $the_query = new WP_Query(array('post_type'	=> $_GET['post_type'],'meta_key' => $_GET['meta_key'],
        'meta_value'   => urldecode($_GET['meta_value']),
        'meta_compare' => '=')
        );
    if ($the_query->have_posts()) {
        echo onePostInfo($the_query, $_GET['filter']);
    }
    wp_reset_postdata();
}



