<?php
/*
 * Header Options
 */
global $opt_name;
Redux::setSection( $opt_name, array (
    'title' => esc_html__('Property Detail', 'inspiry'),
    'id'    => 'property-detail-section',
    'desc'  => esc_html__('This section contains options for property detail page.', 'inspiry'),
    'fields'=> array (

        array(
            'id'       => 'inspiry_property_header_variation',
            'type'     => 'radio',
            'title'    => esc_html__( 'Property Header Design Variation', 'inspiry' ),
            'options'  => array(
                '1' => esc_html__( 'Variation One - Simple slider with basic information on right side of slider.', 'inspiry' ),
                '2' => esc_html__( 'Variation Two - Thumbnail slider with basic information below slider.', 'inspiry' ),
                '3' => esc_html__( 'Variation Three - Horizontally scrollable slider with basic information below slider.', 'inspiry' ),
            ),
            'default'  => '1',
        ),

		/*
		 * Breadcrumbs
		*/
		array(
            'id'       => 'inspiry_property_breadcrumbs',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Breadcrumbs', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),

        /*
         * Description
         */
        array(
            'id'       => 'inspiry_property_description',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Description', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_desc_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Description Title', 'inspiry' ),
            'default'   => esc_html__( 'Description', 'inspiry' ),
            'required' => array( 'inspiry_property_description', '=', '1' ),
        ),


        /*
         * Additional Details
         */
        array(
            'id'       => 'inspiry_property_details',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Additional Details', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_details_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Additional Details Title', 'inspiry' ),
            'default'   => esc_html__( 'Additional Details', 'inspiry' ),
            'required' => array( 'inspiry_property_details', '=', '1' ),
        ),


        /*
         * Features
         */
        array(
            'id'       => 'inspiry_property_features',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Features', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_features_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Features Title', 'inspiry' ),
            'default'   => esc_html__( 'Features', 'inspiry' ),
            'required' => array( 'inspiry_property_features', '=', '1' ),
        ),

        /*
         * Floor Plans
         */
        array(
            'id'       => 'inspiry_property_floor_plans',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Floor Plans', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_floor_plans_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Floor Plans Title', 'inspiry' ),
            'default'   => esc_html__( 'Floor Plans', 'inspiry' ),
            'required' => array( 'inspiry_property_floor_plans', '=', '1' ),
        ),

        /*
         * Video
         */
        array(
            'id'       => 'inspiry_property_video',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Video', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_video_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Video Title', 'inspiry' ),
            'default'   => esc_html__( 'Property Video', 'inspiry' ),
            'required' => array( 'inspiry_property_video', '=', '1' ),
        ),

        /*
	     * Virtual Tour
	     */

	    array(
		    'id'       => 'inspiry_property_virtual_tour',
		    'type'     => 'switch',
		    'title'    => esc_html__( 'Property Virtual Tour', 'inspiry' ),
		    'default'  => 1,
		    'on'       => esc_html__( 'Show', 'inspiry' ),
		    'off'      => esc_html__( 'Hide', 'inspiry' ),
	    ),

        array(
            'id'        => 'inspiry_property_virtual_tour_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Virtual Tour Title', 'inspiry' ),
            'default'   => esc_html__( 'Virtual Tour', 'inspiry' ),
            'required' => array( 'inspiry_property_virtual_tour', '=', '1' ),
        ),

            /*
			 * Map
			 */
        array(
            'id'       => 'inspiry_property_map',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Map', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_map_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Map Title', 'inspiry' ),
            'default'   => esc_html__( 'Location', 'inspiry' ),
            'required' => array( 'inspiry_property_map', '=', '1' ),
        ),


        /*
         * Attachments
         */
        array(
            'id'       => 'inspiry_property_attachments',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Attachments', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_attachments_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Attachments Title', 'inspiry' ),
            'default'   => esc_html__( 'Attachments', 'inspiry' ),
            'required' => array( 'inspiry_property_attachments', '=', '1' ),
        ),


        /*
         * Share
         */
        array(
            'id'       => 'inspiry_property_share',
            'type'     => 'switch',
            'title'    => esc_html__( 'Share This Property', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_share_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Share This Property Title', 'inspiry' ),
            'default'   => esc_html__( 'Share This Property', 'inspiry' ),
            'required' => array( 'inspiry_property_share', '=', '1' ),
        ),


        /*
         * Children Properties
         */
        array(
            'id'       => 'inspiry_children_properties',
            'type'     => 'switch',
            'title'    => esc_html__( 'Children Properties', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_property_children_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Children Properties Title', 'inspiry' ),
            'default'   => esc_html__( 'Sub Properties', 'inspiry' ),
            'required' => array( 'inspiry_children_properties', '=', '1' ),
        ),


        /*
         * Property Comments
         */
        array(
            'id'       => 'inspiry_property_comments',
            'type'     => 'switch',
            'title'    => esc_html__( 'Property Comments', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),


        /*
         * Agent
         */
        array(
            'id'       => 'inspiry_property_agent',
            'type'     => 'switch',
            'title'    => esc_html__( 'Agent Information', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'       => 'inspiry_agent_contact_form',
            'type'     => 'switch',
            'title'    => esc_html__( 'Agent Contact Form', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
            'required' => array( 'inspiry_property_agent', '=', '1' ),
        ),
        array(
            'id'        => 'inspiry_agent_contact_form_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Agent Contact Form Title', 'inspiry' ),
            'default'   => esc_html__( 'Contact Agent', 'inspiry' ),
            'required' => array( 'inspiry_agent_contact_form', '=', '1' ),
        ),


        /*
         * Similar Properties
         */
        array(
            'id'       => 'inspiry_similar_properties',
            'type'     => 'switch',
            'title'    => esc_html__( 'Similar Properties', 'inspiry' ),
            'default'  => 1,
            'on'       => esc_html__( 'Show', 'inspiry' ),
            'off'      => esc_html__( 'Hide', 'inspiry' ),
        ),
        array(
            'id'        => 'inspiry_similar_properties_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Similar Properties Title', 'inspiry' ),
            'default'   => esc_html__( 'Similar Properties', 'inspiry' ),
            'required' => array( 'inspiry_similar_properties', '=', '1' ),
        ),
        array(
            'id'       => 'inspiry_similar_properties_number',
            'type'     => 'select',
            'title'    => esc_html__( 'Maximum number of similar properties', 'inspiry' ),
            'options'  => array(
                1  => '1',
                2  => '2',
                3  => '3',
                4  => '4',
                5  => '5',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'inspiry_similar_properties', '=', '1' ),
        ),
	    array(
		    'id'      => 'inspiry_similar_properties_filter',
		    'type'    => 'checkbox',
		    'title'   => esc_html__( 'Show similar properties based on', 'inspiry' ),
		    'options' => array(
			    'property-city'    => esc_html__( 'Property Location', 'inspiry' ),
			    'property-status'  => esc_html__( 'Property Status', 'inspiry' ),
			    'property-type'    => esc_html__( 'Property Type', 'inspiry' ),
			    'property-feature' => esc_html__( 'Property Feature', 'inspiry' ),

		    ),
		    'default' => array(
			    'property-city'    => '1',
			    'property-status'  => '0',
			    'property-type'    => '1',
			    'property-feature' => '0',
		    ),
		    'required' => array( 'inspiry_similar_properties', '=', '1' ),
	    ),
	    array(
		    'id'      => 'inspiry_similar_properties_sorting',
		    'type'    => 'radio',
		    'title'   => esc_html__( 'Sort Similar Properties By', 'inspiry' ),
		    'default' => 'random',
		    'options' => array(
			    'time'         => esc_html__( 'Time - Recent First', 'inspiry' ),
			    'price-htl'    => esc_html__( 'Price - High to Low', 'inspiry' ),
			    'property-lth' => esc_html__( 'Proce - Low to High', 'inspiry' ),
			    'random'       => esc_html__( 'Random', 'inspiry' ),
		    ),
		    'required' => array( 'inspiry_similar_properties', '=', '1' ),
	    )
    )
) );
