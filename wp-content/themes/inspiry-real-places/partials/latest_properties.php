<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/latest_properties.php'); ?>

<div id="homeProperties" class="property-listing-two">
	<div class="col-md-4 col-xs-7">
		<h1 class="font90">Latest Properties</h1>
	</div>
	<div class="newContainer">
        <?php

        $shortcodesearchstyle = true;

        if (!$shortcodesearchstyle) {
            if (isset($_POST['iteration'])) {
                $iteration = (int) $_POST['iteration'];
                $iteration ++;
            } else {
                $iteration = 1;
            }
            if (isset($_POST['offset'])) {
                $offset = $_POST['offset'];
                $passOffset = $iteration * 16;
            } else {
                $offset = '-1';
                $passOffset = 16;
            }
        }

        if (isset($_GET['category'])) {
            $Property_type = array(
                '1' => 'House',
                '2' => 'House',
                '3' => 'House',
                '4' => 'Apartment',
                '5' => 'Apartment',
                '6' => 'Apartment',
                '7' => 'Local',
                '8' => 'Local',
                '9' => 'Office',
                '10' => 'Office',
                '11' => 'Land',
                '12' => 'Land',
                '13' => 'Country-House',
                '14' => 'Country-House',
                '15' => 'Country-House',
                '16' => 'Building',
                '17' => 'Land',
                '18' => 'House',
                '19' => 'Apartment',
                '20' => 'Apartment',
                '21' => 'Multi-Familial House',
                '22' => 'Multi-Familial House',
                '23' => 'Other',
                '24' => 'Other',
                '25' => 'Warehouse',
                '26' => 'Warehouse',
                '27' => 'Building',
                '28' => 'Uncategorized'
            );

            $Property_status = array(
                '1' => 'For Sale',
                '2' => 'For Rent',
                '2' => 'For Vacation Rental Use',
                '4' => 'For Sale',
                '5' => 'For Rent',
                '6' => 'For Vacation Rental Use',
                '7' => 'For Sale',
                '8' => 'For Rent',
                '9' => 'For Sale',
                '10' => 'For Rent',
                '11' => 'For Sale',
                '12' => 'For Rent',
                '13' => 'For Sale',
                '14' => 'For Rent',
                '15' => 'For Vacation Rental Use',
                '16' => 'For Sale',
                '17' => 'For Sale',
                '18' => 'For Rent',
                '19' => 'For Sale',
                '20' => 'For Rent',
                '21' => 'For Sale',
                '22' => 'For Sale',
                '23' => 'For Sale',
                '24' => 'For Rent',
                '25' => 'For Sale',
                '26' => 'For Rent'
            );

            if (array_key_exists($_GET['category'], $Property_type)) {
                $valueoftype = $Property_type[$_GET['category']];
            }
            if (array_key_exists($_GET['category'], $Property_status)) {
                $valueofstatus = $Property_status[$_GET['category']];
            }
        }

        global $wpdb;
        $tbl = $wpdb->prefix . 'postmeta';
        $prepare_guery = $wpdb->get_results("SELECT * FROM " . $tbl . " where meta_key ='IDXGenerator_searchcriteria_value' ");

        $sector = '';
        $city = '';
        $province = '';
        foreach ($prepare_guery as $key => $valueofsearch) {
            $val = unserialize($valueofsearch->meta_value);
            if ($valueofsearch->post_id == $id) {
                if (isset($val['IDXGenerator_City']) && $val['IDXGenerator_City'] != '') {
                    $city = $val['IDXGenerator_City'];
                    $province = '';
                    $country = '';
                }
                if (isset($val['IDXGenerator_Province']) && $val['IDXGenerator_Province'] != '') {
                    $province = $val['IDXGenerator_Province'];
                    $country = '';
                }
                if (isset($val['IDXGenerator_Sector']) && $val['IDXGenerator_Sector'] != '') {
                    $sector = $val['IDXGenerator_Sector'];
                    $city = '';
                    $province = '';
                    $country = '';
                }
            }
        }
        $juristype = get_post_meta($id, 'IDXGenerator_jurisdiction_type', true);
        switch ($juristype) {
            case "COUNTRY":
                $country = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
                break;
            case "STATEORPROVINCE":
                $province = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
                break;
            case "CITY":
                $city = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
                break;
            case "SECTOR":
                $sector = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
                break;
        }
        
        $searchTerms = urlencode(((strlen($sector) > 0) ? ($sector . ' ') : '') . ((strlen($city) > 0) ? ($city . ' ') : '') . ((strlen($province) > 0) ? ($sector . ' ') : '') . $country);

        if ($shortcodesearchstyle) {
            ?>
                <div class="col-md-12 " style="text-align: center;">
    				<div class="alm-btn-wrap">
                        <?php
                        if (is_home()) {
                            echo '<script>mainPagePropertiesLoading();</script>';
                        } else {
                            $catinsert = ((isset($_GET['category'])) ? (' category__and="' . $_GET['category'] . '"') : '');
                        ?>
                        <script>
                        	var template = "<div class = 'col-md-12' style='text-align: center;'> \
      		                <a name='properties_list'> \
      		                    <div id='ajax-load-more' class='ajax-load-more-wrap blue alm-0 alm-loading' data-alm-id='0' data-canonical-url='/news/' data-slug='home' data-post-id='0' data-localized='ajax_load_more_vars'> \
      		                        <div class='alm-btn-wrap' style='visibility: visible;'> \
      		                            <button class='alm-load-more-btn more loading' rel='next'>Searching Properties</button> \
      		                        </div> \
      		                        <div class='alm-no-results' style='display: none;'>No Property Found</div> \
      		                    </div> \
      		                </a> </div>";
      		            	$("#aftersearchshow").html(template);
      		            	var countryInfo = "<?php echo str_replace("+", " ", $country);?>";
      		            	var searchTerms = "<?php echo str_replace("+", " ", $searchTerms);?>";
      		            	var jsonstring = "{";
          		            <?php
          		            if ((isset($country)) && ($country != '')) echo "jsonstring += \",'country': '" . $country . "'\";";
          		            if ((isset($province)) && ($province != '')) echo "jsonstring += \",'state': '" . $province . "'\";";
          		            if ((isset($city)) && ($city != '')) echo "jsonstring += \",'city': '" . $city . "'\";";
          		            if ((isset($sector)) && ($sector != '')) echo "jsonstring += \",'sector': '" . $sector . "'\";";
          		            if (isset($_GET['category'])) echo "jsonstring += \",'catid': '" . $_GET['category'] . "'\";";
          		            ?>
      		            	jsonstring += "}";
      		            	jsonstring = jsonstring.replace("{,'", "{'");
      		            	jQuery.ajax({
          		                method:'POST',
          		                url: '/wp-admin/admin-ajax.php',
          		                data: {action: 'folder_contents', json:jsonstring},
          		                success: function(res) {
          		                  jQuery('#countryheadingshow').html(countryInfo); 
          		                  $("#aftersearchshow").html(res);
          		                }
          		            });
                        </script>
                        <!-- SEARCHTERMS: <?php echo $searchTerms; ?> -->
						<?php
                            //echo do_shortcode('[ajax_load_more post_type="property" search="' . str_replace("+", " ", $searchTerms) . '"' . $catinsert . ' orderby="date" posts_per_page="20" scroll="true" css_classes="plain-text" sticky_posts="true" progress_bar="true" progress_bar_color="007000" button_label="Show More Properties" button_loading_label="Searching Properties" no_results_text="No Property Found"]');
                        }
                        ?>
                	</div>
    			</div>
    			<div class="newContainer" id="aftersearchshow">
            <?php
        } else {
            $properties_search_arg = array(
                'post_type' => 'property',
                'offset' => $offset,
                'posts_per_page' => 20,
                'meta' => $country,
                'ignore_sticky_posts' => true,
                'meta_query' => array(
                    array(
                        'key' => 'IDXGenerator_Country',
                        'value' => $country
                    ),
                    array(
                        'key' => 'IDXGenerator_City',
                        'value' => $city
                    ),
                    array(
                        'key' => 'IDXGenerator_Province',
                        'value' => $province
                    ),
                    array(
                        'key' => 'IDXGenerator_Sector',
                        'value' => $sector
                    )
                )
            );

            $properties_search_query = new WP_Query(apply_filters('inspiry_property_search', $properties_search_arg));

            $ids = '';
            $noimageids = '';

            $show_btn = 'false';
            if ($properties_search_query->have_posts()) {
                echo '<!-- $id have_posts() to true -->';
                $found_properties = 0;
                $properties_count = 1;
                $columns_count = 3; // todo: add the ability to change number of columns between 2, 3, 4

                while ($properties_search_query->have_posts()) :
                    $properties_search_query->the_post();
                    $custom = get_post_custom();
                    $home_property = new Inspiry_Property(get_the_ID());
                    if (true) {
                        $id = get_the_ID();
                        if (has_post_thumbnail()) {
                            if (strlen($ids) == 0) {
                                $ids = $id;
                            } else {
                                $ids = $ids . "," . $id;
                            }
                            $address = $home_property->get_address();
                            $country = $home_property->get_property_meta('IDXGenerator_Country');
                            $first_type_term = $home_property->get_taxonomy_first_term('property-type', 'all');
                            $first_status_term = $home_property->get_taxonomy_first_term('property-status', 'all');
                            
                            /*
                             * echo '<!-- $id $first_type_term: '. $first_type_term . ' -->';
                             * echo '<!-- $id $first_status_term: '. $first_status_term . ' -->';
                             * echo '<!-- $id $valueofstatus: '. $valueofstatus . ' -->';
                             */
                            
                            if (((!isset($valueoftype)) && (!isset($valueofstatus))) || ((((isset($valueoftype)) && ($first_type_term->name == $valueoftype)) && ((isset($valueofstatus)) && ($first_status_term->name == $valueofstatus))) || (!isset($_GET['category'])))) {
                                // if(true){
                                $show_btn = 'true';
                                ?>
    						<div class="homepropBoxes thumbnail-size">
                               <?php new_property_list_insert(get_the_ID()); ?>
                            </div>
                            <?php
                            }
                        } else {
                            $found_properties--;
                            if (strlen($noimageids) == 0) {
                                $noimageids = $id;
                            } else {
                                $noimageids = $noimageids . "," . $id;
                            }
                        }
                    }
                    $properties_count++;
                endwhile;
                wp_reset_postdata();
            }
            if ($show_btn == 'true') {
                if (strlen($noimageids) > 0) {
                    if (strlen($ids) == 0) {
                        $ids = $noimageids;
                    } else {
                        $ids = $ids . "," . $noimageids;
                    }
                }
                ?>
        <div class="col-md-12 loadmorediv" style="text-align: center;">
			<div class="alm-btn-wrap"></div>
		</div>
        <?php } else { ?>
            <div class="col-md-12 " style="text-align: center;">
			<div class="alm-btn-wrap">
				<button class="alm-load-more-btn more "
					style="background: #0073aa !important;">No Property Found</button>
			</div>
		</div>
        <?php
            }
        }
        ?>    
    </div>
</div>
