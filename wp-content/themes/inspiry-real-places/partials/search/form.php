<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/search/form.php'); ?>

<?php
global $inspiry_options;
if ( function_exists( 'ire_properties_search_form' ) ) {
	ire_properties_search_form( $inspiry_options );
}
