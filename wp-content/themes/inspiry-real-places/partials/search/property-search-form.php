<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/search/property-search-form.php'); ?>

<?php
global $inspiry_options;

	$inspiry_options_copy = array_merge(array(), $inspiry_options);
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['keyword'] = esc_html__( 'Keyword', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['location'] = esc_html__( 'Location', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['type'] = esc_html__( 'Type', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-beds'] = esc_html__( 'Min Beds', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-baths'] = esc_html__( 'Min Baths', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-price'] = esc_html__( 'Min Max Price', 'inspiry' );
                                    $inspiry_options_copy['inspiry_search_fields']['enabled']['min-max-area'] = esc_html__( 'Min Max Area', 'inspiry' );
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['status']);
                                    unset($inspiry_options_copy['inspiry_search_fields']['enabled']['property-id']);
                                    $inspiry_options_copy['inspiry_search_fields']['default']['status'] = "For Sale";
                                    ire_properties_search_parameters_form($inspiry_options_copy, "propertysearch", "");
	?>
<script>
	$(document).ready(function () {
		console.log("*** ensureLocationsLoaded");
		ensureLocationsLoaded("propertysearch", true, "");
	})
</script>



