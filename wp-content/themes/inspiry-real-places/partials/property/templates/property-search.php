<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/templates/property-search.php'); ?>

<?php
/*
 * Google Map or Banner
 */
global $inspiry_options;

$skipsearch = false;
$skipmaps = true;
if (isset($_GET['showmap']) && ! empty($_GET['showmap'])) {
    $skipmaps = $_GET['showmap'] != 'true';
}

if (isset($_GET['showsearch']) && ! empty($_GET['showsearch'])) {
    $skipsearch = $_GET['showsearch'] != 'true';
}

if ($inspiry_options['inspiry_search_module_below_header'] == 'google-map') {
    if (! $skipmaps) {
        get_template_part('partials/header/map');
    }
} else {
    // Image Banner
    get_template_part('partials/header/banner');
}?>
<?php 		
//  Property page advanced search
?>

<div id="PropertySearchBox" class="PropertySearchBox" style="padding-top: 55px;">
		<div class="container-fluid">
		<div class="row">
        <?php
        get_template_part('partials/search/property-search-form');
		get_template_part('partials/search/search-update-btn');
        ?>
		</div>
		</div>
</div>

<?php
$search_layout = $inspiry_options['inspiry_search_layout'];

if ((! $skipsearch) && (('grid' == $search_layout) || ('list' == $search_layout))) {
    ?>
<div class="wrapper-search-form-two">
		<?php
    if ($inspiry_options['inspiry_home_search'] && $inspiry_options['inspiry_header_variation'] != '1') {
        // get_template_part( 'partials/home/search' );
    }
    ?>
	</div>
<?php
}
?>
<div id="content-wrapper" class="">
	<br />
	<br />
	<div id="content" class="layout-boxed">

		<div class="container">
<?php get_template_part( 'partials/property/templates/compare', 'view' ); ?>
			<div class="row">

				<div class="site-main-content">

					<main id="main" class="site-main">
					<div class="container">
						<?php
    global $inspiry_options;

    // Number of properties to display
    $number_of_properties = intval($inspiry_options['inspiry_search_properties_number']);
    if (! $number_of_properties) {
        $number_of_properties = 6;
    }

    // Current Page
    global $paged;
    if (is_front_page()) {
        $paged = (get_query_var('page')) ? get_query_var('page') : 1;
    }

    // Basic query arguments
    $properties_search_arg = array(
        'post_type' => 'property',
        'posts_per_page' => 1000,
        'paged' => $paged
    );

    // Apply search filter
    $properties_search_arg = apply_filters('inspiry_property_search', $properties_search_arg);

    // Apply sorting filter
    $properties_search_arg = apply_filters('inspiry_sort_properties', $properties_search_arg);

    // Create custom query
    $properties_search_query = new WP_Query($properties_search_arg);

    if ('list-sidebar' == $search_layout || 'grid-sidebar' == $search_layout) {
        echo '<div class="row"><div class="col-md-9">';
    }

    /*
     * Found properties heading and sorting controls
     */
    global $found_properties;
    $found_properties = $properties_search_query->found_posts;

    /*
     * Properties
     */
    $ids = '';
    if ($properties_search_query->have_posts()) {

        global $property_list_counter;
        $property_list_counter = 1;

        /*
         * if ( $search_layout == 'grid' || $search_layout == 'grid-sidebar' ) {
         * echo '<div class="row">';
         * }
         */

        $noimageids = "";

        while ($properties_search_query->have_posts()) :

            $properties_search_query->the_post();
            $id = get_the_ID();
            if (has_post_thumbnail()) {
                if (strlen($ids) == 0) {
                    $ids = $id;
                } else {
                    $ids = $ids . "," . $id;
                }
            } else {
                $found_properties --;
                if (strlen($noimageids) == 0) {
                    $noimageids = $id;
                } else {
                    $noimageids = $noimageids . "," . $id;
                }
            }
            /*
             * if ( 'list-sidebar' == $search_layout ) {
             * // display property in list layout with sidebar
             * get_template_part( 'partials/property/templates/property-for-list-with-sidebar' );
             * } else if ( 'grid-sidebar' == $search_layout ) {
             * // display property in grid layout with sidebar
             * get_template_part( 'partials/property/templates/property-for-grid-with-sidebar' );
             * } else if ( 'grid' == $search_layout ) {
             * // display property in grid layout
             * get_template_part( 'partials/property/templates/property-for-grid' );
             * } else {
             * // display property in list layout
             * get_template_part( 'partials/property/templates/property-for-list' );
             * }
             */

            $property_list_counter ++;
        endwhile;

        wp_reset_postdata();

        if ($found_properties == 0) {
            if (strlen($noimageids) > 0) {
                $found_properties = $properties_search_query->found_posts;
                if (strlen($ids) == 0) {
                    $ids = $noimageids;
                } else {
                    $ids = $ids . "," . $noimageids;
                }
            }
        }

        get_template_part('partials/property/templates/listing-control');

        echo '<div class="row col-12 mw-100">';

        // echo '<a name="properties_list"/>';

        /*
         * if ( $search_layout == 'grid' || $search_layout == 'grid-sidebar' ) {
         * echo '</div>';
         * }
         */
        
        ?>
        <!-- ids passed to shortcode:
        
        <?php print_r($ids); ?>
        
         -->
        <?php

        // inspiry_pagination( $properties_search_query );
        echo '<div class="col-xs-12 col-md-12 col-lg-12">';
        echo '<div class="alm-listing" id="aftersearchshow">';
        echo '<div class="loadmorediv" style="text-align: center;">';

        echo do_shortcode('[ajax_load_more post_type="property" post__in="' . $ids . '" orderby="relevance" posts_per_page="20" scroll="true" css_classes="plain-text" sticky_posts="true" progress_bar="true" progress_bar_color="007000" button_label="Show more properties" button_loading_label="Searching Properties" no_results_text="No Property Found"]');

        echo '</div>';
        echo '</div>';
        echo '</div>';
    } else {
        get_template_part('partials/property/templates/listing-control');
    }

    if (('list-sidebar' == $search_layout || 'grid-sidebar' == $search_layout) && is_active_sidebar('properties-list')) {
        echo '</div><div class="col-md-3 site-sidebar-content">';
        get_sidebar('properties-list');
        echo '</div></div>'; // .site-sidebar-content & .row
    }

    ?>
</div>
					</main>
					<!-- .site-main -->

				</div>
				<!-- .site-main-content -->

			</div>
			<!-- .row -->

		</div>
		<!-- .container -->

	</div>
	<!-- .site-content -->

</div>
<!-- .site-content-wrapper -->