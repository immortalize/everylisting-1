<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/templates/archive.php'); ?>

<?php
/*
 * Template for property archives
 */

get_header();

/*
 * Google Map or Banner
 */
global $inspiry_options;
if ( $inspiry_options[ 'inspiry_archive_module_below_header' ] == 'google-map' ) {
    // Image Banner
    get_template_part( 'partials/header/map' );
} else {
    // Image Banner
    get_template_part( 'partials/header/banner' );
}

    global $wp;
    $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    $country1 = $uriSegments[2];
    $statustype = $uriSegments[1];
    $porpetytype = $country1; //str_replace("-"," ",$country1);
?>
<div id="content-wrapper" class="site-content-wrapper site-pages">
<div id="moreloadidof" class="moreloaddivcls">&nbsp;</div>  
<div id="homeProperties" class="property-listing-two">
        <div style='float:left; padding-left:40px; width: 50%;'>
            <h1 class="font90"> 
            <?php if($statustype == 'property-type'){ ?>
            Latest <?php echo str_replace("-"," ",$porpetytype); ?>
            <?php }else if($statustype == 'property-status'){ ?>
            Latest PROPERTIES <?php echo str_replace("-"," ",$porpetytype); ?>
            <?php } ?>
            
            </h1>
        </div>
        <div>
        </div>
<div class="newContainer" id="aftersearchshow">
        <?php 
        //col-xs-7
    if(isset($_POST['iteration'])){
		$iteration = (int)$_POST['iteration'];
		$iteration++;
	}else{
		$iteration = 1;
	}

    if(isset($_POST['offset'])){
    	$offset = $_POST['offset'];
    	$passOffset = $iteration*16;
    }else{
    	$offset = '-1';
    	$passOffset = 16;
    }
 
    //$propertytype = get_post_meta('IDXGenerator_PropertyTypeDescriptor', true);
   
     $taxquery = '';
     if(isset($statustype) && $statustype != ''){
         
            $porpetytype = $porpetytype;
            $statustype = $statustype;
            if($statustype == 'property-type'){
                 $taxquery =  array(
                        array(
                         'taxonomy' => 'property-type',
                         'field'    => 'slug',
                         'terms'    => $porpetytype 
                         ),
                );
            }else if($statustype == 'property-status'){
                 $taxquery =  array(
                        array(
                         'taxonomy' => 'property-status',
                         'field'    => 'slug',
                         'terms'    => $porpetytype 
                         ),
                );
            }
           
            
            
     }else{
         $porpetytype = '';
     }
    
	$properties_search_arg = array(
	    
	    'post_type' => 'property',
	    'orderby'   => 'ID',
	    'order' => 'DESC',
	    'posts_per_page' => 16,
	    'offset' => $offset,
	    'meta' => $country,
        'meta_query' => array(
            array(
               'key' => '_thumbnail_id',
               'compare' => 'EXISTS',
           ),
        ),
        'tax_query' => $taxquery,
	);

	$properties_search_query = new WP_Query( apply_filters( 'inspiry_property_search', $properties_search_arg )  );
	

	$show_btn = 'false';
    if ( $properties_search_query->have_posts() ) : 
            $properties_count = 1;
            $columns_count = 4;   // todo: add the ability to change number of columns between 2, 3, 4

            while ( $properties_search_query->have_posts() ) :
                $properties_search_query->the_post();
                if ( has_post_thumbnail() ) {
                    $custom = get_post_custom();
                    $home_property = new Inspiry_Property( get_the_ID() );
                    $address = $home_property->get_address();
                    $country = $home_property->get_property_meta( 'IDXGenerator_Country' ); 
                    $propety = $home_property->get_property_meta( 'IDXGenerator_Country' ); 
                    
                    $first_type_term = $home_property->get_taxonomy_first_term( 'property-type', 'all' );
                    $first_status_term = $home_property->get_taxonomy_first_term( 'property-status', 'all' );
                    
                    ?>
                    
                     <div class='col-lg-3'>
                        <div class="homepropBoxes thumbnail-size">
                        	<?php new_property_list_insert(get_the_ID()); ?>
                        </div>
                    </div>
                    <?php 
                }
                $properties_count++;
            endwhile;?>
            
        
        <div class = "col-md-12 loadmorediv"  style="text-align: center;">
        	<div class="alm-btn-wrap">
        		<button class="alm-load-more-btn more loadmore"   style="background: #0073aa !important;" data-statustype='<?php echo $statustype; ?>' data-propertytype='<?php echo $porpetytype; ?>'  data-country = ""  data-offset = "<?=$passOffset?>" data-iteration = "<?=$iteration?>" rel="next">Show More Propties</button>
        	</div>
        </div>
        <?php
        else: ?>
        	<div class = "col-md-12 " style="text-align: center;">
        		<div class="alm-btn-wrap">
        			<button class="alm-load-more-btn more " style="background: #0073aa !important;">No Property Found</button>
        		</div>
        	</div>
        <?php
         endif;
         wp_reset_postdata();
         ?>
 </div>
</div>
</div>

<?php
get_footer();
?>

