<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/agent-information.php'); ?>

<?php
global $inspiry_options;
global $inspiry_single_property;

$curpostid = get_the_ID();

/* get agents ids */
$listing_agents_ids = $inspiry_single_property->get_agents_ids();

if ((!isset($listing_agents_ids)) || (!is_array($listing_agents_ids))) {
    $listing_agents_ids = array();
}

echo "<!-- *** listing_agents_ids 1: ";
print_r($listing_agents_ids);
echo " -->";

$idx_listing_agentidstr = get_post_meta($curpostid, 'IDXGenerator_listing_agents_id');
echo "<!-- *** idx_listing_agentidstr: ";
echo $idx_listing_agentidstr[0];
echo " -->";
if ((isset($idx_listing_agentidstr)) && ($idx_listing_agentidstr[0] != "")) {
    $idx_listing_agentids = explode("|", $idx_listing_agentidstr[0]);
    echo "<!-- *** idx_listing_agentids: ";
    print_r($idx_listing_agentids);
    echo " -->";
    if (count($idx_listing_agentids) > 0) {
        foreach($idx_listing_agentids as $idx_listing_agentid) {
            if ($idx_listing_agentid != "") {
                $args = array(
                    'post_type' => 'agent',
                    'meta_key' => 'IDXGenerator_listing_agents_id',
                    'meta_value' => $idx_listing_agentid,
                    'meta_compare' => '='
                );
//                 echo "<!-- *** args: ";
//                 print_r($args);
//                 echo " -->";
                $query = new WP_Query($args);
//                 echo "<!-- *** query: ";
//                 print_r($query);
//                 echo " -->";
                while ($query->have_posts()) {
                    $query->the_post();
                    array_push($listing_agents_ids, get_the_ID());
                }
                wp_reset_postdata();
            }
        }
    }
}

echo "<!-- *** listing_agents_ids 2: ";
print_r($listing_agents_ids);
echo " -->";

$agents_ids = array_merge($listing_agents_ids, get_post_meta($curpostid, 'cobroker_agentid', false));

echo "<!-- *** agents_ids 1: ";
print_r($agents_ids);
echo " -->";

$agents_ids = array_unique($agents_ids);

echo "<!-- *** agents_ids 2: ";
print_r($agents_ids);
echo " -->";

// console_array($inspiry_single_property);
// $agents_ids = filter_agent_ids(get_the_ID(), $agents_ids);

echo "<!-- *** agents_ids 3: ";
print_r($agents_ids);
echo " -->";

$agents_count = count($agents_ids);

console("got " . $agents_count . " agent(s)");

if ( 0 < $agents_count ) {

    $agent_ndx = 0;
    
    ?>
    <script>
    var displayedagentndx = -1;
    
    var clickAgentExpandCollapsefn = function(event) {
    	event.preventDefault();
        if (displayedagentndx != -1) {
        	$('#agentarrow' + displayedagentndx).addClass("fa-angle-right").removeClass("fa-angle-down");
        	$('#agentcontent' + displayedagentndx).hide();
        	if (event.data.toggle == displayedagentndx) {
        		displayedagentndx = -1;
            	return false;
        	}
        	displayedagentndx = -1;
        }
        $('#agentarrow' + event.data.toggle).removeClass("fa-angle-right").addClass("fa-angle-down");
    	$('#agentcontent' + event.data.toggle).show();
    	displayedagentndx = event.data.toggle;
    	return false;
    }
    </script>
    <?php
    
    $print_content = "";
    $print_count = 0;
    $print_content = "<h4 class='fancy-title'>Contact Information</h4><table border='0' style='width:100%; min-width:100%'><tr>";
    
	foreach( $agents_ids as $agent_id ) {

		if ( $agent_id > 0 ) {
		    
		    if ($agent_ndx > 0) {
		        echo '<hr/>';
		    }

			$inspiry_agent = new Inspiry_Agent( $agent_id );
			$agent_post = get_post( $agent_id );
			if (is_object($agent_post)) {
			    
			    $agent_print = '';
			    
    			$agent_permalink = get_permalink( $agent_id );
    			// $is_private = (strpos(get_inspiry_custom_excerpt( $agent_post->post_content ), 'This information should never become customer-facing.') !== false);
    			$is_private = true;
    			$custom = get_post_custom($agent_id);
    			echo '<!-- get_post_custom (2): ';
    			print_r($custom);
    			echo ' -->';
    			?>
    			<div class="inner-wrapper clearfix">
    
    				<?php
    				if ($is_private) { ?>
    						<div id="collapseexpand<?php echo $agent_ndx; ?>" style="cursor: pointer">
    				<?php }
    				if (has_post_thumbnail($agent_id)) {
    					?>
    					<figure class="agent-image">
    						<?php echo get_the_post_thumbnail($agent_id, 'inspiry-agent-thumbnail', array('class' => array('img-circle'))); 
    						$agent_print .= get_the_post_thumbnail($agent_id, 'inspiry-agent-thumbnail', array('class' => array('img-circle'))) . '<br/>';
    						?>
    					</figure>
    					<?php
    				} else if (isset($custom['user_id'])) {
    				    ?>
    					<figure class="agent-image">
    						<?php 
    						// avatar avatar-96 wp-user-avatar wp-user-avatar-96 alignnone photo
    						$content = get_avatar($custom['user_id'][0]);
    						$content = preg_replace('/class=".*?"/', 'class="img-circle"', $content);
    						$agent_print .= $content . '<br/>';
                			?>
    						<?php echo $content ?>
    					</figure>
    					<?php
    				}
    				?>
    
    				<h3 class="agent-name">
    				    <!--sse-->
    					<?php if (!$is_private) {?>
    						<a href="<?php echo esc_url( $agent_permalink ); ?>">
    					<?php }?>
    					<?php echo esc_html( $agent_post->post_title ); 
    					$agent_print .= esc_html( $agent_post->post_title ) . '<br/>';
    					?>
    					<?php if (!$is_private) {?>
    						</a>  
    					<?php }
    				    if (!$is_private) { ?>
    						<a id="collapseexpand<?php echo $agent_ndx; ?>" href="#" style="cursor: pointer;">
    					<?php } ?>
    					<i id='agentarrow<?php echo $agent_ndx; ?>' class='fa fa-angle-right pull-right fa-2x float-right'></i>
    					<?php
    					if (!$is_private) { ?>
    						</a>
    					<?php }
    					$agent_job_title = $inspiry_agent->get_job_title();
    					if (( !empty( $agent_job_title ) ) && ($agent_post->post_title != $agent_job_title)) {
    						?><span><?php echo esc_html( $agent_job_title ); ?></span><?php
    						$agent_print .= esc_html( $agent_job_title ) . '<br/>';
    					}
    					?>
    					<!--/sse-->
    				</h3>
    				
    				<script>$('#collapseexpand<?php echo $agent_ndx; ?>').click({toggle:'<?php echo $agent_ndx; ?>'}, clickAgentExpandCollapsefn);</script>
    				    
    				<div class="agent-social-profiles">
    				    <!--sse-->
    					<?php
    					if ( function_exists( 'ire_agent_social_profiles' ) ) {
    						ire_agent_social_profiles( $inspiry_agent );
    					}
    					?>
    					<!--/sse-->
    				</div>
    				
    				<?php
    				if ($is_private) { ?>
    						</div>
    				<?php }?>
    
    			</div>
    			
    			<div id="agentcontent<?php echo $agent_ndx; ?>" style="display: none;">
    
    			<?php $agent_print .= '<ul class="agent-contacts-list">'; ?>
    			<ul class="agent-contacts-list">
    			     <!--sse-->
    				<?php
    				$template_svg_path = get_template_directory() . '/images/svg/';
    
    				/*
    				 * Mobile
    				 */
    				$mobile = $inspiry_agent->get_mobile();
    				if (($is_private) && (empty($mobile)) && (isset($custom['IDXGenerator_phone']))) {
    				    $mobile = $custom['IDXGenerator_phone'][0];
    				}
    				if ( !empty( $mobile ) ) {
    					?><li class="mobile"><?php include( $template_svg_path . 'icon-mobile.svg' ); ?><span><?php esc_html_e( 'Mobile:', 'inspiry' ); ?></span><?php echo esc_html( $mobile ); ?></li><?php
    					$agent_print .= ("<li class='mobile'>Mobile: <span>" . esc_html_e("Mobile:", "inspiry") . "</span>" . esc_html($mobile) . "</li>");
    				}
    
    				/*
    				 * Office
    				 */
    				$office_phone = $inspiry_agent->get_office_phone();
    				if ( !empty( $office_phone ) ) {
    					?><li class="office"><?php include( $template_svg_path . 'icon-phone.svg' ); ?><span><?php esc_html_e( 'Office:', 'inspiry' ); ?></span><?php echo esc_html( $office_phone ); ?></li><?php
    					$agent_print .= ("<li class='office'>Office: <span>" . esc_html_e( 'Office:', 'inspiry' ) . "</span>" . esc_html( $office_phone ) . "</li>");
    				}
    
    				/*
    				 * Fax
    				 */
    				$fax = $inspiry_agent->get_fax();
    				if ( !empty( $fax ) ) {
    					?><li class="fax"><?php include( $template_svg_path . 'icon-fax.svg' ); ?><span><?php esc_html_e( 'Fax:', 'inspiry' ); ?></span><?php  echo esc_html( $fax ); ?></li><?php
    					$agent_print .= ("<li class='fax'>Fax: <span>" . esc_html_e( 'Fax:', 'inspiry' ) . "</span>" . esc_html( $fax ) . "</li>");
    				}
    
    				/*
    				 * Address
    				 */
    				$office_address = $inspiry_agent->get_office_address();
    				if ( !empty( $office_address ) ) {
    					?><li class="map-pin"><?php include( $template_svg_path . 'map-marker.svg' ); echo esc_html( $office_address ); ?></li><?php
    					$agent_print .= ("<li class='map-pin'>Address: " . esc_html( $office_address ) . "</li>");
    				}
    				?>
    				<!--/sse-->
    			</ul>
    			<?php $agent_print .= '</ul>'; ?>
    			<?php
    
    			if ( ! is_singular( 'agent' ) ) {
    			    if (!$is_private) {
        			        /*
        				 * Agent intro text and view profile button
        				 */
        				echo apply_filters( 'the_content', get_inspiry_custom_excerpt( $agent_post->post_content ) );
        				?>
        				<!--sse--><a class="btn-default show-details" href="<?php echo esc_url( $agent_permalink ); ?>"><?php esc_html_e( 'View Profile', 'inspiry' ); ?><i class="fa fa-angle-right"></i></a><!--/sse-->
        				<?php
    			    }
    			}
    
    			/*
    			 * Agent Contact Form
    			 */
    			$agent_email = $inspiry_agent->get_email();
    			if (($is_private) && (empty($agent_email)) && (isset($custom['IDXGenerator_email']))) {
    			    $agent_email = $custom['IDXGenerator_email'];
    			}
    			if ( (!empty($agent_email)) && $inspiry_options['inspiry_agent_contact_form'] ) {
    
    				if ( function_exists( 'ire_property_single_agent_form' ) ) {
    					ire_property_single_agent_form( $inspiry_options, $inspiry_single_property, $agent_email, $agent_id );
    				}
    
    			}
    			
    			echo '</div>';
    			if (endsWith($print_content, "</tr>")) {
    			    $print_content .= "<tr>";
    			}
    			$print_content .= ('<td style="min-width:50%">' . str_replace("\"", "'", $agent_print) . '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>');
    			$print_count++;
    			if ($print_count % 2 == 0) {
    			    $print_content .= "</tr>";
    			}
    			
    			$agent_ndx++;
    
			} else {
			    console("Not a valid agent");
			}
		}
	}
	if (!endsWith($print_content, "</tr>")) {
	    $print_content .= "<td></td></tr>";
	}
	$print_content .= "</table>";
	?>
    <div class="print-only" style="display: none; min-width: 100%;">
    <?php echo $print_content; ?>
    </div>
	<?php 
}
