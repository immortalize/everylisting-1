<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/content.php'); ?>

<div class="entry-content clearfix">

    <?php
    global $inspiry_options;
    global $post;
    
    echo '<div style="background-color: white; padding: 10px;" class="pull-right">' . do_shortcode("[printfriendly]") . '</div>';

    /*
     * Description
     */
    if ( !empty( $post->post_content ) && $inspiry_options[ 'inspiry_property_description' ] ) {
        if ( !empty( $inspiry_options[ 'inspiry_property_desc_title' ] ) ) {
            ?><h4 class="fancy-title"><?php echo esc_html( $inspiry_options[ 'inspiry_property_desc_title' ] ); ?></h4><?php
        }
        ?><div class="property-content"><?php the_content(); ?></div><?php
    }

    /*
     * Additional Details
     */
    if ( $inspiry_options[ 'inspiry_property_details' ] ) {
        get_template_part( 'partials/property/single/additional-details' );
    }

    /*
     * Features
     */
    if ( $inspiry_options[ 'inspiry_property_features' ] ) {
        get_template_part( 'partials/property/single/features' );
    }

    /*
     * Floor Plans
     */
    if ( $inspiry_options[ 'inspiry_property_floor_plans' ] ) {
        get_template_part( 'partials/property/single/floor-plans' );
    }

    /*
     * Video
     */
    if ( $inspiry_options[ 'inspiry_property_video' ] ) {
        get_template_part( 'partials/property/single/video' );
    }

    /*
     * Virtual Tour
	 */
    if ( $inspiry_options['inspiry_property_virtual_tour'] ) {
        get_template_part( 'partials/property/single/virtual-tour' );
    }
    
    /*
     * View Original Agent Information (Capability 0 is to access original agent information)
	 */
    if ( current_user_can( '0' ) ) {
    	// get_template_part( 'partials/property/single/agent-information' );
    }
    ?>
</div>
