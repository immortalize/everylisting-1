<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/property/single/slider.php'); ?>

<?php
global $post;

$gallery_images = inspiry_get_post_meta(
                            'REAL_HOMES_property_images',
                            array(
                                'type' => 'image_advanced',
                                'size' => 'post-thumbnail'
                            ),
                            $post->ID
                        );

$gallery_delayed_images = get_post_meta( $post->ID, 'delayed_images', true );

if ( !empty( $gallery_images ) ) {
    preg_match_all('/"([^"|]+)"/', $gallery_delayed_images, $matches);

    echo '<div class="single-property-slider gallery-slider flexslider shared-box" style="max-height: 510px;">';

        echo '<ul class="slides">';
        
        $print_count = 0;
        $print_content = "<table border='0'><tr>";

        foreach( $gallery_images as $gallery_image ) {
            // caption
            $caption = ( !empty( $gallery_image['caption'] ) ) ? $gallery_image['caption'] : $gallery_image['alt'];

            echo '<li>';
                echo '<a class="swipebox" data-rel="gallery-'. $post->ID  .'" href="'. $gallery_image['full_url'] .'" title="'. $caption .'" >';
                echo '<img data-url="' . get_permalink($post->ID) .'" data-title="' . $post->post_title .'" data-summary="' . wp_strip_all_tags($post->post_content) .'" data-media="'. $gallery_image['url'] .'" src="'. $gallery_image['url'] .'" alt="'. $gallery_image['title'] .'" class="property-detail-image"/>';
                    $print_content .= '<td><img src="'. $gallery_image['url'] .'"/></td>';
                    $print_count++;
                echo '</a>';
            echo '</li>';
        }
        foreach( $matches as $match ) {
            $count = 0;
            foreach( $match as $m2 ) {
               // caption
                $caption = ( !empty( $gallery_image['caption'] ) ) ? $gallery_image['caption'] : $gallery_image['alt'];
    
                echo '<li>';
                    echo '<a id="delayed_hyperlink_'.$count.'" class="swipebox" data-rel="gallery-'. $post->ID  .'" href="'.trim($m2, '\"').'" title="'. $caption .'" >';
                    echo '<img data-url="' . get_permalink($post->ID) .'" data-title="' . $post->post_title .'" data-summary="' . wp_strip_all_tags($post->post_content) .'" data-media="'.trim($m2, '\"').'" id="delayed_image_'.$count.'" src="'.trim($m2, '\"').'" class="property-detail-image" alt="Loading, please wait..." onerror="removeimage(this.id);"/>';
                        if (endsWith($print_content, "</tr>")) {
                            $print_content .= "<tr>";
                        }
                        $print_content .= '<td><img src="'. trim($m2, '\"') .'"/></td>';
                        $print_count++;
                        if ($print_count % 2 == 0) {
                            $print_content .= "</tr>";
                        }
                    echo '</a>';
                echo '</li>';
                $count++;
            }
           break;
        }
        if (!endsWith($print_content, "</tr>")) {
            $print_content .= "<td></td></tr>";
        }
        $print_content .= "</table>";
        ?>
        <div class="print-only">
        <?php echo $print_content; ?>
        </div>
		<?php 
        echo '</ul>';

    echo '</div>';

    // for print
    if ( has_post_thumbnail() ) {
        echo '<div id="property-featured-image" class="only-for-print">';
            the_post_thumbnail( 'post-thumbnail', array( 'class' => 'img-responsive' ) );
        echo '</div>';
    }

} else if ( has_post_thumbnail() ) {

    inspiry_standard_thumbnail( 'post-thumbnail' );

} else {
    // display placeholder
    inspiry_image_placeholder( 'post-thumbnail', 'img-responsive' );

}
echo "\r\n";
echo '<script>';
echo "\r\n";
?>
    function removeimage(id) {
        console.log("removeimage called.");
        var el = document.getElementById(id);
        if (el != undefined) {
            if (el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
        el =  document.getElementById(id.replace("_image_", "_hyperlink_"));
        if (el != undefined) {
            if (el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
    }

<?php
echo '</script>';