<?php commentPHPContext('/wp-content/themes/inspiry-real-places/partials/agent/single/agent-info.php'); ?>

<?php

if ( ! class_exists( 'Inspiry_Real_Estate' ) ) {
	return;
}

$inspiry_agent = new Inspiry_Agent( get_the_ID() );
?>
<div class="inner-wrapper clearfix">

    <?php
    if ( has_post_thumbnail( get_the_ID() ) ) {
        ?>
        <figure class="agent-image">
            <a href="<?php the_permalink(); ?>" >
                <?php the_post_thumbnail( 'inspiry-agent-thumbnail', array( 'class' => 'img-circle' ) ); ?>
            </a>
        </figure>
        <?php
    }
    ?>

    <h3 class="agent-name">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        <?php
        $agent_job_title = $inspiry_agent->get_job_title();
        if ( !empty( $agent_job_title ) ) {
            ?><span><?php echo esc_html( $agent_job_title ); ?></span><?php
        }
        ?>
    </h3>

    <div class="agent-social-profiles">
	    <?php
	    if ( function_exists( 'ire_agent_social_profiles' ) ) {
		    ire_agent_social_profiles( $inspiry_agent );
	    }
	    ?>
    </div>

</div>

<ul class="agent-contacts-list">
    <?php
    $template_svg_path = get_template_directory() . '/images/svg/';

    /*
     * Mobile
     */
    $mobile = $inspiry_agent->get_mobile();
    if ( !empty( $mobile ) ) {
        ?><!--sse-->
        <li class="mobile"><?php include( $template_svg_path . 'icon-mobile.svg' ); ?><span><?php esc_html_e( 'Mobile:', 'inspiry' ); ?></span><?php echo esc_html( $mobile ) ?></li>
        <!--/sse-->
        <?php
    }

    /*
     * Office
     */
    $office_phone = $inspiry_agent->get_office_phone();
    if ( !empty( $office_phone ) ) {
        ?><!--sse-->
        <li class="office"><?php include( $template_svg_path . 'icon-phone.svg' ); ?><span><?php esc_html_e( 'Office:', 'inspiry' ); ?></span><?php echo esc_html( $office_phone ); ?></li>
        <!--/sse--><?php
    }

    /*
     * Fax
     */
    $fax = $inspiry_agent->get_fax();
    if ( !empty( $fax ) ) {
        ?><!--sse-->
        <li class="fax"><?php include( $template_svg_path . 'icon-fax.svg' ); ?><span><?php esc_html_e( 'Fax:', 'inspiry' ); ?></span><?php  echo esc_html( $fax ); ?></li>
        <!--/sse--><?php
    }

    /*
     * Address
     */
    $office_address = $inspiry_agent->get_office_address();
    if ( !empty( $office_address ) ) {
        ?><!--sse-->
        <li class="map-pin"><?php include( $template_svg_path . 'map-marker.svg' ); echo esc_html( $office_address ); ?></li>
        <!--/sse--><?php
    }
    ?>

</ul>

<?php
if ( ! is_singular( 'agent' ) ) {
    /*
    * Agent intro text and view profile button
    */
    echo apply_filters( 'the_content', get_inspiry_excerpt() ); ?>
    <!--sse--><a class="btn-default show-details" href="<?php the_permalink(); ?>"><?php esc_html_e( 'View Profile', 'inspiry' ); ?><i class="fa fa-angle-right"></i></a><!--/sse--><?php
}

?>