
<div class="option-bar form-control-buttons" >
    <input type="submit"  value="<?php esc_html_e( 'Search', 'inspiry-real-estate' ); ?>" class="form-submit-btn searchbtn float-right" onclick="submitSearch();">
</div>

<script>
function submitSearch() {
	var value = calculateSearchStateJson("advancedsearch");
	value = (value != "{}")?value:"";
	console.log("*** request:  " + value);
	if (value != "") {
    	jQuery.ajax({
              method:'POST',
              url: '/wp-admin/admin-ajax.php',
              data: {action: 'folder_contents', json:value},
              success: function(res) {
					// console.log("*** result:  " + res);
                	$("#aftersearchshow").html(res);					
              }
        });
				var x = document.getElementById("advancesearchClose");
				x.click();
	}
}
</script>

<?php
/*
<div class="option-bar form-control-buttons" style="margin-top: -70px; margin-bottom: -400px;">
    <input type="submit"  value="<?php esc_html_e( 'Search', 'inspiry-real-estate' ); ?>" class="form-submit-btn searchbtn">
    <a class="form-submit-btn expand-adv advsearch"><i class="fa fa-plus" aria-hidden="true"></i></a>
    <div class="tooltiptext_1">Specify the features of interest</div>
</div>
*/
?>