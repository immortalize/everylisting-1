<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/home/properties-two.php'); ?>

<div class="container" id="load_table_home" >
        <br/>
         <?php echo do_shortcode("[table-juris-purchases jurs_id='0' title='Purchases']"); ?>
            
        <br/>
        <?php echo do_shortcode("[table-juris-rents jurs_id='0' title='Vacation Rentals (per day)']"); ?>
        
        <br/>
        <?php echo do_shortcode("[table-juris-leases jurs_id='0' title='Leases (per month)']"); ?>
    </div>
   
<?php 
global $inspiry_options;
$number_of_properties = intval( $inspiry_options[ 'inspiry_home_properties_number_2' ] );
if( !$number_of_properties ) {
    $number_of_properties = 8;
}


// Current Page
//global $paged;
// if ( is_front_page() ) {
//     $paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
// }

$home_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_properties,
    'meta_query' => array(
            array(
               'key' => '_thumbnail_id',
               'compare' => 'EXISTS'
           ),
        )
    //'paged'          => $paged
);

$home_properties_query = new WP_Query( apply_filters( 'inspiry_home_properties', $home_properties_args )  );

global $found_properties;

$found_properties = $home_properties_query->found_posts; ?>

<div id="homeProperties" class="property-listing-two">
    <div class='row' id="pt121">
        <div class="col-md-5 col-xs-7">
            <h1 class="font90">Latest&nbsp;<div style='display:inline' id='countryheadingshow'></div>&nbsp;Properties</h1>
        </div>
        <div class="col-md-2 "></div>
        <div class="row showmob">
            <div class="col-md-12">
                <button class="showmob1">Filters</button>
            </div>
        </div>
        
        <div class='col-md-5 map-select col-xs-10'>
            <div class='row'>
                <!-- <form action="" method="POST"> -->
                    <div class="col-md-5 pd0"></div>
                    <div id="hide123" class="property-filter">
                        <div class='col-md-4 pd0'>
                            <?php 
                            $search = "<select name='lifestyle1[]' class = 'lifestyles1'>";
                            $search .= "<option value = ''>Select</option>";
                            $search .= "<option value = 'Beach Living'>Beach Living</option>";
                            $search .= "<option value = 'Mountain Living'>Mountain Living</option>";
                            $search .= "<option value = 'City Life'>City Life</option>";
                            $search .= "<option value = 'Private Island'>Private Island</option>";
                            $search .= "<option value = 'Hotel Investment'>Hotel Investment</option>";
                            $search .= "<option value = 'Farm Land'>Farm Land</option>";
                            $search .= "<option value = 'Short-Term Rental Investment'>Short-Term Rental Investment</option>";
                            $search .= "</select>";
                            echo $search;
                            ?>
                        </div>
                        <div class='col-md-4 pd0' id="box_second">
                            
                            <?php 
                           
                            $countries = countries_list(false, false);
                            sort($countries);
                           
                            $drop = "<select name='countryname' id='country_listing1'>";
                            $drop .= "<option value = ''>Country</option>";
                            $drop .= "<option value = 'Worldwide'>Worldwide</option>";
                            foreach ($countries as $k => $v) {
                                $drop .= "<option value = '".$v."'>".$v."</option>";
                            }
                            $drop .= "</select>";
                            if (count($countries) > 0) echo $drop;
                            ?>
                        </div>
                    </div>
                    <div class='col-md-2 pd0' id="colcen1">
                       <button class="form-submit-btn clear_btn countrySearch" style="visibility: hidden; background-color: #57AA5B !important;color: white">APPLY</button>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </div>

  <div class="newContainer" id="aftersearchshow">
        <?php 
        //col-xs-7
        if(isset($_POST['iteration'])){
            $iteration = (int)$_POST['iteration'];
            $iteration++;
        }else{
            $iteration = 1;
        }
        
        if(isset($_POST['offset'])){
            $offset = $_POST['offset'];
            $passOffset = $iteration*16;
        }else{
            $offset = '';
            $passOffset = 16;
        }
    
    if (is_front_page()) {
        if (isset($_COOKIE['country'])) {
            $country = $_COOKIE['country'];
        }
    } else {
        $country = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
    }
    $allownothumbnail = false;
                          
    if ((isset($country)) && (strlen($country) > 0)) {
        $properties_search_arg = array(
            'post_type' => 'property',
            'posts_per_page' => 16,
            'offset' => $offset,
            'orderby'   => 'ID',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key'   => 'IDXGenerator_Country',
                    'value' => $country
                ),
                array(
                    'key' => '_thumbnail_id',
                    'compare' => 'EXISTS',
                ),
            ),
        );
    } else {
        $properties_search_arg = array(
            'post_type' => 'property',
            'posts_per_page' => 16,
            'offset' => $offset,
            'orderby'   => 'ID',
            'order' => 'DESC',
            'meta_query' => array(
                array(
                    'key' => '_thumbnail_id',
                    'compare' => 'EXISTS',
                ),
            ),
        );
    }
	
	echo '<!-- DEBUGQUERY 163: ';
	print_r(apply_filters( 'inspiry_property_search', $properties_search_arg));
	echo ' -->';

	$properties_search_query = new WP_Query( apply_filters( 'inspiry_property_search', $properties_search_arg )  );
	
	echo '<!-- DEBUGQUERYRES: ';
	echo "Found ' . $properties_search_query->found_posts . ' posts.";
	echo ' -->';
	
	if ((!$properties_search_query->have_posts()) && (($offset == '') || ($offset == 0))) {
	    wp_reset_postdata();
	    if ((isset($country)) && (strlen($country) > 0)) {
	        $properties_search_arg = array(
	            'post_type' => 'property',
	            'posts_per_page' => 16,
	            'offset' => $offset,
	            'orderby'   => 'ID',
	            'order' => 'DESC',
	            'meta_query' => array(
	                array(
	                    'key'   => 'IDXGenerator_Country',
	                    'value' => $country
	                )
	            ),
	        );
	    } else {
	        $properties_search_arg = array(
	            'post_type' => 'property',
	            'posts_per_page' => 16,
	            'offset' => $offset,
	            'orderby'   => 'ID',
	            'order' => 'DESC'
	        );
	    }
	    echo '<!-- DEBUGQUERY 188: ';
	    print_r(apply_filters( 'inspiry_property_search', $properties_search_arg));
	    echo ' -->';
	    
	    $properties_search_query = new WP_Query( apply_filters( 'inspiry_property_search', $properties_search_arg )  );
	    $allownothumbnail = true;
	}
	
	echo '<script>console.log("196. Found ' . $properties_search_query->found_posts . ' posts.");</script>';

    if ($properties_search_query->have_posts()): 
    
        for ($pass = 0; $pass <= 1; $pass++) {
    
            $shownpropertiescount = 0;
            $properties_count = 1;
            $columns_count = 4;   // todo: add the ability to change number of columns between 2, 3, 4
            
            while ($properties_search_query->have_posts()):
            	$properties_search_query->the_post();
            	if ((has_post_thumbnail()) || ($allownothumbnail)) {
            	    $shownpropertiescount++;
            	    // echo '<script>console.log("Displaying post #' .$properties_count . '");</script>';
            	    ?>
            	    <div class='col-lg-3'>
            	    <div class="homepropBoxes thumbnail-size">
            	    <?php
            	    new_property_list_insert(get_the_ID());
            	    ?>
            	    </div>
            	    </div>
            	    <?php
            	} else {
            	    echo '<script>console.log("Post #' .$properties_count . ' has no thumbnail.");</script>';
            	}
                $properties_count++;
             endwhile;
             
             if ($shownpropertiescount > 0) {
             	break;
             }
             if ($pass == 0) {
                $allownothumbnail = true;
                rewind_posts();
             }
         }
         ?>
            
         <div class = "col-md-12 loadmorediv"  style="text-align: center;">
        	<div class="alm-btn-wrap">
        		<button class="alm-load-more-btn more loadmore"   style="background: #0073aa !important;"  data-country = "<?php echo $country; ?>"  data-offset = "<?=$passOffset?>" data-iteration = "<?=$iteration?>" rel="next">Show More Properties</button>
        	</div>
        </div>
            
        <?php
        else: ?>
        	<div class = "col-md-12 " style="text-align: center;">
        		<div class="alm-btn-wrap">
        			<button class="alm-load-more-btn more " style="background: #0073aa !important;">No Property Found</button>
        		</div>
        	</div>
        <?php
         endif;
         wp_reset_postdata();
         ?>
    </div>
</div>

<?php  wp_reset_postdata(); 
/*


issues
1 view temnplate in ajax load more
2 shortcode change + search query change
3 ajax not worked in function.php


*/

 ?>    


              



