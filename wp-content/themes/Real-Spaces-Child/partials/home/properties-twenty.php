<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/home/properties-twenty.php'); ?>

<?php
global $inspiry_options;
$number_of_properties = intval($inspiry_options['inspiry_home_properties_number_2']);
if (! $number_of_properties) {
    $number_of_properties = 8;
}

// Current Page
// global $paged;
// if ( is_front_page() ) {
// $paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
// }

$home_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_properties,
    'meta_query' => array(
        array(
            'key' => '_thumbnail_id',
            'compare' => 'EXISTS'
        )
    )
    // 'paged' => $paged
);

$home_properties_query = new WP_Query(apply_filters('inspiry_home_properties', $home_properties_args));

global $found_properties;

$found_properties = $home_properties_query->found_posts;
?>

<div id="homeProperties" class="property-listing-two">
	<div class='row' id="pt121">
		<div class="col-md-12 col-xs-12 col-lg-6">
			<h1 class="font90" style="width: 100% !important">Latest Properties</h1>
		</div>

		<!--         <div class="row showmob"> -->
		<!--             <div class="col-md-12"> -->
		<!--                 <button class="showmob1">Filters</button> -->
		<!--             </div> -->
		<!--         </div> -->

		<div class='col-md-12 col-xs-12 col-lg-6'>
			<!-- <form action="" method="POST"> -->
			<div class="col-md-2"></div>
			<div id="hide123" class="property-filter pdl">
				<div class="row">
					<div class='col-xs-10 col-md-10 col-lg-5 form-group' >
                    <?php
                    $search = '<label for="lifestyles1" style="color: black;">Lifestyle</label>';
                    $search .= "<select name='lifestyle1[]' id='lifestyles1' class='selectpicker'>";
                    $search .= "<option value = '' data-hidden = 'true'>None</option>";
                    $search .= "<option value = 'Beach'>Beach Living</option>";
                    $search .= "<option value = 'Mountain'>Mountain Living</option>";
                    $search .= "<option value = 'City'>City Life</option>";
                    $search .= "<option value = 'Private Island'>Private Island</option>";
                    $search .= "<option value = 'Hotel'>Hotel Investment</option>";
                    $search .= "<option value = 'Farm Land'>Farm Land</option>";
                    $search .= "<option value = 'Short-Term Rental Investment'>Short-Term Rental Investment</option>";
                    $search .= "</select>";
                    echo $search;
                    ?>
                </div>
					<div class='col-xs-10 col-md-10 col-lg-5 form-group' id="box_second">
                    
                    <?php

                    $countries = countries_list(false, false);
                    sort($countries);

                    $drop = '<label for="country_listing1" style="color: black;">Country</label>';
                    $drop .= '<select name="countryname" id="country_listing1" class="selectpicker">';
                    $drop .= '<option value="Worldwide">Worldwide</option>';
                    foreach ($countries as $k => $v) {
                        $drop .= '<option value = "' . $v . '">' . $v . '</option>';
                    }
                    $drop .= "</select>";
                    if (count($countries) > 0)
                        echo $drop;
                    ?>
                </div>
                <div class='col-xs-2 col-md-2 col-lg-2' id="colcen1">
					<button class="form-submit-btn countrySearch" style="visibility: hidden; background-color: #57AA5B !important; color: white">APPLY</button>
				</div>
			</div>

			</div>
			
			<!-- </form> -->
		</div>
	</div>

	<div class="newContainer" id="aftersearchshow">
        <?php
        // col-xs-7
        if (isset($_POST['iteration'])) {
            $iteration = (int) $_POST['iteration'];
            $iteration ++;
        } else {
            $iteration = 1;
        }

        if (isset($_POST['offset'])) {
            $offset = $_POST['offset'];
            $passOffset = $iteration * 16;
        } else {
            $offset = '';
            $passOffset = 16;
        }

        $jurisdictiontype = null;
        $jurisdictionname = null;
        if ((is_front_page()) && (isset($_COOKIE['country']))) {
            $jurisdictiontype = 'COUNTRY';
            $jurisdictionname = $_COOKIE['country'];
        } else {
            $jurisdictiontype = get_post_meta($id, 'IDXGenerator_jurisdiction_type', true);
            $jurisdictionname = get_post_meta($id, 'IDXGenerator_jurisdiction_name', true);
        }

        if (($jurisdictiontype != null) && ($jurisdictionname != null)) {
            $properties_search_arg = array(
                'post_type' => 'property',
                'posts_per_page' => 16,
                'offset' => $offset,
                'orderby' => 'ID',
                'order' => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'IDXGenerator_jurisdiction_type',
                        'value' => $jurisdictiontype
                    ),
                    array(
                        'key' => 'IDXGenerator_jurisdiction_name',
                        'value' => $jurisdictionname
                    ),
                    array(
                        'key' => '_thumbnail_id',
                        'compare' => 'EXISTS'
                    )
                )
            );
        } else {
            $properties_search_arg = array(
                'post_type' => 'property',
                'posts_per_page' => 16,
                'offset' => $offset,
                'orderby' => 'ID',
                'order' => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => '_thumbnail_id',
                        'compare' => 'EXISTS'
                    )
                )
            );
        }

        echo '<!-- DEBUGQUERY: ';
        print_r(apply_filters('inspiry_property_search', $properties_search_arg));
        echo ' -->';

        $properties_search_query = new WP_Query(apply_filters('inspiry_property_search', $properties_search_arg));

        // echo '<script>console.log("Found ' . $properties_search_query->found_posts . ' posts.");</script>';

        if ($properties_search_query->have_posts()) :
            $properties_count = 1;
            $columns_count = 3; // todo: add the ability to change number of columns between 2, 3, 4

            while ($properties_search_query->have_posts()) :
                $properties_search_query->the_post();
                if (has_post_thumbnail()) {
                    // echo '<script>console.log("Displaying post #' .$properties_count . '");</script>';
                    ?>
                <div class='col-lg-4'>
			<div class="homepropBoxes thumbnail-size">
                <?php
                    new_property_list_insert(get_the_ID());
                    ?>
                </div>
		</div>
                <?php
                } else {
                    echo '<script>console.log("Post #' . $properties_count . ' has no thumbnail.");</script>';
                }
                $properties_count ++;
            endwhile
            ;
            ?>
            
         <div class="col-md-12 loadmorediv" style="text-align: center;">
			<div class="alm-btn-wrap">
				<button class="alm-load-more-btn more loadmore"
					data-country="<?php echo $country; ?>"
					data-offset="<?=$passOffset?>" data-iteration="<?=$iteration?>"
					rel="next" style="background-color: #313131 !important">See More Properties</button>
			</div>
		</div>
            
        <?php
        else :
            ?>
            <div class="col-md-12 " style="text-align: center;">
			<div class="alm-btn-wrap">
				<button class="alm-load-more-btn more ">No Property Found</button>
			</div>
		</div>
        <?php
        endif;
        wp_reset_postdata();
        ?>
    </div>
</div>

<?php

wp_reset_postdata();
/*
 *
 *
 * issues
 * 1 view temnplate in ajax load more
 * 2 shortcode change + search query change
 * 3 ajax not worked in function.php
 *
 *
 */

?>    


              



