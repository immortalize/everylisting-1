
<style>
.wrapper {
	margin: 10px 90px 10px;
	/* max-width: 1200px; */
	background: #293542;
	border-radius: 20px;
	box-shadow: 0 0 7.49px 0.51px rgb(42, 53, 66);
}

.cols-wrapper {
	overflow: hidden;
	text-align: center;
}

.cols-4 .col {
	width: 25%;
}

.col {
	float: left;
	/*  margin-right: 3.2%;*/
	/*   margin-bottom: 30px; */
}

.cols-4 .col:nth-of-type(4n) {
	clear: right;
	margin-right: 0;
}

.milestone-container {
	position: relative;
	padding-top: 20px;
}

.milestone {
	color: #ffffff;
	font-size: 30px;
	line-height: 1.2;
	padding: 0;
	margin-bottom: 20px;
	position: relative;
	font-weight: 700;
	text-transform: uppercase;
	letter-spacing: -0.02em;
}

.milestone-container h3 {
	color: #ffffff;
	margin-bottom: 0px;
}

@media screen and (max-width: 1000px) {
	.cols-4 .col {
		width: 48.4%;
	}
	.cols-4 .col:nth-of-type(2n) {
		clear: right;
		margin-right: 0;
	}
}

@media screen and (max-width: 520px) {
	.col {
		clear: none !important;
		margin-right: 0 !important;
		margin-left: 0 !important;
		max-width: 100% !important;
		width: 100% !important;
	}
}
</style>
<div class="wrapper">
	<div class="cols-wrapper cols-4" style="text-align: center;">
		<div class="col">
			<div class="milestone-container">
				<span class="timer1 milestone"></span>
				<h3>Countries</h3>
			</div>
			<br>
		</div>
		<div class="col">
			<div class="milestone-container">
				<span class="timer2 milestone"></span>
				<h3>Active Properties</h3>
			</div>
			<br>
		</div>
		<div class="col nomargin">
			<div class="milestone-container">
				<span class="timer4 milestone"></span>
				<h3>For Sale</h3>
			</div>
			<br>
		</div>
		<div class="col">
			<div class="milestone-container">
				<span class="timer3 milestone"></span>
				<h3>For Rent</h3>
			</div>
		</div>
	</div>
</div>



