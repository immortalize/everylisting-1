<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/logo.php'); ?>

<div id="sitelogo">
        <?php
        global $inspiry_options;
        $logo_image_classes = '';
        $inspiry_site_name  = get_bloginfo( 'name' );
        $inspiry_tag_line   = get_bloginfo( 'description' );
            ?>
	
	<a href="<?php echo esc_url( home_url('/') ); ?>"><img src="/mediacode/logos/retired/header_bar_logo.jpg" alt="<?php echo esc_attr( $inspiry_site_name ); ?>" /></a>
   
</div>
 
<!-- /#site-logo -->