<?php commentPHPContext('/wp-content/themes/Real-Spaces-Child/partials/header/menu-left.php'); ?>

<nav id="site-main-nav-left" class="site-main-nav-left">
    <?php
    if (!function_exists('compare_title')) :
        function compare_title($a, $b) {
            return strnatcmp(get_the_title($a), get_the_title($b));
        }
    endif; // compare_title
    if (!function_exists('posts_nav_menu_items')) :
        function posts_nav_menu_items($items) {
//             $homelink = '<li class="home"><a href="' . home_url( '/' ) . '">' . __('Home') . '</a></li>';
//             // add the home link to the end of the menu
//             $items = $items . $homelink;
            $posts_args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'perm' => 'readable',
                    'posts_per_page' => '5',
                    'orderby' => 'ID',
                    'order' => 'DESC'
                );
            $posts = array();
            $posts_query = new WP_Query(apply_filters('inspiry_home_properties', $posts_args));
            while ($posts_query->have_posts()) {
                $posts_query->the_post();
                array_push($posts, get_post());
            }
            unset($replacement);
            unset($classes);
            $matches = array();
            if (!wp_is_mobile()) {
                if (preg_match_all('/<li([^>]*?)class=\"([^\\"]*)\"><a href=\"([^\"]*?)\"([^>]*?)>Home<\/a><\/li>/', $items, $matches, PREG_PATTERN_ORDER) > 0) {
                    $other = $matches[1];
                    $classes = $matches[2];
                    $link = $matches[3];
                    $extra = $matches[4];
                    $replacement = $matches[0];
                    $homereplacementhtml = '<li' . $other[0] . 'class="' . $classes[0] . '"><a href="' . $link[0] . '"' . $extra[0] .'><img src="/mediacode/logos/logoname/220-36-color.png" width="177px" height="29px"/></a></li>';
                    $items = str_replace($replacement, $homereplacementhtml, $items);
                }
            }
            if (preg_match_all('/<li([^>]*?)class=\"([^\\"]*)\"><a href=\"#\">Posts<\/a><\/li>/', $items, $matches, PREG_PATTERN_ORDER) > 0) {
                $other = $matches[1];
                $classes = $matches[2];
                $replacement = $matches[0];
            }
            $posthtml = '';
            if ((isset($classes)) && (isset($replacement))) {
                $i = 0;
                foreach ($replacement as $oneReplacement) {
                    foreach ($posts as $post) {
                        $posthtml .= '<li' . $other[$i] . 'class="' . $classes[$i] . '"><a href="' . get_permalink($post) . '">' . get_the_title($post) . '</a></li>';
                    }
                    $items = str_replace($oneReplacement, $posthtml, $items);
                    $i++;
                }
            }
            $items = str_replace("<a href=\"#\">&#8211;</a>", "<hr/>", $items);
//             echo '<!-- HTML: ';
//             echo $posthtml;
//             echo ' -->';
//             echo '<!-- REGEX: ';
//             echo 'class: ' . $classes;
//             echo ' - replacement: ' . $replacement;
//             echo ' -->';
            return $items;
        }
        endif; // posts_nav_menu_items
    
    add_filter('wp_nav_menu_items', 'posts_nav_menu_items');
    wp_nav_menu( array(
        'menu' => '2662',
        'container' => false,
        'menu_class' => 'main-menu clearfix',
        'fallback_cb' => false,
    ) );
    ?>
	

</nav>

